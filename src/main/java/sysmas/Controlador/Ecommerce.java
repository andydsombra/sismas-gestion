package sysmas.Controlador;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sysmas.Entidades.*;
import sysmas.Enumeraciones.Cat;
import sysmas.Enumeraciones.Op;
import sysmas.Errores.ErrorServicio;
import sysmas.Interfaces.ClienteInterface;
import sysmas.Servicios.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import sysmas.model.Response;

@RestController
@RequestMapping("")
public class Ecommerce {

    @Autowired
    private UsuarioServicio userSErv;

    @Autowired
    private Producto_OpService prodService;

    @Autowired
    private StockService stockServ;

    @Autowired
    private OperacionesService opServ;

    @Autowired
    private ProductosServicio productosServ;

    @PostMapping("/pedido/nuevo")
    public  ResponseEntity<?> guardarPedido(@RequestBody String pedido) throws ErrorServicio {

        JSONObject jsObject= new JSONObject(pedido);

        JSONObject cliente= jsObject.getJSONObject("cliente");
        JSONArray productos= jsObject.getJSONArray("productos");
        Double total = jsObject.getDouble("total");
        Usuario user = new Usuario();

        try{
            user = userSErv.buscarPorTelefono(cliente.getString("telefono"));
        }catch (Exception e ){
            return new ResponseEntity<>(new Response("No se encuentra el cliente"), HttpStatus.BAD_REQUEST);
        }

        Date fecha= new Date();

        Operaciones op = opServ.nuevaOperacion(Op.ECOMMERCE,25990L, user.getId(), total, fecha);

        for (int i = 0; i < productos.length(); i++) {
            JSONObject objet= productos.getJSONObject(i);
            System.out.println(objet);
            Producto_Op prod = prodService.nuevoProductoOp(objet.getString("id"),op,objet.getInt("cantidad"), objet.getDouble("precioUnitario") );
            stockServ.restarStock(productosServ.buscarStockProducto(objet.getString("id")),objet.getInt("cantidad"));
        }

        return new ResponseEntity<>(new Response("Pedido Cargado"), HttpStatus.CREATED);
    };

    @PostMapping("/pedido/nuevo2")
    public ResponseEntity<?> guardarPedido2(@RequestBody String pedido) throws ErrorServicio {

        JSONObject jsObject= new JSONObject(pedido);

        JSONObject cliente= jsObject.getJSONObject("cliente");
        JSONArray productos= jsObject.getJSONArray("productos");
        Double total = jsObject.getDouble("totalMay");

        Usuario user = userSErv.buscarPorTelefono(cliente.getString("telefono"));

        Date fecha= new Date();

        Operaciones op = opServ.nuevaOperacion(Op.ECOMMERCE,25990L, user.getId(), total, fecha);

            for (int i = 0; i < productos.length(); i++) {
                JSONObject objet= productos.getJSONObject(i);
                Producto_Op prod = prodService.nuevoProductoOp(objet.getString("id"),op,objet.getInt("cantidad"), objet.getDouble("precioMayorista") );
                stockServ.restarStock(productosServ.buscarStockProducto(objet.getString("id")),objet.getInt("cantidad"));
            }


        return new ResponseEntity<>(new Response("Pedido Cargado"), HttpStatus.CREATED);
    };


    @GetMapping("/categoria")
    public List categoriasAPI() throws ErrorServicio {

        ArrayList<Categoria> a = new ArrayList<>();

        Categoria d = new Categoria();

        d.setId(1);
        d.setNombre((Cat.ELECTRO).toString());
        d.setTipos(productosServ.buscarPorCat(Cat.ELECTRO));
        a.add(d);

        Categoria c = new Categoria();

        c.setId(0);
        c.setNombre((Cat.AUTO).toString());
        c.setTipos(productosServ.buscarPorCat(Cat.AUTO));
        a.add(c);

        return a;
    };

    @PostMapping("/verificar")
    public ClienteInterface verificarCliente(@RequestBody String mail ){

        String email= mail.replace("{\"mail\":\"", "");
        String correo= email.replace("\"}", "");

        try {
            return userSErv.clientePorMail(correo);
        } catch (Exception e) {
            return userSErv.clientePorMail(correo);
        }
    }

}
