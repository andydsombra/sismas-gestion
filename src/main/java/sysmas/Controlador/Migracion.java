package sysmas.Controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sysmas.Entidades.*;
import sysmas.Enumeraciones.Tipo;
import sysmas.Enumeraciones.User;
import sysmas.Errores.ErrorServicio;
import sysmas.Servicios.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/migracion")
public class Migracion {

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private ClienteServicio clienteServ;

    @Autowired
    private ProductosServicio productosServ;

    @Autowired
    private ProductoServicio prodServ;

    @Autowired
    private DatosService datosServ;

    @Autowired
    private StockService stockServ;

    @Autowired
    private FotoServicio fotoServ;

    @Autowired
    private PreciosService preciosServ;

    @GetMapping("/users")
    public List users() throws ErrorServicio {

        List<Cliente> clientes= clienteServ.todosLosClientes();
        for (Cliente c: clientes) {
            Datos datos= datosServ.nuevoDato(c.getAlta());

            User user= User.PUBLICO;
            if(c.getTipo()== Tipo.MAYORISTA){
                user=User. MAYORISTA;
            }


           // usuarioServicio.registarUsuario(c.getMail(),c.getNombre(),c.getNombre(),"uTunning","uTunning",c.getDireccion(),c.getTelefono(),c.getDocumento(),datos,null,user);
        }
        

        return usuarioServicio.todos();
    }

    @GetMapping("/prueba")
    public String utilFecha(){
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;
        int hora = hoy.getHours();
        int min= hoy.getMinutes();

        LocalDateTime localDate= LocalDateTime.of(anoh,mesh, diah, hora, min );

        ZoneId zoneBuenosAires = ZoneId.of("America/Buenos_Aires");
        ZonedDateTime asiaZonedDateTime = localDate.atZone(zoneBuenosAires);

        ZoneId zoneAmsterdan = ZoneId.of("Europe/Amsterdam");
        ZonedDateTime nyDateTime = asiaZonedDateTime.withZoneSameInstant(zoneAmsterdan);

        // print dateTime with tz
        System.out.println("Date : " + asiaZonedDateTime);
        System.out.println("Date : " + nyDateTime);

        return localDate.toString();

    }

    @GetMapping("/usuarios")
    public List usuarios(){
        return usuarioServicio.tipoAdmin();
    }

    @GetMapping("/clientes")
    public List clientes(){
        return usuarioServicio.tipoCliente();
    }

    @GetMapping("/productos")
    public List productos() throws ErrorServicio {
        List<Producto> prods= prodServ.buscarTodos();


        /*for (Producto product: prods) {
            product.
            ///Stock stock = stockServ.nuevoStock(product.getCantIdeal(),product.getCantMinima(),product.getCantDeposito(), product.getCantLocal(),product.getPreVenta());
            ///Datos datos= datosServ.nuevoDato(new Date());
            Precios precios= preciosServ.nuevoPrecio(product.getCosto(), product.getPrecioUnitario(), product.getPrecioMay1(), product.getPrecioMay2());
            Foto foto = product.getFoto();

            productosServ.cargarProducto(product.getBarras(),product.getCodInterno(),product.getNombre(),product.getCategoria(),product.getTipo(),product.getColor(),product.getDescripcion(),stock.getId(),foto.getId(),precios.getId(), datos.getId());

        }*/


        return productosServ.buscarTodos();

    }

    @GetMapping("/producto")
    public List producto() throws ErrorServicio {

        ArrayList<ApiModel> models= new ArrayList<>();

        List<Productos> prod= productosServ.buscarTodos();

        ApiModel model= new ApiModel();

        for ( Productos p: prod) {
            model.setBarras(p.getBarras());
            model.setCategoria(p.getCat().toString());
            model.setFoto("http://localhost:8080/foto/producto?id=" + p.getFotos_id());
            model.setNombre(p.getNombre());
            model.setPrecios(preciosServ.buscarPorId(p.getPrecios_id()));
            model.setStock(stockServ.encotrarStock(p.getStock_id()));

            models.add(model);

        }


        return models;
    }

}
