package sysmas.Seguridad.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import sysmas.Entidades.Cliente;
import sysmas.Entidades.Usuario;
import sysmas.Repositorios.UsuarioRepository;
import sysmas.Servicios.ClienteServicio;

import java.util.Iterator;
import java.util.List;

@Service
public class UsuarioDetailsService implements UserDetailsService  {
	
	@Autowired
	private UsuarioRepository userRepo;

  @Override
  public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
    ///El siguiente MAp solo contiene un arreglo de usarios y roles.
	 /* Map<String, String> usuarios = Map.of(
        "jcabelloc", "USER",
        "mlopez", "ADMIN",
        "andy","ADMIN"
    );*/
	  List<Usuario> users= null;

	  try {
		  users= userRepo.findAll();
	} catch (Exception e) {
		System.out.println("Error al buscar todos los Usuarios");
	}
	  
	 
	  Usuario usuario= new Usuario();
	  
	 for (Usuario user : users) {
		 
		 String email = user.getMail();
		if (email.equalsIgnoreCase(mail)) {
			usuario= user;
			break;
		}
	}
	 
	 var rol=usuario.getTipo().toString();
    /*var rol = usuarios.get(username);*////Setear rol si Usuario existe///
    
    
    if (rol != null) {
      User.UserBuilder userBuilder = User.withUsername(mail);
      // "secreto" => [BCrypt] => $2a$10$56VCAiApLO8NQYeOPiu2De/EBC5RWrTZvLl7uoeC3r7iXinRR1iiq
      //String encryptedPassword = "$2a$10$9svgMsttcH2vcBjuQG.K2eLiYoW9w1IIfyZDKtDnl5W1xDIGfvdMm";///Password en base de datos
      String encryptedPassword = usuario.getClave();
      userBuilder.password(encryptedPassword).roles(rol);
      return userBuilder.build();
    } else {
      throw new UsernameNotFoundException(mail);
    }

  }
}
