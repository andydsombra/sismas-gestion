package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Precios;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.PreciosRepository;

import java.util.Optional;

@Service
public class PreciosService {

    @Autowired
    private PreciosRepository preciosRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Precios nuevoPrecio(Double costo, Double publico, Double may1, Double may2){
        Precios p= new Precios();

        p.setCosto(costo);
        p.setPublico(publico);
        p.setMay1(may1);
        p.setMay2(may2);

        return preciosRepo.save(p);
    }

    @Transactional(readOnly = true)
    public Precios buscarPorId(Long id) throws ErrorServicio {
        Precios p= new Precios();
        try {
            Optional<Precios> resp= preciosRepo.findById(id);
            if(resp.isPresent()){
                p= resp.get();
            }
            return p;
        }catch(Exception e){
            throw new ErrorServicio("No se encuentra el precio con id " + id);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Precios modificarPrecio(Long id,Double costo, Double publico, Double may1, Double may2 ) throws ErrorServicio {
        Precios p= new Precios();
        try {
            Optional<Precios> resp= preciosRepo.findById(id);
            if(resp.isPresent()){
                p= resp.get();

                p.setCosto(costo);
                p.setPublico(publico);
                p.setMay1(may1);
                p.setMay2(may2);

            }
            return preciosRepo.save(p);
        }catch(Exception e){
            throw new ErrorServicio("No se encuentra el precio con id " + id);
        }

    }


}
