package sysmas.Servicios;

import sysmas.Entidades.Datos;
import sysmas.Entidades.Foto;
import sysmas.Entidades.Usuario;
import sysmas.Enumeraciones.User;
import sysmas.Enumeraciones.UserAccess;
import sysmas.Errores.ErrorServicio;
import sysmas.Interfaces.ClienteInterface;
import sysmas.Repositorios.UsuarioRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class UsuarioServicio {

    @Autowired
    private UsuarioRepository usuarioRepositorio;

    @Autowired
    private DatosService datosServ;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario registarUsuario(String mail, String nombre, String apellido, String clave1, String clave2, String direccion, String telefono, String documento, String dato, Foto foto, User tipo) throws ErrorServicio {

        Usuario usuario = new Usuario();

        Datos datos= datosServ.nuevoDato(new Date());

       /* Validar(mail, nombre, apellido);*/
        String clave = validarClave(clave1, clave2);

        String email= "cliente@utunning.com.ar";
        if(mail.contains("@")){
            email= mail;
        }

        usuario.setMail(email);
        usuario.setNombre(nombre);
        usuario.setApellido(apellido);
        String encriptada = new BCryptPasswordEncoder().encode(clave);
        usuario.setClave(encriptada);
        usuario.setDatos_id(datos.getId());
        usuario.setActivo(true);
        usuario.setDocumento(documento);
        usuario.setDireccion(direccion);
        usuario.setTelefono(telefono);
        if(foto == null || foto.toString().isEmpty()){
            usuario.setFotos_id(24541L);
        }else{
            usuario.setFotos_id(foto.getId());
        }
        usuario.setTipo(tipo);

        return usuarioRepositorio.save(usuario);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario modificarUsuario(Long id, String mail, String nombre, String apellido, String direccion, String telefono, String documento, User tipo) throws ErrorServicio {

        Usuario usuario = usuarioRepositorio.buscarPorId(id);

        usuario.setMail(mail);
        usuario.setNombre(nombre);
        usuario.setApellido(apellido);
        usuario.setDocumento(documento);
        usuario.setDireccion(direccion);
        usuario.setTipo(tipo);

        return usuarioRepositorio.save(usuario);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario modificarFoto(Long id, Foto foto) throws ErrorServicio {

        Usuario usuario = usuarioRepositorio.buscarPorId(id);

        if(foto == null || foto.toString().isEmpty()){
            usuario.setFotos_id(null);
        }else{
            usuario.setFotos_id(foto.getId());
        }

        return usuarioRepositorio.save(usuario);

    }

    public void Validar(String mail, String nombre, String apellido) throws ErrorServicio {

        if (nombre == null || nombre.isEmpty()) {
            throw new ErrorServicio("El nombre del usuario no puede ser nulo");
        }

        if (apellido == null || apellido.isEmpty()) {
            throw new ErrorServicio("El apellido del usuario no puede ser nulo");
        }

        if (mail == null || mail.isEmpty()) {
            throw new ErrorServicio("Debe ingresar su mail");
        }

        Usuario e = buscarPorMail(mail);

        if (e != null) {
            throw new ErrorServicio("El mail ya se encuentra registrado");
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void modificarClave(String clave1, String clave2, Long id) throws ErrorServicio {
        String clave = validarClave(clave1, clave2);

        Optional<Usuario> respuesta = usuarioRepositorio.findById(id);
        if (respuesta.isPresent()) {
            Usuario usuario = respuesta.get();

            String encriptada = new BCryptPasswordEncoder().encode(clave);
            usuario.setClave(encriptada);
            usuarioRepositorio.save(usuario);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void bajaUsuario(Long id) throws ErrorServicio {

        Optional<Usuario> respuesta = usuarioRepositorio.findById(id);
        if (respuesta.isPresent()) {
            Usuario usuario = respuesta.get();

            Long datos = usuario.getDatos_id();

            datosServ.baja(datos);
            usuario.setActivo(true);
            usuarioRepositorio.save(usuario);
        }
    }


    public String validarClave(String clave1, String clave2) throws ErrorServicio {

        if (clave1 == null || clave1.length() <= 5) {
            throw new ErrorServicio("La clave debe tener al menos 6 carateres");
        }

        if (clave1.equals(clave2)) {
            String clave = clave1;
            return clave;
        } else {
            throw new ErrorServicio("Las claves deben coincidir");
        }
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorId(Long id) {
        Usuario a = null;
        Optional<Usuario> res = usuarioRepositorio.findById(id);
        if (res.isPresent()) {
            a = res.get();
            if(a.getTelefono()== null || a.getTelefono().isEmpty()){
                a.setTelefono("0");
            }
        }
        return a;
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorMail(String mail) {
        Usuario a = usuarioRepositorio.buscarPorMail(mail);

        return a;
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorTelefono(String tel) {
        Usuario a = usuarioRepositorio.buscarTel(tel);

        return a;
    }

    @Transactional(readOnly = true)
    public List todos() {
        List<Usuario> todos = usuarioRepositorio.findAll();
        return todos;
    }

    @Transactional(readOnly = true)
    public List tipoCliente() {
        List<Usuario> todos = usuarioRepositorio.findAll();

        ArrayList<Usuario> resultad= new ArrayList<>();

        for (Usuario u: todos) {
            if(u.getTipo()== User.PUBLICO || u.getTipo()== User.MAYORISTA || u.getTipo()== User.MAYORISTA2){
                resultad.add(u);
            }
        }

        return resultad;
    }

    @Transactional(readOnly = true)
    public List tipoAdmin() {
        List<Usuario> todos = usuarioRepositorio.findAll();

        ArrayList<Usuario> resultad= new ArrayList<>();

        for (Usuario u: todos) {
            if(u.getTipo()== User.ADMIN2 || u.getTipo()== User.ADMIN1 || u.getTipo()== User.ADMIN3){
                resultad.add(u);
            }
        }

        return resultad;
    }

    public Boolean existe(Long id) {
        Boolean existe = false;
        try {
            Usuario resultado = usuarioRepositorio.buscarPorId(id);
            existe = true;
        } catch (Exception e) {
            existe = false;
        }

        return existe;
    }


    public ClienteInterface clientePorMail(String mail) {
        List<Usuario> clientes = usuarioRepositorio.findAll();
        ClienteInterface a = new ClienteInterface();

        for (int i = 0; i < clientes.size(); i++) {
            Usuario c = clientes.get(i);

            if (c.getMail().equalsIgnoreCase(mail)) {
                a.setNombre(c.getNombre());
                a.setDireccion(c.getDireccion());
                a.setId(c.getId());
                a.setMail(c.getMail());
                a.setDocumento(c.getDocumento());
                a.setTelefono(c.getTelefono());
                a.setTipo(c.getTipo());
            }
        }

        return a;
    }

   /* @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepositorio.buscarPorMail(mail);
        if (usuario != null) {
            List<GrantedAuthority> permisos = new ArrayList<>();

            GrantedAuthority p1 = new SimpleGrantedAuthority("ROLE_" + usuario.getTipo());
            permisos.add(p1);

            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(true);

            session.setAttribute("usuariosession", usuario);

            User user = new User(usuario.getMail(), usuario.getClave(), permisos);
            return user;

        } else {
            return null;
        }
    }*/

    @Transactional(readOnly = true)
    public Boolean verificarUser(String mail, String password) {
        Boolean b = false;
        try {
            Usuario u = usuarioRepositorio.buscarPorMail(mail);

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            b = encoder.matches(password, u.getClave());
        } catch (Exception e) {
            b= false;
        }



        return b;
    }
}
