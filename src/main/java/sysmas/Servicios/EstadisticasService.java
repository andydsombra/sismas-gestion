package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sysmas.Entidades.*;
import sysmas.Errores.ErrorServicio;
import sysmas.model.ApiModelDTO;
import sysmas.util.Comparadores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class EstadisticasService {

    @Autowired
    private ProductosServicio prodServ;

    @Autowired
    private Producto_OpService prodOpServ;

    @Autowired
    private OperacionesService opServ;

    @Autowired
    private PreciosService preciosServ;

    @Autowired
    private StockService stockServ;

    @Autowired
    private PagosService pagosServ;

    public List prodMasVendidos(){

        ArrayList<ProdEstadisticas> prods= new ArrayList<>();
        ArrayList<ProdEstadisticas> prodsResp= new ArrayList<>();

        Date fecha= new Date();

        /// Traigo una lista con las id de las Operaciones del día solicitado
        List<Long> operaciones= opServ.buscarPorFecha(fecha);



        ///Recorro la lista de operaciones y traigo todos los productos que corresponden a cada operación
        for (Long op: operaciones) {
            List<Producto_Op> prodsOp= prodOpServ.prodOpPorOperacionE(op);

            /// Con la lista de los productos de cada operación, recorro la lista
            for (Producto_Op pr:prodsOp) {

                /// Verifico si la lista de productos vendidos está vacía. Si lo está agrego el primer producto
                if(prods.size()==0){
                    ProdEstadisticas prod= new ProdEstadisticas();
                    Productos prodOR= prodServ.buscarProducto(pr.getProducto_id());
                    prod.setId(pr.getProducto_id());
                    prod.setCantidad(pr.getCantidad());
                    prod.setNombre(prodOR.getNombre());
                    prod.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + prodOR.getId());
                    prod.setTipo(prodOR.getTipo());
                    prods.add(prod);
                }else {

                    /// Si no está vacía, la recorro y verifico si el producto que tengo ya existe mediante la id del Producto Original
                    Boolean exist= false;
                    for (int i = 0; i < prods.size(); i++) {
                        ProdEstadisticas p=  prods.get(i);

                       /// Si el producto ya está en la lista, entonces solo le agrego la cantidad del que tengo , seteo "Existe" en "true" y rompo el bucle
                        if (p.getId()== pr.getProducto_id()){
                            p.setCantidad(p.getCantidad() + pr.getCantidad());
                            exist= true;
                            break;
                        }
                    }

                    ///Si al finalizar la recorrida del bucle no se encotró el producto, se agrega a la lista.
                    if (exist== false){
                        ProdEstadisticas prod= new ProdEstadisticas();
                        Productos prodOR= prodServ.buscarProducto(pr.getProducto_id());
                        prod.setId(pr.getProducto_id());
                        prod.setCantidad(pr.getCantidad());
                        prod.setNombre(prodOR.getNombre());
                        prod.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + prodOR.getId());
                        prod.setTipo(prodOR.getTipo());

                        prods.add(prod);
                    }
                }


            }

        }

        // Se ordenan los productos del mas al menos vendido
        Collections.sort(prods, Comparadores.ordernarMasVendido);

        // Se envían finalmente los 5 productos mas vendidos del día
        for (int i = 0; i < 5 ; i++) {
           ProdEstadisticas p= prods.get(i);
           prodsResp.add(p);
        }

       return prodsResp;


    }


    public List faltantes() throws ErrorServicio {
        //Traigo todos los productos
        List<Productos> preliminar = prodServ.buscarTodos();
        List<Productos> lista = new ArrayList<>();

        //Al recorrer cada producto, verifico si el stock actual está por debajo de la cantidad mínima
        for (Productos pr: preliminar) {
            Stock stock= stockServ.encotrarStock(pr.getStock_id());
            if(stock.getStock() < stock.getMinimo()){
                lista.add(pr);
            }
        }

        // Paso de la clase Productos a la clase ApiModelDTO
        List definitiva= prodServ.setearApiModel(lista);

        return definitiva;
    }


    public Double mercaderia() throws ErrorServicio {

        Double total= 0.0;

        try {
        List<Productos> prods= prodServ.buscarTodos();
        for (Productos prod: prods ) {
            Stock stock= stockServ.encotrarStock(prod.getStock_id());
            Precios precio= preciosServ.buscarPorId(prod.getPrecios_id());

            Double totalProd= stock.getStock() * precio.getCosto();

            total = total + totalProd;

        }
            return total;
        }catch (Exception e){
            return total;
        }

    }

    public Double ingresos() throws ErrorServicio {
        Double total= 0.0;
        Date fecha =new Date();

        try {
            List<Pagos> pagos= pagosServ.buscarPorFecha(fecha);

            for (Pagos pago: pagos) {
                total = total + pago.getMonto();
            }

            return total;
        }catch (Exception e){
            return total;
        }



    }

    public Double ganacias(){
        Double total= 0.0;
        Date fecha =new Date();

        try {
            List<Long> operaciones= opServ.buscarPorFecha(fecha);
            for (int i = 0; i < operaciones.size(); i++) {
                List<Producto_Op> prodOps= prodOpServ.prodOpPorOperacionE(operaciones.get(i));

                for (int e = 0; e < prodOps.size(); e++) {
                    Producto_Op prodOp= prodOps.get(e);
                    total= total + prodOp.getGanancia();
                }

            }
            return total;
        }catch (Exception e){
            return total;
        }
    }
}
