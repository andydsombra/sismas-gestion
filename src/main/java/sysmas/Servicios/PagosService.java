package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Operaciones;
import sysmas.Entidades.Pago;
import sysmas.Entidades.Pagos;
import sysmas.Entidades.Usuario;
import sysmas.Enumeraciones.Divisa;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.PagosRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PagosService {

    @Autowired
    private PagosRepository pagosRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Pagos nuevoPago(Divisa divisa, Double monto, Long usuario_id, Long operacion_id){
        Pagos p = new Pagos();

        p.setDivisa(divisa);
        p.setFecha(new Date());
        p.setMonto(monto);
        p.setUsuario_id(usuario_id);
        p.setOperacion_id(operacion_id);

        return pagosRepo.save(p);
    }

    @Transactional(readOnly = true)
    public Pagos buscarPorId(String id) throws ErrorServicio {
        Pagos p = new Pagos();
        try {
            Optional<Pagos> resp = pagosRepo.findById(id);
            if(resp.isPresent()){
                p= resp.get();;

            }
            return p;
        }catch (Exception e){
            throw new ErrorServicio("No se encuentra el pago con id " + id);
        }

    }

    @Transactional(readOnly = true)
    public List buscarPorFecha(Date fecha) throws ErrorServicio {
        List<Pagos> ps = pagosRepo.findAll();
        List<Pagos> resultado= new ArrayList<>();

        Date hoy = fecha;

        for (int i = 0; i < ps.size(); i++) {
            Pagos pr = ps.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                resultado.add(pr);

            }
        }

        return resultado;
    }

    @Transactional(readOnly = true)
    public List buscarPorFechayDivisa(Date fecha, Divisa divisa) throws ErrorServicio {
        List<Pagos> ps = pagosRepo.findAll();
        List<Pagos> resultado= null;

        Date hoy = fecha;

        for (int i = 0; i < ps.size(); i++) {
            Pagos pr = ps.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate() && pr.getDivisa()== divisa) {
                resultado.add(pr);

            }
        }

        return resultado;
    }

    public Double totalFechayDivisa(Date fecha, Divisa divisa) throws ErrorServicio {
        List<Pagos> ps = pagosRepo.findAll();
        Double total = 0.0;

        Date hoy = fecha;

        for (int i = 0; i < ps.size(); i++) {
            Pagos pr = ps.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate() && pr.getDivisa()== divisa) {
                total= total + pr.getMonto();

            }
        }

        return total;
    }

    public Double totalFechayUsuario(Long id) throws ErrorServicio {
        List<Pagos> ps = pagosRepo.buscarPagoIdUsuario(id);
        System.out.println(ps.size());
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < ps.size(); i++) {
            Pagos pr = ps.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total= total + pr.getMonto();
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalFechayDivisaPorUsuario(Date fecha, Divisa divisa, Long user_id) throws ErrorServicio {
        List<Pagos> ps = pagosRepo.buscarPagoIdUsuarioyDivisa(divisa,user_id);
        Double total = 0.0;

        Date hoy = fecha;

        for (int i = 0; i < ps.size(); i++) {
            Pagos pr = ps.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total= total + pr.getMonto();

            }
        }

        return total;
    }

    public Date utilFecha( Date fecha){
        Date hoy = fecha;
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;
        int hora = hoy.getHours();
        int min= hoy.getMinutes();

        LocalDateTime localDate= LocalDateTime.of(anoh,mesh, diah, hora, min );

        ZoneId BsAs= ZoneId.of("America/Buenos_Aires");
        ZonedDateTime bsDateTime= localDate.atZone(BsAs);
        return fecha;

    }
}
