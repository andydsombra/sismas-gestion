package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.*;
import sysmas.Enumeraciones.*;
import sysmas.Errores.ErrorServicio;

import sysmas.Repositorios.ProductosRepository;
import sysmas.model.ApiModelDTO;

import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductosServicio {

    @Autowired
    private ProductosRepository productoRepositorio;

    @Autowired
    private StockService stockServ;

    @Autowired
    private PreciosService preciosServ;

    @Autowired
    private DatosService datosServ;


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Productos cargarProducto(String barras, String cod, String nombre, Cat cat, String tipo, String color, String descripcion, Long stock, Long foto, Long precios ) throws ErrorServicio {


        Productos producto = new Productos();

        validarCodigo(barras);
        validarCodInterno(cod);

        producto.setBarras(barras);
        producto.setNombre(nombre);
        producto.setCod_interno(cod);
        producto.setCat(cat);
        producto.setEstado(Estados.ACTIVO);
        producto.setDescrip(descripcion);
        producto.setStock_id(stock);
        producto.setTipo(tipo);
        producto.setColor(color);
        producto.setPrecios_id(precios);
        if(foto == null || foto == 0L || foto.toString().isEmpty() ){
            producto.setFotos_id(34010L);
        }else{
            producto.setFotos_id(foto);
        }
        producto.setNuevo(Nuevo.NUEVO);



        return productoRepositorio.save(producto);
    }

    public void validar(Foto foto) throws ErrorServicio {
        if (foto == null || foto.toString().isEmpty()) {
            throw new ErrorServicio("La foto está vacia");
        }
    }

    public void validarCodigo(String id) throws ErrorServicio {

        Productos p = productoRepositorio.buscarPorBarra(id);
        if (p != null) {
            throw new ErrorServicio("El código de barras ya está registrado");
        }

    }

    public void validarCodInterno(String id) throws ErrorServicio {
        Productos p = new Productos();

       try {
            p = productoRepositorio.buscarPorCodigoInterno(id);
        } catch (Exception e) {
        }
        if (p != null) {
            if (p.getCod_interno().equals(id)) {
                throw new ErrorServicio("El código interno ya está registrado");
            }
        }

    }

    public void validar(String id, String nombre, String tipo, String color, Cat cat) throws ErrorServicio {
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Productos modificarProducto(Productos prod) throws ErrorServicio {

        //validar(prod.getId(), prod.getNombre(), prod.getTipo(), prod.getColor(), prod.getCat());

        Productos producto = new Productos();

        Optional<Productos> resp= productoRepositorio.findById(prod.getId());
        if(resp.isPresent()){
            producto= resp.get();

        if (!prod.getBarras().equals(producto.getBarras()) && prod.getBarras() != null) {
            validarCodigo(prod.getBarras());
            producto.setBarras(prod.getBarras());
        }

        if (!prod.getCod_interno().equals(prod.getCod_interno()) && prod.getCod_interno() != null) {
            validarCodInterno(prod.getCod_interno());
            producto.setBarras(prod.getCod_interno());
        }

        String descripcion = null;
        if(prod.getDescrip() == null || prod.getDescrip().isEmpty()){
            descripcion= producto.getDescrip();
        }

        producto.setId(resp.get().getId());
        producto.setBarras(prod.getBarras());
        producto.setNombre(prod.getNombre());
        producto.setCod_interno(prod.getCod_interno());
        producto.setCat(prod.getCat());
        producto.setDescrip(descripcion);
        producto.setNuevo(Nuevo.NONUEVO);
        producto.setStock_id(prod.getStock_id());
        producto.setTipo(prod.getTipo());
        producto.setColor(prod.getColor());
        producto.setPrecios_id(prod.getPrecios_id());
        producto.setFotos_id(prod.getFotos_id());
        producto.setNuevo(Nuevo.NONUEVO);
    }
        return productoRepositorio.save(producto);

    }


    @Transactional(readOnly = true)
    public Productos buscarProducto(String id) {
        Productos producto = new Productos();

        Optional<Productos> resp= productoRepositorio.findById(id);
        if(resp.isPresent()){
            producto= resp.get();
        }

        return  producto;
    }

    @Transactional(readOnly = true)
    public Long buscarStockProducto(String id) {
        Long stock_id= 0L;
        Productos producto = new Productos();

        Optional<Productos> resp= productoRepositorio.findById(id);
        if(resp.isPresent()){
            producto= resp.get();
            stock_id= producto.getStock_id();
        }

        return  stock_id;
    }

    @Transactional(readOnly = true)
    public List buscarPorCat(Cat cat) throws ErrorServicio {
        List<Productos> lista = productoRepositorio.buscarPorCat(cat);
        ArrayList<TiposApi> p = new ArrayList();
        ArrayList<String> tiposString = new ArrayList();

        String tipo = "";

        if (tiposString.size() == 0) {
            TiposApi t = new TiposApi();
            t.setId(0);
            t.setNombre("Todos");
            t.setProductos(setearProductos(lista));
            p.add(t);
        }

        for (Productos pe : lista) {
            tipo = pe.getTipo();
            int cont = 0;

            for (int i = 0; i < tiposString.size(); i++) {

                String l = tiposString.get(i);
                if (tipo.equals(l)) {
                    cont = 1;
                    break;
                } else {
                    cont = 0;
                }

            }

            if (cont == 0) {
                tiposString.add(tipo);
            }
        }

        for (int i = 0; i < tiposString.size(); i++) {
            TiposApi t = new TiposApi();
            t.setId(i + 1);
            t.setNombre(tiposString.get(i));
            t.setProductos(setearProductos(productoRepositorio.buscarPorTipo(tiposString.get(i))));
            p.add(t);
        }

        return p;
    }

    public ArrayList setearProductos(List<Productos> pro) throws ErrorServicio {
        ArrayList<ApiModelDTO> p = new ArrayList();

        for (Productos pr : pro) {
            ApiModelDTO a = new ApiModelDTO();
            a.setId(pr.getId());
            Stock stock= stockServ.encotrarStock(pr.getStock_id());
            a.setStock(stock.getStock());
            a.setCantMinima(stock.getMinimo());
            a.setNombre(pr.getNombre());
            a.setCategoria(pr.getCat().toString());
            a.setTipo(pr.getTipo());
            a.setBarras(pr.getBarras());
            a.setColor(pr.getColor());
            a.setCodInterno(pr.getCod_interno());
            Precios precio= preciosServ.buscarPorId(pr.getPrecios_id());
            a.setPrecioUnitario(precio.getPublico());
            a.setPrecioMayorista(precio.getMay1());
            a.setPrecioMayorista2(precio.getMay2());
            a.setDescripcion(pr.getDescrip());
            a.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + pr.getId());
            p.add(a);
        }

        return p;
    }


    public List setearApiModel(List<Productos> pro) throws ErrorServicio {

        ArrayList<ApiModel> lista = new ArrayList<>();

        for (Productos p : pro) {
            ApiModel model= new ApiModel();
            model.setId(p.getId());
            model.setBarras(p.getBarras());
            model.setTipo(p.getTipo());
            model.setCodInterno(p.getCod_interno());
            model.setColor(p.getColor());
            model.setCategoria(p.getCat().toString());
            model.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + p.getId());
            model.setNombre(p.getNombre());
            model.setPrecios(preciosServ.buscarPorId(p.getPrecios_id()));
            model.setStock(stockServ.encotrarStock(p.getStock_id()));
            lista.add( model);
        }
        return lista;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void borrarProducto(String id) {
        Productos producto = new Productos();

        Optional<Productos> resp= productoRepositorio.findById(id);
        if(resp.isPresent()){
            producto= resp.get();
           producto.setEstado(Estados.INACTIVO);
           productoRepositorio.save(producto);
        }
    }


    @Transactional(readOnly = true)
    public List<Productos> buscarTodos() {
        return productoRepositorio.findAll();
    }

    @Transactional(readOnly = true)
    public List buscarActivos() {
        List<Productos> prods= productoRepositorio.findAll();
        ArrayList<Productos> activos = new ArrayList<>();

        for (Productos p: prods) {
            if(p.getEstado().equals(Estados.ACTIVO)){
                activos.add(p);
            }
        }


        return activos;
    }
   /* @Transactional(readOnly = true)
    public Double totalMercaderia() {
        List<Productos> p = productoRepositorio.findAll();
        Double total = 0.0;

        for (int i = 0; i < p.size(); i++) {
            Productos pr = p.get(i);
            Double v = pr.getCantActual() * pr.getCosto();
            total = total + v;
        }

        return total;
    }*/



    @Transactional(readOnly = true)
    public Boolean existe(String id) {
        Boolean existe = false;
        try {
            Productos resultado = productoRepositorio.buscarPorCodigo(id);
            existe = true;
        } catch (Exception e) {
            existe = false;
        }

        return existe;
    }

}
