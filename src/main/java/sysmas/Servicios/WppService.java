
package sysmas.Servicios;

import sysmas.Entidades.Cliente;
import com.twilio.Twilio; 
import com.twilio.converter.Promoter; 
import com.twilio.rest.api.v2010.account.Message; 
import com.twilio.type.PhoneNumber; 
 
import java.net.URI; 
import java.math.BigDecimal;
import org.springframework.stereotype.Service;

@Service
public class WppService {
    
    public static final String ACCOUNT_SID = "AC004ee3066c83e6aff4b38267486ae710"; 
    public static final String AUTH_TOKEN = "80aebcd8841cedbbfd1ed223be920315"; 
 
    public void mandarWppMay(String mensaje) { 
        
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN); 
        Message message = Message.creator( 
                new com.twilio.type.PhoneNumber("whatsapp:+5492617588255"), 
                new com.twilio.type.PhoneNumber("whatsapp:+14155238886"),  
               "Nuevo pedido Mayorista")      
            .create(); 
        
        System.out.println(message.getSid()); 
        
    } 
    
    public void mandarWpp(){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN); 
        Message message = Message.creator( 
                new com.twilio.type.PhoneNumber("whatsapp:+5492617194713"), 
                new com.twilio.type.PhoneNumber("whatsapp:+14155238886"),  
               "Nuevo pedido Público")      
            .create(); 
        
        System.out.println(message.getSid()); 
    }
    
}
