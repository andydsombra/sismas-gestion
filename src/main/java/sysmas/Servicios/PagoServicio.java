package sysmas.Servicios;

import sysmas.Entidades.Pago;
import sysmas.Entidades.Usuario;
import sysmas.Entidades.Venta;
import sysmas.Enumeraciones.TipoPago;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.PagoRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PagoServicio {

    @Autowired
    private PagoRepository pagoRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Pago nuevoPago(String detalles, Double monto, Venta venta, TipoPago pago, Usuario user) throws ErrorServicio {
        Pago p = new Pago();

        validar(detalles, monto, venta, pago);
        p.setFecha(new Date());
        p.setMonto(monto);
        p.setVenta(venta);
        p.setPago(pago);
        p.setUsuario(user);

        return pagoRepo.save(p);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void elimianrPago(Long id) throws ErrorServicio{
        List<Pago> a = pagoRepo.buscarPorVenta(id);
        try {
            for (Pago pv : a) {
                
                pagoRepo.deleteById(pv.getId());
            }
        } catch (Error e) {
            throw new ErrorServicio(e.getMessage());
        }
    }

    public void validar(String detalles, Double monto, Venta venta, TipoPago pago) throws ErrorServicio {
        if (detalles == null || detalles.isEmpty()) {
            throw new ErrorServicio("Los detalles no pueden estar vacíos");
        }
        if (monto == null || monto.toString().isEmpty()) {
            throw new ErrorServicio("El monto no puede ser $0.0");
        }
        if (venta == null || venta.toString().isEmpty()) {
            throw new ErrorServicio("Venta vacía");
        }
        if (pago == null || pago.toString().isEmpty()) {
            throw new ErrorServicio("Debe indicar una forma de pago");
        }
    }

    @Transactional(readOnly = true)
    public Double totalPagoEfectivo() {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.EFECTIVO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoDebito() {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.DEBITO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoTarjeta() {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.CREDITO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoMercadoPago() {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.MERCADOPAGO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoTransferencia() {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.TRANSFERENCIA);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }

        return total;
    }
    
    @Transactional(readOnly = true)
    public Double totalPagos() {
        List<Pago> p = pagoRepo.findAll();
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                total = total + pr.getMonto();
            }
        }

        return total;
    }
    
    @Transactional(readOnly = true)
    public Double totalPagosUser(Long id) {
        List<Pago> p = pagoRepo.findAll();
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                 if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
            }
        }

        return total;
    }
    
   @Transactional(readOnly = true)
    public Double totalPagoEfectivoUser(Long id) {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.EFECTIVO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
               
            }
        }

        return total;
    }
    
    @Transactional(readOnly = true)
    public Pago totalUserManana(TipoPago tipo,Long id) {
        List<Pago> p = pagoRepo.buscarPagoId(tipo,id);
        Pago pa = new Pago();
        Double total= 0.0;
        pa.setPago(tipo);
        
        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(ve.getHours()<14){
                     total= total + pr.getMonto();
                }
               
            }
        }
        pa.setMonto(total);

        return pa;
    }
    
    @Transactional(readOnly = true)
    public Pago totalUserTarde(TipoPago tipo,Long id) {
        List<Pago> p = pagoRepo.buscarPagoId(tipo,id);
        Pago pa = new Pago();
        Double total= 0.0;
        pa.setPago(tipo);
        
        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(ve.getHours()>14){
                     total= total + pr.getMonto();
                }
               
            }
        }

        return pa;
    }

    @Transactional(readOnly = true)
    public Double totalPagoDebitoUser(Long id) {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.DEBITO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
               
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoTarjetaUser(Long id) {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.CREDITO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
               
            }
        }

        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoMercadoPagoUser(Long id) {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.MERCADOPAGO);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
               
            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double totalPagoTransferenciaUser(Long id) {
        List<Pago> p = pagoRepo.buscarPago(TipoPago.TRANSFERENCIA);
        Double total = 0.0;

        Date hoy = new Date();

        for (int i = 0; i < p.size(); i++) {
            Pago pr = p.get(i);
            Date ve = pr.getFecha();
            
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                if(pr.getUsuario().getId().equals(id)){
                     total = total + pr.getMonto();
                }
               
            }
        }
        return total;
    }
    
    
}
