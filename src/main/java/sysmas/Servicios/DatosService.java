package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Datos;
import sysmas.Enumeraciones.Estados;
import sysmas.Repositorios.DatosRepository;

import java.util.Date;
import java.util.Optional;

@Service
public class DatosService {

    @Autowired
    private DatosRepository datosRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Datos nuevoDato( Date alta){

        Datos dato = new Datos();

        dato.setAlta(alta);
        dato.setEstado(Estados.ACTIVO);

        return datosRepo.save(dato);
    }

    @Transactional(readOnly = true)
    public Datos buscar(Long id){
        Datos dato= new Datos();
        Optional<Datos> resp = datosRepo.findById(id);
        if (resp.isPresent()){
            dato= resp.get();
        }

        return dato;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void baja(Long id){


        Optional<Datos> resp= datosRepo.findById(id);
        if(resp.isPresent()){
            Datos dato = resp.get();

            dato.setBaja(new Date());
            dato.setEstado(Estados.INACTIVO);

            datosRepo.save(dato);

        }

    }

}
