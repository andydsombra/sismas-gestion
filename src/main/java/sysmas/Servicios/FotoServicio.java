
package sysmas.Servicios;

import sysmas.Entidades.Foto;
import sysmas.Repositorios.FotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
public class FotoServicio {
    @Autowired
    private FotoRepository fotoRepositorio;
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Foto cargarArchivo(MultipartFile archivo){
        Foto foto = new Foto();
        
        if (archivo != null && !archivo.isEmpty()) {
            try {
                
                foto.setMime(archivo.getContentType());
                foto.setNombre(archivo.getName());
                foto.setContenido(archivo.getBytes());

                return fotoRepositorio.save(foto);
   
            } catch (Exception e) { 
                System.out.println(e.getMessage());
                return null;
            }
        }else{
            System.out.println("Archivo llegó nulo");
            return null;
        }
        
        
    }

    @Transactional(readOnly = true)
    public Foto encontrarFoto(Long id){
        Foto foto = new Foto();
        Optional<Foto> reso= fotoRepositorio.findById(id);
        if (reso.isPresent()){
            foto = reso.get();
        }

        return foto;
    }
}

