package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Arqueo_Usuarios;
import sysmas.Entidades.Arqueos;
import sysmas.Entidades.Pagos;
import sysmas.Entidades.Usuario;
import sysmas.Enumeraciones.Divisa;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.Arqueo_usuarioRepository;

import java.util.Date;
import java.util.List;

@Service
public class Arqueo_usuarioServicio {

    @Autowired
    private Arqueo_usuarioRepository arqueoRepo;

    @Autowired
    private PagosService pagosServ;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Arqueo_Usuarios nuevoArqUsuario( Long usuario_id) throws ErrorServicio {
        Arqueo_Usuarios arq= new Arqueo_Usuarios();

        Date fecha= new Date();
        Double credito=pagosServ.totalFechayDivisaPorUsuario(fecha, Divisa.CREDITO, usuario_id);
        Double debito=pagosServ.totalFechayDivisaPorUsuario(fecha, Divisa.DEBITO, usuario_id);
        Double efectivo=pagosServ.totalFechayDivisaPorUsuario(fecha, Divisa.EFECTIVO, usuario_id);
        Double mercadoPago=pagosServ.totalFechayDivisaPorUsuario(fecha, Divisa.MERCADOPAGO, usuario_id);
        Double transferencia=pagosServ.totalFechayDivisaPorUsuario(fecha, Divisa.TRANSFERENCIA, usuario_id);

        arq.setFecha(fecha);
        arq.setUsuario_id(usuario_id);
        arq.setCredito(credito);
        arq.setDebito(debito);
        arq.setEfectivo(efectivo);
        arq.setTotal((credito+debito+efectivo+transferencia+mercadoPago));
        arq.setMercado_pago(mercadoPago);
        arq.setTransferencia(transferencia);
        arq.setDiferencia(0.0);

        return arqueoRepo.save(arq);
    }

    @Transactional(readOnly = true)
    public List buscarArqUsuarioPorFecha(Long id, Date fecha){
        List<Arqueo_Usuarios> user= arqueoRepo.buscarPorId(id);
        List<Arqueo_Usuarios> resultado= null;

        Date hoy = fecha;

        for (int i = 0; i < user.size(); i++) {
            Arqueo_Usuarios pr = user.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                resultado.add(pr);

            }
        }


        return resultado;
    }

    @Transactional(readOnly = true)
    public Double totalArqUsuarioPorFecha(Long id) throws ErrorServicio {

        return pagosServ.totalFechayUsuario(id);
    }


}
