package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Foto;
import sysmas.Entidades.Producto;
import sysmas.Entidades.Proveedor;
import sysmas.Enumeraciones.Cat;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.ProductoRepository;

import java.util.List;

@Service
public class ProductoServicio {

    @Autowired
    private ProductoRepository productoRepositorio;

    FotoServicio fs = new FotoServicio();

    ProveedorServicio ps = new ProveedorServicio();

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Producto cargarProducto(String barras, String cod, String nombre, Cat cat, String tipo, String color, String descripcion, Integer cantActual, Integer cantIdeal, Foto foto, Proveedor proveedor, Double costo, Double precioM1, Double precioM2, Double precioUnitario, Double precioDolar, Integer cantDepo, Integer cantLocal, Integer cantM) throws ErrorServicio {

        Producto producto = new Producto();

        validar(barras, nombre, tipo, color, cantActual, cantIdeal, costo, precioDolar, cat);

        validarCodigo(barras);
        validarCodInterno(cod);

        producto.setBarras(barras);
        producto.setCodInterno(cod);
        producto.setNombre(nombre);
        producto.setTipo(tipo);
        producto.setColor(color);
        producto.setDescripcion(descripcion);
        producto.setCantActual(cantActual);
        producto.setCantIdeal(cantIdeal);
        producto.setCosto(costo);
        producto.setPrecioMay1(precioM1);
        producto.setPrecioMay2(precioM2);
        producto.setPrecioUnitario(precioUnitario);
        producto.setPrecioUnitarios(costo + (costo / 100) * 35);
        producto.setPrecioMay1s(costo + (costo / 100) * 22);
        producto.setPrecioMay2s(costo + (costo / 100) * 15);
        producto.setPrecioDolar(precioDolar);
        producto.setCategoria(cat);
        producto.setCantDeposito(cantDepo);
        producto.setCantLocal(cantLocal);
        producto.setCantMinima(cantM);
        producto.setPreVenta(0);
        validar(foto);
        producto.setFoto(foto);
        producto.setProveedor(proveedor);

        return productoRepositorio.save(producto);
    }

    public void validar(Foto foto) throws ErrorServicio {
        if (foto == null || foto.toString().isEmpty()) {
            throw new ErrorServicio("La foto está vacia");
        }
    }

    public void validarCodigo(String id) throws ErrorServicio {

        Producto p = productoRepositorio.buscarPorBarra(id);
        if (p != null) {
            throw new ErrorServicio("El código de barras ya está registrado");
        }

    }

    public void validarCodInterno(String id) throws ErrorServicio {
        Producto p = new Producto();
        try {
            p = productoRepositorio.buscarPorCodigoInterno(id);
        } catch (Exception e) {
        }
        if (p != null) {
            if (p.getCodInterno().equals(id)) {
                throw new ErrorServicio("El código interno ya está registrado");
            }
        }

    }

    public void validar(String id, String nombre, String tipo, String color, Integer cantActual, Integer cantIdeal, Double costo, Double precioDolar, Cat cat) throws ErrorServicio {
        if (id == null || id.isEmpty()) {
            throw new ErrorServicio("Falta el código del producto");
        }
        if (costo == null || costo.toString().isEmpty()) {
            throw new ErrorServicio("El costo no puede ser $0");
        }
        if (nombre == null || nombre.isEmpty()) {
            throw new ErrorServicio("Debe indicar el nombre del producto");
        }
        if (cat == null || cat.toString().isEmpty()) {
            throw new ErrorServicio("Debe indicar la marca del producto");
        }
        if (tipo == null || tipo.isEmpty()) {
            throw new ErrorServicio("Debe indicar el tipo del producto");
        }
        if (color == null || color.isEmpty()) {
            throw new ErrorServicio("Debe indicar un color");
        }
        if (cantActual == null || cantActual.toString().isEmpty()) {
            throw new ErrorServicio("La cantidad Actual no puede ser 0");
        }
        if (cantIdeal == null || cantIdeal.toString().isEmpty()) {
            throw new ErrorServicio("La cantidad Ideal no puede ser 0");

        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void modificarProducto(String id, String barras, String cod, String nombre, String tipo, String color, String descripcion, Integer cantActual, Integer cantIdeal, Proveedor proveedor, Double costo, Double precioM1, Double precioM2, Double precioUnitario, Double precioDolar, Cat cat, Foto foto, Integer cantDepo, Integer cantLocal, Integer cantM) throws ErrorServicio {

        validar(id, nombre, tipo, color, cantActual, cantIdeal, costo, precioDolar, cat);

        Producto producto = productoRepositorio.buscarPorCodigo(id);

        if (!barras.equals(producto.getBarras()) && barras != null) {
            validarCodigo(barras);
            producto.setBarras(barras);
        }

        if (!cod.equals(producto.getCodInterno()) && cod != null) {
            validarCodInterno(cod);
            producto.setBarras(cod);
        }
        
        if(descripcion == null || descripcion.isEmpty()){
            descripcion= producto.getDescripcion();
        }
        producto.setNombre(nombre);
        producto.setTipo(tipo);
        producto.setColor(color);
        producto.setCantActual(cantActual);
        producto.setCantIdeal(cantIdeal);
        producto.setCosto(costo);
        producto.setPrecioMay1(precioM1);
        producto.setPrecioMay2(precioM2);
        producto.setPrecioUnitario(precioUnitario);
        producto.setPrecioUnitarios(costo + (costo / 100) * 35);
        producto.setPrecioMay1s(costo + (costo / 100) * 22);
        producto.setPrecioMay2s(costo + (costo / 100) * 15);
        producto.setPrecioDolar(precioDolar);
        producto.setCategoria(cat);
        producto.setCantDeposito(cantDepo);
        producto.setCantLocal(cantLocal);
        producto.setCantMinima(cantM);
        producto.setDescripcion(descripcion);
        producto.setFoto(foto);
        producto.setProveedor(proveedor);
        productoRepositorio.save(producto);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void igresarInterno(String id, Double costo, Double precioM1, Double precioM2, Double precioUnitario, Integer cantDepo, Integer cantLocal, Integer preVenta) throws ErrorServicio {

        Producto producto = productoRepositorio.buscarPorCodigo(id);

        Integer d = producto.getCantDeposito() + cantDepo;
        Integer l = producto.getCantLocal() + cantLocal;
        Integer p = producto.getPreVenta() - preVenta;
        Integer a = producto.getCantActual();

        if (p == 0) {
            a = (d + l);
        } else {
            a = a + preVenta;
        }

        producto.setCosto(costo);
        producto.setPrecioMay1(precioM1);
        producto.setPrecioMay2(precioM2);
        producto.setPrecioUnitario(precioUnitario);
        producto.setPrecioUnitarios(costo + (costo / 100) * 35);
        producto.setPrecioMay1s(costo + (costo / 100) * 22);
        producto.setPrecioMay2s(costo + (costo / 100) * 15);
        producto.setCantDeposito(d);
        producto.setCantLocal(l);
        producto.setCantActual(a);
        producto.setPreVenta(p);
        productoRepositorio.save(producto);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void reintegrarStock(String id, Integer cant) {
        Producto p = productoRepositorio.buscarPorBarra(id);

        if (p.getPreVenta() == 0) {
            Integer a = p.getCantDeposito() + p.getCantLocal() + cant;

            p.setCantLocal(p.getCantLocal() + cant);
            p.setCantActual(a);
        } else {
            if (p.getPreVenta() > cant) {
                Integer v = p.getPreVenta() - cant;
                p.setPreVenta(v);
                p.setCantActual(v * (-1));
            } else {
                if (p.getPreVenta() < cant) {
                    Integer v = cant - p.getPreVenta();
                    p.setPreVenta(0);
                    p.setCantLocal(v);
                    p.setCantActual(v);
                }
            }
        }
        productoRepositorio.save(p);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void borrarProducto(String id) {
        Producto p = productoRepositorio.buscarPorCodigo(id);

        productoRepositorio.delete(p);
    }

    /*
    Función de ejecución única
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void descripcion() {

        List<Producto> p = productoRepositorio.findAll();
        for (Producto pro : p) {
            pro.setDescripcion("-");
            productoRepositorio.save(pro);
        }
    }

    @Transactional(readOnly = true)
    public List buscarTodos() {
        return productoRepositorio.findAll();
    }

    @Transactional(readOnly = true)
    public Double totalMercaderia() {
        List<Producto> p = productoRepositorio.findAll();
        Double total = 0.0;

        for (int i = 0; i < p.size(); i++) {
            Producto pr = p.get(i);
            Double v = pr.getCantActual() * pr.getCosto();
            total = total + v;
        }

        return total;
    }

    @Transactional(readOnly = true)
    public List buscarPorTipo(String tipo) {
        return productoRepositorio.buscarPorTipo(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorNom(String tipo) {
        return productoRepositorio.buscarPorNom(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorCat(Cat tipo) {
        return productoRepositorio.buscarPorCat(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorTipoDepo(String tipo) {
        return productoRepositorio.buscarPorTipoDepo(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorNomDepo(String tipo) {
        return productoRepositorio.buscarPorNomDepo(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorCatDepo(Cat tipo) {
        return productoRepositorio.buscarPorCatDepo(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorTipoLocal(String tipo) {
        return productoRepositorio.buscarPorTipoLocal(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorNomLocal(String tipo) {
        return productoRepositorio.buscarPorNomLocal(tipo);
    }

    @Transactional(readOnly = true)
    public List buscarPorCatLocal(Cat tipo) {
        return productoRepositorio.buscarPorCatLocal(tipo);
    }

    @Transactional(readOnly = true)
    public Producto buscarPorId(String id) throws ErrorServicio {
        if (id == null || id.isEmpty()) {
            throw new ErrorServicio("Ingrese un código");
        }
        return productoRepositorio.buscarPorCodigo(id);
    }

    @Transactional(readOnly = true)
    public Producto buscarPorCodigo(String barras) throws ErrorServicio {
        if (barras == null || barras.isEmpty()) {
            throw new ErrorServicio("Ingrese un código");
        }
        return productoRepositorio.buscarPorBarra(barras);
    }

    @Transactional(readOnly = true)
    public Producto buscarPorCodigoInt(String interno) throws ErrorServicio {
        if (interno == null || interno.isEmpty()) {
            throw new ErrorServicio("Ingrese un código");
        }
        return productoRepositorio.buscarPorCodigoInterno(interno);
    }

    @Transactional(readOnly = true)
    public Boolean existe(String id) {
        Boolean existe = false;
        try {
            Producto resultado = productoRepositorio.buscarPorCodigo(id);
            existe = true;
        } catch (Exception e) {
            existe = false;
        }

        return existe;
    }

    @Transactional(readOnly = true)
    public List buscarPorNombre(String nombre) {
        return productoRepositorio.buscarPorNombre(nombre);
    }

    @Transactional(readOnly = true)
    public List buscarLocal() {
        Integer sitio = 0;
        return productoRepositorio.buscarLocal(sitio);
    }

    @Transactional(readOnly = true)
    public List buscarDeposito() {
        Integer sitio = 0;
        return productoRepositorio.buscarDeposito(sitio);
    }

    @Transactional(readOnly = true)
    public List buscarFaltante() {
        Integer sitio = 0;
        return productoRepositorio.buscarFaltante(sitio);
    }

    @Transactional(readOnly = true)
    public List buscarFaltanteUrgente() {
        Integer sitio = 0;
        return productoRepositorio.buscarFaltanteUrgente(sitio);
    }

    public void validarId(String id) throws ErrorServicio {
        if (id == null || id.isEmpty()) {
            throw new ErrorServicio("Ingrese un código");
        }
        Producto p = buscarPorCodigo(id);
        if (p == null || p.toString().isEmpty()) {
            throw new ErrorServicio("El producto no existe");
        }
    }

    @Transactional(readOnly = true)
    public List buscarPorProveedor(String id) {
        return productoRepositorio.buscarPorProveedor(id);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void restarCant(String id, Integer cant) {

        Producto producto = productoRepositorio.buscarPorCodigo(id);

        if (producto.getCantLocal() == 0) {
            if (producto.getCantDeposito() == 0) {
                Integer v = producto.getPreVenta() + cant;
                producto.setPreVenta(v);
                producto.setCantActual(v * (-1));
            } else {
                if (producto.getCantDeposito() > cant) {
                    Integer v = producto.getCantDeposito() - cant;
                    producto.setCantDeposito(v);
                    producto.setCantActual(v);

                } else {
                    if (producto.getCantDeposito() == cant) {
                        producto.setCantDeposito(0);
                        producto.setCantActual(0);
                    } else {
                        if (producto.getCantDeposito() < cant) {
                            Integer v = cant - producto.getCantDeposito();
                            producto.setCantDeposito(0);
                            producto.setCantActual(v * (-1));
                            producto.setPreVenta(v);
                        }
                    }
                }
            }
        } else {
            if (producto.getCantLocal() == cant) {
                producto.setCantLocal(0);
                producto.setCantActual(producto.getCantDeposito());
            } else {
                if (producto.getCantLocal() > cant) {
                    Integer v1 = producto.getCantActual() - cant;
                    Integer v2 = producto.getCantDeposito() + v1;
                    producto.setCantLocal(v1);
                    producto.setCantActual(v2);

                } else {
                    if (producto.getCantLocal() < cant) {
                        Integer v = cant - producto.getCantLocal();
                        producto.setCantLocal(0);

                        if (producto.getCantDeposito() == 0) {
                            Integer vx = producto.getPreVenta() + v;
                            producto.setPreVenta(vx);
                            producto.setCantActual(vx * (-1));
                        } else {
                            if (producto.getCantDeposito() > v) {
                                Integer vx = producto.getCantDeposito() - v;
                                producto.setCantDeposito(vx);
                                producto.setCantActual(vx);
                            } else {
                                if (producto.getCantDeposito() == v) {
                                    producto.setCantDeposito(0);
                                    producto.setCantActual(0);
                                } else {
                                    if (producto.getCantDeposito() < v) {
                                        Integer vx = v - producto.getCantDeposito();
                                        producto.setCantDeposito(0);
                                        producto.setCantActual(vx * (-1));
                                        producto.setPreVenta(vx);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        productoRepositorio.save(producto);

    }

    public Boolean exist(String id) {
        Boolean a = false;
        int c = 0;
        List<Producto> e = productoRepositorio.findAll();
        for (Producto producto : e) {
            if (producto.getBarras().equals(id)) {
                c = c + 1;
            }
        }
        if (c == 1) {
            a = true;
        }
        return a;
    }
}
