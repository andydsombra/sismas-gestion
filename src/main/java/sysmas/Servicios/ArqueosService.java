package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Arqueos;
import sysmas.Entidades.Usuario;
import sysmas.Enumeraciones.Divisa;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.ArqueoRepository;

import java.util.Date;
import java.util.List;

@Service
public class ArqueosService {

    @Autowired
    private ArqueoRepository arqueoRepo;

    @Autowired
    private PagosService pagosServ;

    @Autowired
    private OperacionesService opServ;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Arqueos nuevoArqueo(Date fecha, Usuario usuario) throws ErrorServicio {
        Arqueos arq= new Arqueos();

        arq.setFecha(fecha);
        arq.setUsuario_id(usuario.getId());
        arq.setCredito(pagosServ.totalFechayDivisa(fecha, Divisa.CREDITO));
        arq.setDebito(pagosServ.totalFechayDivisa(fecha, Divisa.DEBITO));
        arq.setEfectivo(pagosServ.totalFechayDivisa(fecha, Divisa.EFECTIVO));
        arq.setMercado_pago(pagosServ.totalFechayDivisa(fecha, Divisa.MERCADOPAGO));
        arq.setTransferencia(pagosServ.totalFechayDivisa(fecha, Divisa.TRANSFERENCIA));
        arq.setTotal(arq.getCredito()+arq.getDebito()+arq.getEfectivo()+arq.getMercado_pago()+ arq.getTransferencia());
        arq.setGanancia(opServ.ganaciasDiarias(fecha));

        return arqueoRepo.save(arq);

    }

    @Transactional(readOnly = true)
    public Arqueos buscarPorFecha(Date fecha){
        List<Arqueos> arqueos= arqueoRepo.findAll();

        Arqueos arq= new Arqueos();

        for (int i = 0; i < arqueos.size(); i++) {
            Arqueos pr = arqueos.get(i);
            Date ve = pr.getFecha();
            if (fecha.getYear() == ve.getYear() && fecha.getMonth() == ve.getMonth() && fecha.getDate() == ve.getDate()) {
                arq= pr;
            }
        }

        return arq;

    }

    @Transactional(readOnly = true)
    public List buscarTodos(){
        return arqueoRepo.findAll();

    }
}
