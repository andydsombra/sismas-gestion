package sysmas.Servicios;

import sysmas.Entidades.Cliente;
import sysmas.Entidades.Venta;
import sysmas.Enumeraciones.Tipo;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.ClienteRepository;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteServicio {

    @Autowired
    private ClienteRepository clienteRepositorio;
    
    @Autowired
    private VentaServicio ventaServicio;


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Cliente registrarCliente(String nombre, String direccion, String telefono, String mail, Tipo tipo, String doc, String numClient) throws ErrorServicio {

        System.out.println("Por validar");
        Validar(nombre, direccion, telefono, mail, tipo, doc, numClient);
        System.out.println("Validado");
        
        Cliente cliente = new Cliente();

        cliente.setNombre(nombre);
        cliente.setDireccion(direccion);
        cliente.setDocumento(doc);
        cliente.setActivo("SI");
        cliente.setMail(mail);
        cliente.setTelefono(telefono);
        cliente.setTipo(tipo);
        cliente.setNumClient(numClient);
        cliente.setAlta(new Date());

        return clienteRepositorio.save(cliente);
    }

    public void Validar(String nombre, String direccion, String telefono, String mail, Tipo tipo, String doc, String numClient) throws ErrorServicio {

        if (nombre == null || nombre.isEmpty()) {
            throw new ErrorServicio("El nombre del cliente no puede ser nulo");
        }

        if (mail == null || mail.isEmpty()) {
            throw new ErrorServicio("El mail del cliente no puede ser nulo");
        }

        if (direccion == null || direccion.isEmpty()) {
            throw new ErrorServicio("Debe ingresar una dirección");
        }

        if (telefono == null || telefono.isEmpty()) {
            throw new ErrorServicio("Debe ingresar su número de teléfono");
        }

        if (tipo == null || tipo.toString().isEmpty()) {
            throw new ErrorServicio("El tipo de cliente no puede ser nulo");
        }
        if (doc == null || doc.isEmpty()) {
            throw new ErrorServicio("Debe ingresar su Documento, Cuil o Cuit de teléfono");
        }
        
        if (numClient == null || numClient.toString().isEmpty()) {
            throw new ErrorServicio("Debe ingresar su Númeo de Cliente");
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Cliente modificarCliente(String id, String nombre, String direccion, String telefono, String mail, Tipo tipo, String doc, String numClient) throws ErrorServicio {

        Validar(nombre, direccion, telefono, mail, tipo, doc, numClient);
        
        Cliente cliente = null;

        Optional<Cliente> respuesta = clienteRepositorio.findById(id);
        if (respuesta.isPresent()) {

            cliente = respuesta.get();

            cliente.setNombre(nombre);
            cliente.setDireccion(direccion);
            cliente.setDocumento(doc);
            cliente.setMail(mail);
            cliente.setTelefono(telefono);
            cliente.setTipo(tipo);
            cliente.setNumClient(numClient);
        }

        return clienteRepositorio.save(cliente);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarCliente(String id){
        Optional<Cliente> respuesta = clienteRepositorio.findById(id);
        if (respuesta.isPresent()) {

            Cliente cliente = respuesta.get();

            cliente.setActivo("NO");
            clienteRepositorio.save(cliente);
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void modificarMailCliente(String id,String mail){
        Cliente cliente = null;
        
        Optional<Cliente> respuesta = clienteRepositorio.findById(id);
        if (respuesta.isPresent()) {

            cliente = respuesta.get();

            cliente.setMail(mail);

        }

        clienteRepositorio.save(cliente);
    }

    @Transactional(readOnly = true)
    public Cliente buscarPorId(String id) {
        Cliente cliente = null;
        Optional<Cliente> respuesta = clienteRepositorio.findById(id);
        if (respuesta.isPresent()) {

            cliente = respuesta.get();
        }

        return cliente;
    }
    
    @Transactional(readOnly = true)
    public List todosLosClientes(){
        return clienteRepositorio.buscarActivos("SI");
    }
    
    @Transactional(readOnly = true)
    public List buscarPorNombre(String nom){
        return clienteRepositorio.buscarPorNombre(nom);
    }
    
    @Transactional(readOnly = true)
    public Cliente buscarNombre(String nom){
        return clienteRepositorio.buscarNombre(nom);
    }
    
    @Transactional(readOnly = true)
    public Cliente buscarNumero(String nom){
        return clienteRepositorio.buscarNumero(nom);
    }
    
    @Transactional(readOnly = true)
    public Cliente buscarTel(String nom){
        return clienteRepositorio.buscarTel(nom);
    }
    
    @Transactional(readOnly = true)
    public List buscarCat(Tipo nom){
        if (nom.toString().equals("MAYORISTA")) {
            System.out.println("-------------------------------ES MAYORISTA");
            return clienteRepositorio.buscarTipoMay(Tipo.MAYORISTA);
            
        }else{
            System.out.println("------------------------------ES minorista");
            return clienteRepositorio.buscarTipoMin(Tipo.MINORISTA);
        }
        
    }
    
    @Transactional(readOnly = true)
    public ArrayList clientesDeudores(){
        ArrayList<Cliente> c= new ArrayList();
        List<Cliente> a= clienteRepositorio.findAll();
        
        for (int i = 0; i < a.size(); i++) {
            Cliente x=new Cliente();
            Cliente cl=a.get(i);
            Double d=deuda(cl.getId());
            int e=(int) Math.round(d);
            if(e!=0){
                x.setNumClient(cl.getNumClient());
                x.setNombre(cl.getNombre());
                x.setTelefono(cl.getTelefono());
                x.setMail(cl.getMail());
                x.setDeuda(d);
                c.add(x);
            }
        }
        
        
        return c;
    }
    
    public Double deuda(String id){
        Double deuda=0.0;
        List<Venta> v=ventaServicio.buscarPorCliente(id);
        for (int i = 0; i < v.size(); i++) {
            Venta ve=v.get(i);
            int e=(int) Math.round(ve.getResto());
            if(e!=0){
                deuda= deuda+ve.getResto();
            }
        }
        
        return deuda;
    }
}
