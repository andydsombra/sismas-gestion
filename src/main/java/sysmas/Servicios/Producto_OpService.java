package sysmas.Servicios;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.*;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.Producto_OpRepository;
import sysmas.model.ProductoOpModel;

import java.util.ArrayList;
import java.util.List;

@Service
public class Producto_OpService {

    @Autowired
    private Producto_OpRepository prodRepo;

    @Autowired
    private PreciosService preciosService;

    @Autowired
    private ProductosServicio productosServ;

    @Autowired StockService stockService;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Producto_Op nuevoProductoOp(String producto_id, Operaciones operacion, int cantidad, Double precio) throws ErrorServicio {
        Producto_Op p= new Producto_Op();


        p.setProducto_id(producto_id);
        p.setCantidad(cantidad);
        p.setPrecio(precio);
        p.setSubtotal(cantidad*precio);
        p.setOperacion_id(operacion.getId());
        p.setGanancia(obtenerGanancia(precio,cantidad,p.getProducto_id()));

        return prodRepo.save(p);
    }

    @Transactional(readOnly = true)
    public List prodOpPorOperacion(Long id){
        ArrayList<ProductoOpModel> prods = new ArrayList<>();
        List<Producto_Op> lista= prodRepo.buscarPorOp(id);

        for (int i = 0; i < lista.size(); i++) {
            Producto_Op prod= lista.get(i);
            Productos pr=productosServ.buscarProducto(prod.getProducto_id());
            ProductoOpModel model= new ProductoOpModel();
            model.setId(prod.getId());
            model.setProducto_id(prod.getProducto_id());
            model.setNombre(pr.getNombre());
            model.setTipo(pr.getTipo());
            model.setPrecio(prod.getPrecio());
            model.setCantidad(prod.getCantidad());
            model.setSubtotal(prod.getSubtotal());

            prods.add(model);
        }

        return prods;
    }

    @Transactional(readOnly = true)
    public List prodOpPorOperacionE(Long id){

        List<Producto_Op> lista= prodRepo.buscarPorOp(id);


        return lista;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarProdOp(String id) throws ErrorServicio {

        Producto_Op productoOp = encontrarProdOp(id);

        Productos prod=productosServ.buscarProducto(productoOp.getProducto_id());

        int cantidad= productoOp.getCantidad();
        try{
            prodRepo.deleteById(productoOp.getId());

            stockService.reintegrarStock(prod.getStock_id(), cantidad);
        }catch (Exception e){
            throw new ErrorServicio("No se pudo eliminar el producto" + e.getMessage());
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Producto_Op encontrarProdOp(String id) throws ErrorServicio {

        Producto_Op prod=prodRepo.getById(id);

        return prod;
    }

    private Double obtenerGanancia(Double precio, int cantidad, String producto_id) throws ErrorServicio {
        Productos prod= productosServ.buscarProducto(producto_id);

        Precios p= preciosService.buscarPorId(prod.getPrecios_id());

        Double ganancia = 0.0;

        Double gananciaUnidad = precio - p.getCosto();

        ganancia = gananciaUnidad * cantidad;

        return ganancia;
    }
}
