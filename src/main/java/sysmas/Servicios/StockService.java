package sysmas.Servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sysmas.Entidades.Stock;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.StockRepository;

import java.util.Optional;

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Stock nuevoStock(int ideal, int minimo, int stk, int sobreventa){
        Stock stock= new Stock();

        stock.setIdeal(ideal);
        stock.setStock(stk);
        stock.setMinimo(minimo);
        stock.setSobreventa(sobreventa);

        return stockRepo.save(stock);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Stock modificarStock(Long id,int stk, int ideal, int minimo, int sobreventa) throws ErrorServicio {
        Stock stock= new Stock();

        try {
            Optional<Stock> resp= stockRepo.findById(id);
            if (resp.isPresent()){
                stock= resp.get();

                stock.setId(resp.get().getId());
                stock.setIdeal(ideal);
                stock.setStock(stk);
                stock.setMinimo(minimo);
                stock.setSobreventa(sobreventa);

            }

            return stockRepo.save(stock);
        }catch (Exception e){
            throw  new ErrorServicio(" No se encuentra stock con el id " + id);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Stock restarStock(Long id,  int cant) throws ErrorServicio {
        Stock stock= new Stock();

        try {
            Optional<Stock> resp= stockRepo.findById(id);
            if (resp.isPresent()){
                stock= resp.get();
                stock.setId(id);

                ///Si el stock es = 0 entonces, se increnta la cantidad de productos sobrevendidos
                if (stock.getStock() == 0) {
                    stock.setSobreventa(stock.getSobreventa() + cant);
                }


                if (stock.getStock() > 0) {
                    int diferencia= stock.getStock() - cant;

                    if (diferencia == 0){
                        stock.setStock(0);
                    }
                    if (diferencia > 0){
                        stock.setStock(diferencia);
                    }
                    if (diferencia < 0){
                        stock.setStock(0);
                        stock.setSobreventa( diferencia * -1 );
                    }
                }

            }

            return stockRepo.save(stock);
        }catch (Exception e){
            throw  new ErrorServicio(" No se encuentra stock con el id " + id);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Stock reintegrarStock(Long id,  int cant) throws ErrorServicio {
        Stock stock= new Stock();

        try {
            Optional<Stock> resp= stockRepo.findById(id);
            if (resp.isPresent()){
                stock= resp.get();
                stock.setId(id);
                if (stock.getSobreventa() == 0) {
                    stock.setStock( stock.getStock() + cant);
                }
                if (stock.getSobreventa() > 0) {
                    int diferencia= cant - stock.getSobreventa();

                    if (diferencia == 0){
                        stock.setSobreventa(0);
                    }
                    if (diferencia >0){
                        stock.setSobreventa(0);
                        stock.setStock(diferencia);
                    }
                    if (diferencia < 0){
                        stock.setSobreventa(stock.getSobreventa() - cant);
                    }
                }

            }
            return stockRepo.save(stock);
        }catch (Exception e){
            throw  new ErrorServicio(" No se encuentra stock con el id " + id);
        }

    }

    @Transactional(readOnly = true)
    public Stock encotrarStock(Long id) throws ErrorServicio {
        Stock stock= new Stock();

        try {
            Optional<Stock> resp= stockRepo.findById(id);
            if (resp.isPresent()) {
                stock = resp.get();

            }
            return stock;
        }catch (Exception e){
            throw  new ErrorServicio(" No se encuentra stock con el id " + id);
        }
    }


}
