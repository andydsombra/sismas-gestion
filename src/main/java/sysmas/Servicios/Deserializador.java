package sysmas.Servicios;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sysmas.Entidades.*;
import sysmas.Enumeraciones.Cat;
import sysmas.Errores.ErrorServicio;

@Service
public class Deserializador {


    @Autowired
    private ProductosServicio prodServ;

    @Autowired
    private StockService stockServ;

    @Autowired
    private PreciosService preciosServ;



    public Productos editarProducto(String producto) throws ErrorServicio {
        Productos p= new Productos();

        JSONObject prod= new JSONObject(producto);

        try{

        JSONObject stock= prod.getJSONObject("stock");
        Stock s=stockServ.modificarStock(stock.getLong("id"),stock.getInt("stock"), stock.getInt("ideal"),stock.getInt("minimo"), stock.getInt("sobreventa"));

        JSONObject precios = prod.getJSONObject("precios");
        Precios pr = preciosServ.modificarPrecio(precios.getLong("id"), precios.getDouble("costo"), precios.getDouble("publico"), precios.getDouble("may1"), precios.getDouble("may2") );

        Productos respaldo= prodServ.buscarProducto(prod.getString("id"));

        p.setId(prod.getString("id"));
        p.setBarras(prod.getString("barras"));
        p.setNombre(prod.getString("nombre"));
        p.setCod_interno(prod.getString("codInterno"));
        p.setCat(Cat.valueOf(prod.getString("categoria")));
        p.setDescrip(prod.getString("descripcion"));
        p.setStock_id(stock.getLong("id"));
        p.setTipo(prod.getString("tipo"));
        p.setColor(prod.getString("color"));
        p.setPrecios_id(precios.getLong("id"));
        if(prod.getLong("foto") == 0L){
            p.setFotos_id(respaldo.getFotos_id());
        }else{
            p.setFotos_id(prod.getLong("foto"));
        }


         prodServ.modificarProducto(p);

        return p;
        }catch (Exception e){
           throw  new ErrorServicio("No se pudo modificar el producto" + e.getMessage());
        }
    }

    public Productos nuevoProducto(String producto) throws ErrorServicio {
        Productos p= new Productos();

        JSONObject prod= new JSONObject(producto);

        try{

            JSONObject stock= prod.getJSONObject("stock");
            JSONObject precios = prod.getJSONObject("precios");

            Stock s = stockServ.nuevoStock(stock.getInt("ideal"),stock.getInt("minimo"), stock.getInt("stock"),stock.getInt("sobreventa"));
            Precios precio= preciosServ.nuevoPrecio(precios.getDouble("costo"), precios.getDouble("publico"),precios.getDouble("may1"), precios.getDouble("may2"));

            p.setBarras(prod.getString("barras"));
            p.setNombre(prod.getString("nombre"));
            p.setCod_interno(prod.getString("codInterno"));
            p.setCat(Cat.valueOf(prod.getString("categoria")));
            p.setDescrip(prod.getString("descripcion"));
            p.setStock_id(s.getId());
            p.setTipo(prod.getString("tipo"));
            p.setColor(prod.getString("color"));
            p.setPrecios_id(precio.getId());
            p.setFotos_id(prod.getLong("foto"));


            Productos pN = prodServ.cargarProducto(p.getBarras(),p.getCod_interno(), p.getNombre(), p.getCat(),p.getTipo(),p.getColor(),p.getDescrip(),p.getStock_id(),p.getFotos_id(),p.getPrecios_id());

            return pN;
        }catch (ErrorServicio e){
            throw  new ErrorServicio("No se pudo crear el producto " + e);
        }
    }

}
