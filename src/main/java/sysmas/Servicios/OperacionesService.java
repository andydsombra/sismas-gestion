package sysmas.Servicios;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import sysmas.Entidades.*;
import sysmas.Enumeraciones.Estado;
import sysmas.Enumeraciones.Op;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.OperacionesRepository;
import sysmas.model.OperacionDTO;
import sysmas.model.OperacionModel;
import sysmas.model.ProductoOpModel;
import sysmas.model.Response;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OperacionesService {

    @Autowired
    private OperacionesRepository operacionesRepo;

    @Autowired
    private Producto_OpService prodService;

    @Autowired
    private ProductosServicio productosServ;

    @Autowired
    private StockService stockServ;

    @Autowired
    private UsuarioServicio userServ;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Operaciones nuevaOperacion(Op operacion, Long vendedor_id, Long cliente_id, Double total, Date fecha){
        Operaciones op= new Operaciones();

        op.setOperacion(operacion);
        op.setEstado(Estado.NUEVO);
        op.setFecha(fecha);
        op.setCliente_id(cliente_id);
        op.setVendedor_id(vendedor_id);
        op.setTotal(total);
        op.setResto(total);

        return operacionesRepo.save(op);

    }


    public OperacionModel operacion(String operacion) throws ErrorServicio {

        OperacionModel opModel= new OperacionModel();
        ArrayList<Producto_Op> prods= new ArrayList<>();


        try {
            JSONObject jsObject= new JSONObject(operacion);

            Date fecha= new Date(jsObject.getLong("fecha"));
            System.out.println(fecha);

            Op opera= Op.valueOf(jsObject.getString("operacion"));

            Operaciones op= nuevaOperacion(opera, jsObject.optLong("vendedor_id"), jsObject.optLong("cliente_id"), jsObject.getDouble("total"), fecha);

            opModel.setId(op.getId());
            opModel.setVendedor_id(op.getVendedor_id());
            opModel.setCliente_id(op.getCliente_id());
            opModel.setOperacion(op.getOperacion());
            opModel.setTotal(op.getTotal());
            opModel.setEstado(op.getEstado());
            opModel.setResto(op.getTotal());

            JSONArray array= jsObject.getJSONArray("productos");
            for (int i = 0; i < array.length(); i++) {
                JSONObject objet= array.getJSONObject(i);
                Producto_Op prod = prodService.nuevoProductoOp(objet.getString("producto_id"),op,objet.getInt("cantidad"), objet.getDouble("precio") );
                stockServ.restarStock(productosServ.buscarStockProducto(objet.getString("producto_id")),objet.getInt("cantidad"));
                prods.add(prod);
            }

            opModel.setProductos(prodService.prodOpPorOperacion(op.getId()));

            return opModel;
        }catch (ErrorServicio e){
            throw new ErrorServicio("Error al cargar la Operacion ");
        }

    }

    public OperacionModel actualizarOperacion(String operacion) throws ErrorServicio {

        OperacionModel opModel= new OperacionModel();
        JSONObject jsObject= new JSONObject(operacion);

        System.out.println("deserializado "+jsObject.getDouble("total"));

        try {

                List<ProductoOpModel> productos = prodService.prodOpPorOperacion(jsObject.getLong("id"));

                for (ProductoOpModel prod: productos) {
                    prodService.eliminarProdOp(prod.getId());
                }

                Op opera = Op.valueOf(jsObject.getString("operacion"));

                Operaciones op = editarOperacion(jsObject.getLong("id"), opera, jsObject.optLong("vendedor_id"), jsObject.optLong("cliente_id"), jsObject.getDouble("total"));

                opModel.setId(op.getId());
                opModel.setVendedor_id(op.getVendedor_id());
                opModel.setCliente_id(op.getCliente_id());
                opModel.setOperacion(op.getOperacion());
                opModel.setTotal(op.getTotal());
                opModel.setEstado(op.getEstado());
                opModel.setResto(op.getTotal());

                JSONArray array = jsObject.getJSONArray("productos");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject objet = array.getJSONObject(i);

                    Producto_Op prod = prodService.nuevoProductoOp(objet.getString("producto_id"), op, objet.getInt("cantidad"), objet.getDouble("precio"));
                    stockServ.restarStock(productosServ.buscarStockProducto(objet.getString("producto_id")), objet.getInt("cantidad"));
                }
                List<ProductoOpModel> productosNuevos= prodService.prodOpPorOperacion(jsObject.getLong("id"));
                opModel.setProductos(productosNuevos);


            return opModel;
        }catch (ErrorServicio e) {
            throw new ErrorServicio("Error al cargar la Operacion ");
        }

    }


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Operaciones editarOperacion(Long id,Op operacion, Long vendedor_id, Long cliente_id, Double total) throws ErrorServicio {
        Operaciones op= new Operaciones();

        System.out.println("editarOperacion"+total);

        try{
            Optional<Operaciones> resp= operacionesRepo.findById(id);

            if(resp.isPresent()){
                op= resp.get();
                op.setId(id);
                op.setOperacion(operacion);
                op.setEstado(Estado.NUEVO);
                op.setFecha(new Date());
                op.setVendedor_id(vendedor_id);
                op.setTotal(total);
                op.setResto(total);

            }
            System.out.println("Antes de guardar"+op.getTotal());
            return operacionesRepo.save(op);

        }catch (Exception e){
            throw new ErrorServicio("NO se encuentra la operacion con id " + id);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Operaciones editarResto(Long id, Double pago) throws ErrorServicio {
        Operaciones op= new Operaciones();

        try{
            Optional<Operaciones> resp= operacionesRepo.findById(id);

            if(resp.isPresent()){
                op= resp.get();

                Double resto= op.getResto() - pago;
                op.setResto(resto);

                if (resto<= 0.0){
                    op.setEstado(Estado.PAGADO);
                }else{
                    op.setEstado(Estado.PENDIENTE);
                }
            }

            return operacionesRepo.save(op);

        }catch (Exception e){
            throw new ErrorServicio("NO se encuentra la operacion con id " + id);
        }

    }

    @Transactional(readOnly = true)
    public OperacionModel buscarPorId(Long id) throws ErrorServicio {
        Operaciones op= new Operaciones();
        OperacionModel opModel= new OperacionModel();

        try{
            Optional<Operaciones> resp= operacionesRepo.findById(id);

            if(resp.isPresent()){
                op= resp.get();

                opModel.setId(op.getId());
                opModel.setVendedor_id(op.getVendedor_id());
                opModel.setCliente_id(op.getCliente_id());
                opModel.setOperacion(op.getOperacion());
                opModel.setTotal(op.getTotal());
                opModel.setEstado(op.getEstado());
                opModel.setFecha(op.getFecha().toString());
                opModel.setResto(op.getResto());

                List<ProductoOpModel> prods= prodService.prodOpPorOperacion(op.getId());
                opModel.setProductos(prods);
            }

            return opModel;

        }catch (Exception e){
            throw new ErrorServicio("NO se encuentra la operacion con id " + id);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarPorId(Long id) throws ErrorServicio {

        try{
            Optional<Operaciones> resp= operacionesRepo.findById(id);

            if(resp.isPresent()){
                List<ProductoOpModel> lista= prodService.prodOpPorOperacion(id);

                for (ProductoOpModel prod: lista) {
                    prodService.eliminarProdOp(prod.getId());
                }

                operacionesRepo.deleteById(id);
            }


        }catch (Exception e){
            throw new ErrorServicio("No se encuentra la operacion con id " + id);
        }

    }

    @Transactional(readOnly = true)
    public List buscarPorTipo(String tipoOperacion) throws ErrorServicio {

        Op tipo = Op.valueOf(tipoOperacion);

        ArrayList<OperacionDTO> operaciones = new ArrayList<>();

        try{

            List<Operaciones> ops= operacionesRepo.buscarPorTipo(tipo);

            for (int i = 0; i < ops.size() ; i++) {
                Operaciones op= ops.get(i);
                OperacionDTO operacionDTO= new OperacionDTO();

                operacionDTO.setId(op.getId());
                operacionDTO.setOperacion(op.getOperacion());
                operacionDTO.setEstado(op.getEstado());
                operacionDTO.setTotal(op.getTotal());
                operacionDTO.setFecha(op.getFecha());
                operacionDTO.setResto(op.getResto());
                operacionDTO.setVendedor(userServ.buscarPorId(op.getVendedor_id()));
                operacionDTO.setCliente(userServ.buscarPorId(op.getCliente_id()));

                operaciones.add(operacionDTO);
            }


            return operaciones;

        }catch (Exception e){
            throw new ErrorServicio("No se pueden cargar las operaciones de tipo "+ tipoOperacion);
        }

    }

    public List buscarUltimas(String tipoOperacion) throws ErrorServicio {

        Op tipo = Op.valueOf(tipoOperacion);

        ArrayList<OperacionDTO> operaciones = new ArrayList<>();

        try{

            List<Operaciones> ops= operacionesRepo.buscarPorTipo(tipo);

            for (int i = 0; i < ops.size() ; i++) {
                Operaciones op= ops.get(i);
                if(op.getEstado()== Estado.NUEVO || op.getEstado()== Estado.PENDIENTE) {
                    OperacionDTO operacionDTO = new OperacionDTO();

                    operacionDTO.setId(op.getId());
                    operacionDTO.setOperacion(op.getOperacion());
                    operacionDTO.setEstado(op.getEstado());
                    operacionDTO.setTotal(op.getTotal());
                    operacionDTO.setFecha(op.getFecha());
                    operacionDTO.setResto(op.getResto());
                    operacionDTO.setVendedor(userServ.buscarPorId(op.getVendedor_id()));
                    operacionDTO.setCliente(userServ.buscarPorId(op.getCliente_id()));

                    operaciones.add(operacionDTO);
                }
            }


            return operaciones;

        }catch (Exception e){
            throw new ErrorServicio("No se pueden cargar las operaciones de tipo "+ tipoOperacion);
        }

    }

    public List buscarPorFecha(Date fecha){

        List<Operaciones> operaciones= operacionesRepo.buscarPorTipo(Op.VENTA);
        List<Long> deHoy= new ArrayList<>();

        Date hoy = utilFecha(fecha);
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;


        for (int i = 0; i < operaciones.size(); i++) {
            Operaciones pr = operaciones.get(i);
            Date date= utilFecha(pr.getFecha());
            int dia = date.getDate();
            int mes = date.getMonth() + 1;
            int ano = date.getYear() + 1900;


            if (anoh == ano && mesh == mes && diah == dia) {
                deHoy.add(pr.getId());
            }
        }

        return deHoy;
    }

    public List buscarPorTipoEcommerce() throws ErrorServicio {

        Op tipo = Op.ECOMMERCE;

        ArrayList<OperacionDTO> operaciones = new ArrayList<>();

        try{

            List<Operaciones> ops= operacionesRepo.buscarPorTipo(tipo);

            for (int i = 0; i < ops.size() ; i++) {
                Operaciones op= ops.get(i);
                if(op.getEstado()== Estado.NUEVO || op.getEstado()== Estado.VISTO) {
                    OperacionDTO operacionDTO = new OperacionDTO();

                    operacionDTO.setId(op.getId());
                    operacionDTO.setOperacion(op.getOperacion());
                    operacionDTO.setEstado(op.getEstado());
                    operacionDTO.setTotal(op.getTotal());
                    operacionDTO.setFecha(op.getFecha());
                    operacionDTO.setResto(op.getResto());
                    operacionDTO.setVendedor(userServ.buscarPorId(op.getVendedor_id()));
                    operacionDTO.setCliente(userServ.buscarPorId(op.getCliente_id()));

                    operaciones.add(operacionDTO);
                }
            }


            return operaciones;

        }catch (Exception e){
            throw new ErrorServicio("No se pueden cargar las operaciones de tipo "+ tipo);
        }

    }

    @Transactional(readOnly = true)
    public List buscarPorTipoyEstado(String tipoOperacion, String tipoEstado) throws ErrorServicio {

        Op tipo = Op.valueOf(tipoOperacion);
        Estado estado= Estado.valueOf(tipoEstado);

        ArrayList<OperacionDTO> operaciones = new ArrayList<>();

        try{

            List<Operaciones> ops= operacionesRepo.buscarPorTipoyEstado(tipo, estado);
            for (int i = 0; i < ops.size() ; i++) {
                Operaciones op= ops.get(i);
                OperacionDTO operacionDTO= new OperacionDTO();

                operacionDTO.setId(op.getId());
                operacionDTO.setOperacion(op.getOperacion());
                operacionDTO.setEstado(op.getEstado());
                operacionDTO.setTotal(op.getTotal());
                operacionDTO.setFecha(op.getFecha());
                operacionDTO.setResto(op.getResto());
                operacionDTO.setVendedor(userServ.buscarPorId(op.getVendedor_id()));
                operacionDTO.setCliente(userServ.buscarPorId(op.getCliente_id()));

                operaciones.add(operacionDTO);
            }


            return operaciones;

        }catch (Exception e){
            throw new ErrorServicio("No se pueden cargar las operaciones de tipo "+ tipoOperacion);
        }

    }

    @Transactional(readOnly = true)
    public List buscarTodas() throws ErrorServicio {

        ArrayList<OperacionDTO> operaciones = new ArrayList<>();

        try{
            List<Operaciones> ops= operacionesRepo.findAll();
            for (int i = 0; i < ops.size() ; i++) {
                Operaciones op= ops.get(i);
                OperacionDTO operacionDTO= new OperacionDTO();

                operacionDTO.setId(op.getId());
                operacionDTO.setOperacion(op.getOperacion());
                operacionDTO.setEstado(op.getEstado());
                operacionDTO.setTotal(op.getTotal());
                operacionDTO.setFecha(op.getFecha());
                operacionDTO.setResto(op.getResto());
                operacionDTO.setVendedor(userServ.buscarPorId(op.getVendedor_id()));
                operacionDTO.setCliente(userServ.buscarPorId(op.getCliente_id()));

                operaciones.add(operacionDTO);
            }


            return operaciones;

        }catch (Exception e){
            throw new ErrorServicio("No se pueden cargar las operaciones");
        }

    }

    @Transactional(readOnly = true)
    public Double ganaciasDiarias(Date fecha){
        List<Operaciones> operaciones= operacionesRepo.buscarPorTipo(Op.VENTA);

        Double ganacia= 0.0;

        Date hoy = fecha;

        for (int i = 0; i < operaciones.size(); i++) {
            Operaciones pr = operaciones.get(i);
            Date ve = pr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {
                List<Producto_Op> prod= prodService.prodOpPorOperacion(pr.getId());
                for (Producto_Op p: prod) {
                    ganacia= ganacia + p.getGanancia();
                }

            }
        }
        return ganacia;

    }


    public Date utilFecha( Date fecha){
        Date hoy = fecha;
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;
        int hora = hoy.getHours();
        int min= hoy.getMinutes();

        LocalDateTime localDate= LocalDateTime.of(anoh,mesh, diah, hora, min );

        ZoneId BsAs= ZoneId.of("America/Buenos_Aires");
        ZonedDateTime bsDateTime= localDate.atZone(BsAs);
        return fecha;

    }
}
