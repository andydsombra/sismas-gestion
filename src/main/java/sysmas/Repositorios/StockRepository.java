package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
}
