
package sysmas.Repositorios;

import sysmas.Entidades.Pedido;
import sysmas.Enumeraciones.Estado;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
        
     @Query("SELECT a FROM Pedido a WHERE a.estado Like :est ORDER BY id DESC")
    public List<Pedido> buscarOrdernado(@Param("est")Estado est);
    
    @Query("SELECT a FROM Pedido a Where a.id= :id")
    public Pedido buscarPorId(@Param("id")Long id);
    
    @Query("SELECT a FROM Pedido a Where a.proveedor.id= :prov")
    public List<Pedido> buscarPorProv(@Param("prov")String prov);
    
    @Modifying
    @Query("DELETE FROM Pedido a WHERE a.id= :eliminar")
    public void deletePedido(@Param("eliminar")Long eliminar);
}
