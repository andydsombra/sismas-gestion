package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Datos;

@Repository
public interface DatosRepository extends JpaRepository<Datos, Long> {
}
