package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Operaciones;
import sysmas.Enumeraciones.Estado;
import sysmas.Enumeraciones.Op;

import java.util.List;

@Repository
public interface OperacionesRepository extends JpaRepository<Operaciones, Long> {

    @Query("SELECT a FROM Operaciones a WHERE a.operacion LIKE :op")
    public List<Operaciones> buscarPorTipo(@Param("op")Op op);

    @Query("SELECT a FROM Operaciones a WHERE a.operacion LIKE :op AND a.estado LIKE :estado")
    public List<Operaciones> buscarPorTipoyEstado(@Param("op")Op op, @Param("estado")Estado estado);

}
