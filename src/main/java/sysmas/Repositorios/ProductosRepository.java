package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Producto;
import sysmas.Entidades.Productos;
import sysmas.Enumeraciones.Cat;

import java.util.List;

@Repository
public interface ProductosRepository extends JpaRepository<Productos, String> {


    @Query("SELECT a FROM Productos a WHERE a.barras LIKE :barra")
    public Productos buscarPorBarra(@Param("barra")String barra);

    @Query("SELECT a FROM Productos a WHERE a.id = :id")
    public Productos buscarPorCodigo(@Param("id")String id);

    @Query("SELECT a FROM Productos a WHERE a.cod_interno = :id")
    public Productos buscarPorCodigoInterno(@Param("id")String id);

    @Query("SELECT a FROM Productos a WHERE a.cat LIKE :cat")
    public List<Productos> buscarPorCat(@Param("cat")Cat cat);

    @Query("SELECT a FROM Productos a WHERE a.tipo LIKE %:tip%")
    public List<Productos> buscarPorTipo(@Param("tip")String tip);

}
