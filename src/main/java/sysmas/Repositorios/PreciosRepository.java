package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Precios;

import java.util.List;

@Repository
public interface PreciosRepository extends JpaRepository<Precios, Long> {

}
