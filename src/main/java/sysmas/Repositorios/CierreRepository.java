
package sysmas.Repositorios;

import sysmas.Entidades.CierreUsuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CierreRepository extends JpaRepository<CierreUsuario, String>{
    
    @Query("SELECT a FROM CierreUsuario a WHERE a.user.id LIKE :id")
    public List<CierreUsuario> buscarPorId(@Param("id")Long id);
    
}
