package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Arqueo_Usuarios;

import java.util.List;

@Repository
public interface Arqueo_usuarioRepository extends JpaRepository<Arqueo_Usuarios, String> {

    @Query("SELECT a FROM Arqueo_Usuarios a WHERE a.usuario_id LIKE %:id%")
    public List<Arqueo_Usuarios> buscarPorId(@Param("id")Long id);
}
