
package sysmas.Repositorios;

import sysmas.Entidades.Venta;
import sysmas.Enumeraciones.Estado;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VentaRepository extends JpaRepository<Venta, String>{
    
    @Query("SELECT a FROM Venta a WHERE a.estado LIKE :est ORDER BY id DESC")
    public List<Venta> buscarPorEstado(@Param("est")Estado est);

    
    @Query("SELECT a FROM Venta a Where a.id= :id")
    public Venta buscarPorId(@Param("id")Long id);
    
    @Query("SELECT a FROM Venta a WHERE a.estado LIKE :est ORDER BY a.id DESC")
    public List<Venta> buscarUltimas(@Param("est")Estado est);
    
    @Query("SELECT a FROM Venta a ORDER BY a.id DESC")
    public List<Venta> buscarTodas();
    
    @Query("SELECT a FROM Venta a WHERE a.cliente.id= :id ")
    public List<Venta> buscarPorCliente(@Param("id")String id);
    
    @Query("SELECT a FROM Venta a WHERE a.vendedor.id= :id ")
    public List<Venta> buscarPorUser(@Param("id")Long id);
    
    @Modifying
    @Query("DELETE FROM Venta a WHERE a.id= :eliminar")
    public void deleteVenta(@Param("eliminar")Long eliminar);
 
    
}
