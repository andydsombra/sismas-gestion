package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Producto_Op;

import java.util.List;

@Repository
public interface Producto_OpRepository extends JpaRepository<Producto_Op, String> {

    @Query("SELECT a FROM Producto_Op a WHERE a.operacion_id LIKE :op")
    public List<Producto_Op> buscarPorOp(@Param("op")Long op);
}
