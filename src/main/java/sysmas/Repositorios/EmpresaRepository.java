
package sysmas.Repositorios;

import sysmas.Entidades.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
    
   @Query("SELECT a FROM Empresa a Where a.id LIKE :id")
    public Empresa buscar(@Param("id")Integer id);
    
    
    
    
    
}
