package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Arqueos;

@Repository
public interface ArqueoRepository  extends JpaRepository<Arqueos,String > {
}
