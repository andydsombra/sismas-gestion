package sysmas.Repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sysmas.Entidades.Pago;
import sysmas.Entidades.Pagos;
import sysmas.Enumeraciones.Divisa;
import sysmas.Enumeraciones.TipoPago;

import java.util.List;

@Repository
public interface PagosRepository extends JpaRepository<Pagos,String> {

    @Query("SELECT a FROM Pagos a Where a.usuario_id LIKE :id AND a.divisa LIKE :divisa")
    public List<Pagos> buscarPagoIdUsuarioyDivisa(@Param("divisa") Divisa divisa, @Param("id")Long id );

    @Query("SELECT a FROM Pagos a Where a.usuario_id LIKE :id")
    public List<Pagos> buscarPagoIdUsuario(@Param("id")Long id );

    @Query("SELECT a FROM Pagos a Where a.divisa LIKE :divisa")
    public List<Pagos> buscarPagoIdDivisa(@Param("divisa") Divisa divisa);

}
