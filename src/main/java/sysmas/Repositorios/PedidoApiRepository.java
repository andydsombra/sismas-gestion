package sysmas.Repositorios;

import sysmas.Entidades.PedidoAPI;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoApiRepository extends CrudRepository<PedidoAPI, Long> {

    @Query("SELECT a FROM PedidoAPI a WHERE a.id LIKE :id")
    public PedidoAPI buscarPorID(@Param("id") Long id);

    @Query("SELECT a FROM PedidoAPI a")
    public List<PedidoAPI> buscarTodos();

}
