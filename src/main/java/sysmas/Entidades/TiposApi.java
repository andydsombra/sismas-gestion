
package sysmas.Entidades;

import sysmas.model.ApiModelDTO;

import java.util.List;

public class TiposApi {

    private int id;
    private String nombre;
    private List<ApiModelDTO> productos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ApiModelDTO> getProductos() {
        return productos;
    }

    public void setProductos(List<ApiModelDTO> productos) {
        this.productos = productos;
    }
}
    
    