package sysmas.Entidades;

import org.hibernate.annotations.GenericGenerator;
import sysmas.Enumeraciones.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Productos {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;
    private String barras;
    private Long precios_id;
    private Long fotos_id;
    private Estados estado;
    private Long stock_id;
    private String cod_interno;
    private String nombre;
    private String tipo;
    private Cat cat;
    private String descrip;
    private String color;
    private Nuevo nuevo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarras() {
        return barras;
    }

    public void setBarras(String barras) {
        this.barras = barras;
    }

    public Long getPrecios_id() {
        return precios_id;
    }

    public void setPrecios_id(Long precios_id) {
        this.precios_id = precios_id;
    }

    public Long getFotos_id() {
        return fotos_id;
    }

    public void setFotos_id(Long fotos_id) {
        this.fotos_id = fotos_id;
    }


    public Nuevo getNuevo() {
        return nuevo;
    }

    public void setNuevo(Nuevo nuevo) {
        this.nuevo = nuevo;
    }

    public Long getStock_id() {
        return stock_id;
    }

    public void setStock_id(Long stock_id) {
        this.stock_id = stock_id;
    }

    public String getCod_interno() {
        return cod_interno;
    }

    public Estados getEstado() {
        return estado;
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }

    public void setCod_interno(String cod_interno) {
        this.cod_interno = cod_interno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
