
package sysmas.Entidades;

import com.sun.istack.NotNull;
import sysmas.Enumeraciones.Estados;
import sysmas.Enumeraciones.User;
import sysmas.Enumeraciones.UserAccess;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String mail;
    @NotNull
    private String nombre;
    @NotNull
    private String apellido;
    private String telefono;
    private String direccion;
    private String documento;
    private String num_Cliente;
    @NotNull
    private Long datos_id;
    private Boolean activo;
    private Long fotos_id;
    @NotNull
    private String password;
    @Enumerated
    private User tipo;

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public User getTipo() {
        return tipo;
    }

    public void setTipo(User tipo) {
        this.tipo = tipo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Long getDatos_id() {
        return datos_id;
    }

    public void setDatos_id(Long datos_id) {
        this.datos_id = datos_id;
    }

    public Long getFotos_id() {
        return fotos_id;
    }

    public void setFotos_id(Long fotos_id) {
        this.fotos_id = fotos_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getClave() {
        return password;
    }

    public void setClave(String clave) {
        this.password = clave;
    }

    public String getNumCliente() {
        return num_Cliente;
    }

    public void setNumClient(String numCliente) {
        this.num_Cliente = numCliente;
    }
}
