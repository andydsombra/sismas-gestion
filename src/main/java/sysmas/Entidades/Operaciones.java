package sysmas.Entidades;

import jdk.jfr.Timestamp;
import sysmas.Enumeraciones.Estado;
import sysmas.Enumeraciones.Op;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Operaciones {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    private String arqueo_id;
    private Long vendedor_id;
    private Long cliente_id;
    private Double total;
    private Double resto;
    private Op operacion;
    private Estado estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getArqueo_id() {
        return arqueo_id;
    }

    public void setArqueo_id(String arqueo_id) {
        this.arqueo_id = arqueo_id;
    }

    public Long getVendedor_id() {
        return vendedor_id;
    }

    public void setVendedor_id(Long vendedor_id) {
        this.vendedor_id = vendedor_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getResto() {
        return resto;
    }

    public void setResto(Double resto) {
        this.resto = resto;
    }

    public Op getOperacion() {
        return operacion;
    }

    public void setOperacion(Op operacion) {
        this.operacion = operacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
