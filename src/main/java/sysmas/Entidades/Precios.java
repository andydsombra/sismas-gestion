package sysmas.Entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Precios {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double costo;
    private Double publico;
    private Double may1;
    private Double may2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Double getPublico() {
        return publico;
    }

    public void setPublico(Double publico) {
        this.publico = publico;
    }

    public Double getMay1() {
        return may1;
    }

    public void setMay1(Double may1) {
        this.may1 = may1;
    }

    public Double getMay2() {
        return may2;
    }

    public void setMay2(Double may2) {
        this.may2 = may2;
    }
}
