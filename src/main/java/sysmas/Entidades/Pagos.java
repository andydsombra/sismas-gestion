package sysmas.Entidades;

import org.hibernate.annotations.GenericGenerator;
import sysmas.Enumeraciones.Divisa;
import sysmas.Enumeraciones.TipoPago;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Pagos {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;

    @Temporal(TemporalType.DATE)
    private Date fecha;
    private Divisa divisa;
    private Double monto;
    private Long operacion_id;
    private Long usuario_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Divisa getDivisa() {
        return divisa;
    }

    public void setDivisa(Divisa divisa) {
        this.divisa = divisa;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Long getOperacion_id() {
        return operacion_id;
    }

    public void setOperacion_id(Long operacion_id) {
        this.operacion_id = operacion_id;
    }

    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }
}
