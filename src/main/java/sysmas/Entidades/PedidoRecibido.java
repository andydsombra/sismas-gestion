
package sysmas.Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PedidoRecibido implements Serializable {


    private ClienteModel cliente;

    private ArrayList<ApiModel> productos;
    
    private String estado;

    private Double total;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<ApiModel> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<ApiModel> productos) {
        this.productos = productos;
    }

    public ClienteModel getCliente() {
        return cliente;
    }

    public void setCliente(ClienteModel cliente) {
        this.cliente = cliente;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}

