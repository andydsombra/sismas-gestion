
package sysmas.Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;


public class VentaApi implements Serializable {
    
    private ArrayList<ApiModel> listProd;
    private Cliente cliente;
    private Double total;
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public ArrayList<ApiModel> getListProd() {
        return listProd;
    }

    public void setListProd(ArrayList<ApiModel> listProd) {
        this.listProd = listProd;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
        
}
