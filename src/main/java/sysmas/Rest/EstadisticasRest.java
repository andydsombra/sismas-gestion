package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sysmas.Entidades.ProdEstadisticas;
import sysmas.Servicios.EstadisticasService;
import sysmas.model.EstadisticaDTO;
import sysmas.model.Response;

import java.util.List;

@RestController
@RequestMapping("/api/estadisticas")
public class EstadisticasRest {

    @Autowired
    private EstadisticasService estadisticasServ;

    @GetMapping("/productosMasVendidos")
    public ResponseEntity<?> prodsMasVendidos(){
        try{
            List lista= estadisticasServ.prodMasVendidos();
            return  new ResponseEntity<>( lista , HttpStatus.OK);
        }catch ( Exception e) {
            return  new ResponseEntity<>( new Response("No se pudieron encontrar los productos mas vendidos"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/faltantes")
    public ResponseEntity<?> prodsFaltantes(){
        try{
            List lista= estadisticasServ.faltantes();
            return  new ResponseEntity<>( lista , HttpStatus.OK);
        }catch ( Exception e) {
            return  new ResponseEntity<>( new Response("No se pudieron encontrar los productos faltantes"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/general")
    public ResponseEntity<?> general(){
        try {
            EstadisticaDTO estadisticaDTO= new EstadisticaDTO();

            estadisticaDTO.setMercaderia(estadisticasServ.mercaderia());
            estadisticaDTO.setIngresos(estadisticasServ.ingresos());
            estadisticaDTO.setGanancias(estadisticasServ.ganacias());
            estadisticaDTO.setFaltantes(estadisticasServ.faltantes().size());

            return new ResponseEntity<>(estadisticaDTO, HttpStatus.OK);

        }catch (Exception e) {
            return  new ResponseEntity<>( new Response("No se pudieron generar los datos"), HttpStatus.BAD_REQUEST);
        }
    }

}
