package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sysmas.Entidades.Foto;
import sysmas.Entidades.Productos;
import sysmas.Errores.ErrorServicio;
import sysmas.Servicios.FotoServicio;
import sysmas.Servicios.ProductosServicio;
import sysmas.model.Response;

@RestController
@RequestMapping("")
public class FotoRest {

    @Autowired
    private FotoServicio fotoServ;

    @Autowired
    private ProductosServicio productosServ;

    @GetMapping("/foto/producto")
    public ResponseEntity<byte[]> fotoProducto(@RequestParam String id) {
        try {
            Productos producto = productosServ.buscarProducto(id);
            if(producto.getFotos_id()== null){
                throw new ErrorServicio("El producto no tiene foto");

            }
            Foto photo= fotoServ.encontrarFoto(producto.getFotos_id());

            byte[] foto= photo.getContenido();

            HttpHeaders headers= new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG);
            return new ResponseEntity<>(foto, headers, HttpStatus.OK);
        } catch (ErrorServicio e) {
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/foto")
    public ResponseEntity<byte[]> fotoProducto(@RequestParam Long id) {
        try {
            Foto photo= fotoServ.encontrarFoto(id);

            byte[] foto= photo.getContenido();

            HttpHeaders headers= new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG);
            return new ResponseEntity<>(foto, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/foto/nueva")
    public ResponseEntity<?> nuevaFoto(@RequestParam MultipartFile file){
        try{
            Foto foto = fotoServ.cargarArchivo(file);
            return new ResponseEntity<>(foto.getId(), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(new Response(e.getMessage()), HttpStatus.BAD_REQUEST );
        }



    }
}
