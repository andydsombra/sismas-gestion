package sysmas.Rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sysmas.Entidades.Pagos;
import sysmas.Enumeraciones.Divisa;
import sysmas.Servicios.OperacionesService;
import sysmas.Servicios.PagosService;
import sysmas.model.PagosModel;
import sysmas.model.Response;

@RestController
@RequestMapping("/api/pagos")
public class PagosRest {

    @Autowired
    private PagosService pagosServ;

    @Autowired
    private OperacionesService opServ;

    @PostMapping("/pago")
    public ResponseEntity<?> nuevoPago(@RequestBody PagosModel pago){
         try{
             pagosServ.nuevoPago(pago.getDivisa(), pago.getMonto(), pago.getUsuario_id(), pago.getOperacion_id());

             opServ.editarResto(pago.getOperacion_id(), pago.getMonto());

             return ResponseEntity.ok("Pago cargado");
         }catch (Exception e){
             return new ResponseEntity<>("No se pudo registrar el pago", HttpStatus.BAD_REQUEST);
         }

    }

    @PostMapping("/pagos")
    public ResponseEntity<?> nuevoPago(@RequestBody String listaPagos){

        try{
            JSONArray array= new JSONArray(listaPagos);
            for (int i = 0; i < array.length(); i++) {
                JSONObject objet = array.getJSONObject(i);

                Divisa divisa= Divisa.valueOf(objet.getString("divisa"));
                Pagos pago= pagosServ.nuevoPago(divisa, objet.getDouble("monto"), objet.getLong("usuario_id"), objet.getLong("operacion_id"));

                opServ.editarResto(pago.getOperacion_id(), pago.getMonto());
            }


            return new ResponseEntity<>(new Response( "Pago cargado"), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>("No se pudo registrar el pago", HttpStatus.BAD_REQUEST);
        }

    }
}
