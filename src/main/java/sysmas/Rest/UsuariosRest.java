package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sysmas.Entidades.Datos;
import sysmas.Entidades.Usuario;
import sysmas.Errores.ErrorServicio;
import sysmas.Servicios.UsuarioServicio;

import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UsuariosRest {

    @Autowired
    private UsuarioServicio usuarioServ;


    @GetMapping
    public ResponseEntity<?> usuarios(){

        return new ResponseEntity<>(usuarioServ.tipoAdmin(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> usuario(@PathVariable Long id){

        return new ResponseEntity<>(usuarioServ.buscarPorId(id), HttpStatus.OK);
    }

    @GetMapping("/clientes")
    public ResponseEntity<?> clientes(){
        return new ResponseEntity<>(usuarioServ.tipoCliente(), HttpStatus.OK);
    }

    @PostMapping("/nuevo")
    public ResponseEntity<?> nuevoUsario(@RequestBody Usuario usuario) throws ErrorServicio {

        Usuario user= usuarioServ.registarUsuario(usuario.getMail(), usuario.getNombre(), usuario.getApellido(), usuario.getClave(), usuario.getClave(), usuario.getDireccion(), usuario.getTelefono(), usuario.getDocumento(), null,null, usuario.getTipo());

        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @PostMapping("/editar")
    public ResponseEntity<?> editarUsario(@RequestBody Usuario usuario) throws ErrorServicio {

        Usuario user= usuarioServ.modificarUsuario(usuario.getId(), usuario.getMail(), usuario.getNombre(), usuario.getApellido(), usuario.getDireccion(), usuario.getTelefono(), usuario.getDocumento(), usuario.getTipo());

        return new ResponseEntity<>(user,HttpStatus.OK);
    }
}
