package sysmas.Rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sysmas.Entidades.Cliente;
import sysmas.Interfaces.ClienteInterface;
import sysmas.Repositorios.UsuarioRepository;
import sysmas.Seguridad.Servicio.JwtUtilService;
import sysmas.Servicios.APIServicio;
import sysmas.Servicios.ClienteServicio;
import sysmas.Servicios.ProductoServicio;
import sysmas.Servicios.UsuarioServicio;
import sysmas.model.AuthenticationReq;
import sysmas.model.TokenInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("")
public class AuthRest {
  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  UserDetailsService usuarioDetailsService;

  @Autowired
  private JwtUtilService jwtUtilService;
  
  @Autowired
  private UsuarioRepository clienteRepo;
  
  @Autowired
  private ProductoServicio productoServ;
  
  @Autowired
  private APIServicio apiServ;
  
  @Autowired
  private UsuarioServicio usuarioServ;
  
  @Autowired
  private ClienteServicio clienteServ;

  private static final Logger logger = LoggerFactory.getLogger(AuthRest.class);


  @GetMapping("/publico")
  public ResponseEntity<?> getMensajePublico() {
    var auth =  SecurityContextHolder.getContext().getAuthentication();
    logger.info("Datos del Usuario: {}", auth.getPrincipal());
    logger.info("Datos de los Permisos {}", auth.getAuthorities());
    logger.info("Esta autenticado {}", auth.isAuthenticated());

    ArrayList<Cliente> productos= (ArrayList<Cliente>) clienteServ.todosLosClientes();
    Map<String, String> mensaje = new HashMap<>();
    mensaje.put("contenido", "Hola Mundo");
    return ResponseEntity.ok(mensaje);
  }
  
  
  
  @GetMapping("/publico/clientes")
  public ArrayList<ClienteInterface> getClientesPublico() {
	  
	  
	  ArrayList<ClienteInterface> clientes = new ArrayList<>();
	  
	  clientes = (ArrayList<ClienteInterface>) apiServ.todosClientes();
	 /* for (Usuario cliente : clientes) {
		cliente.setTipo(UserAccess.CLIENTE);
		clienteRepo.save(cliente);
	}*/
	  
	  return clientes;
  }




  @PostMapping("/login")
  public ResponseEntity<TokenInfo> authenticate(@RequestBody AuthenticationReq authenticationReq) {
    logger.info("Autenticando al usuario {}", authenticationReq.getUsuario());

		authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(authenticationReq.getUsuario(),
            authenticationReq.getClave()));


    final UserDetails userDetails = usuarioDetailsService.loadUserByUsername(
        authenticationReq.getUsuario());

    final String jwt = jwtUtilService.generateToken(userDetails);

    TokenInfo tokenInfo = new TokenInfo(jwt);

    return ResponseEntity.ok(tokenInfo);
  }
}
