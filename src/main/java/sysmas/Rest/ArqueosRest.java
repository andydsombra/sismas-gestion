package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sysmas.Entidades.Arqueo_Usuarios;
import sysmas.Errores.ErrorServicio;
import sysmas.Repositorios.Arqueo_usuarioRepository;
import sysmas.Servicios.Arqueo_usuarioServicio;

@RestController
@RequestMapping("/arqueos")
public class ArqueosRest {

    @Autowired
    private Arqueo_usuarioServicio arqUSerServ;


    @GetMapping("/usuario")
    public ResponseEntity<?> arqueoUsuario(@RequestParam Long id) throws ErrorServicio {

        Arqueo_Usuarios arq= arqUSerServ.nuevoArqUsuario(id);

        return new ResponseEntity<>(arq, HttpStatus.CREATED);
    }

    @GetMapping("/usuario/total")
    public ResponseEntity<?> arqueoUsuarioTotal(@RequestParam Long id) throws ErrorServicio {

        Double arq= arqUSerServ.totalArqUsuarioPorFecha(id);

        return new ResponseEntity<>(arq, HttpStatus.CREATED);
    }

}
