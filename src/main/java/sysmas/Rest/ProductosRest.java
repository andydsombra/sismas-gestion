package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sysmas.Entidades.ApiModel;
import sysmas.Entidades.Productos;
import sysmas.Errores.ErrorServicio;
import sysmas.Servicios.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/productos")
public class ProductosRest {

    @Autowired
    private ProductosServicio productosServ;

    @Autowired
    private StockService stockServ;

    @Autowired
    private PreciosService preciosServ;

    @Autowired
    private Deserializador deserializador;

    @GetMapping
    public ResponseEntity<?> productos() throws ErrorServicio {
        ArrayList<ApiModel> models= new ArrayList<>();

        List<Productos> prod= productosServ.buscarActivos();



        for (int i = 0; i < prod.size() ; i++) {
            Productos p = prod.get(i);
            ApiModel model= new ApiModel();

            model.setId(p.getId());
            model.setBarras(p.getBarras());
            model.setTipo(p.getTipo());
            model.setCodInterno(p.getCod_interno());
            model.setColor(p.getColor());
            model.setCategoria(p.getCat().toString());
            model.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + p.getId());
            model.setNombre(p.getNombre());
            model.setPrecios(preciosServ.buscarPorId(p.getPrecios_id()));
            model.setStock(stockServ.encotrarStock(p.getStock_id()));

            models.add(model);
        }



        return new ResponseEntity<>(models, HttpStatus.OK);
    }

    @PostMapping("/editar")
    public ResponseEntity<?> editarProd(@RequestBody String producto) throws ErrorServicio {
        try{
            return new ResponseEntity<>(deserializador.editarProducto(producto), HttpStatus.ACCEPTED);
        }catch (ErrorServicio e){
            System.out.println(e);
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/nuevo")
    public ResponseEntity<?> nuevoProd(@RequestBody String producto) throws ErrorServicio {
        try{
            return new ResponseEntity<>(deserializador.nuevoProducto(producto), HttpStatus.CREATED);
        }catch (ErrorServicio e){
            System.out.println(e);
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/producto")
    public ResponseEntity<?> producto(@RequestParam String id){
        Productos p = productosServ.buscarProducto(id);
        ApiModel model= new ApiModel();
        try {

            model.setId(p.getId());
            model.setBarras(p.getBarras());
            model.setTipo(p.getTipo());
            model.setCodInterno(p.getCod_interno());
            model.setColor(p.getColor());
            model.setCategoria(p.getCat().toString());
            model.setFoto("http://universotunning.com.ar:8080/foto/producto?id=" + p.getId());
            model.setNombre(p.getNombre());
            model.setPrecios(preciosServ.buscarPorId(p.getPrecios_id()));
            model.setStock(stockServ.encotrarStock(p.getStock_id()));

            return new ResponseEntity(model, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("No se pudo encontrar el producto", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/eliminar")
    public ResponseEntity<?> eliminarProd(@RequestParam String id){
        try {
            productosServ.borrarProducto(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>("No se pudo elimianr el producto" ,HttpStatus.BAD_REQUEST);
        }
    }
}
