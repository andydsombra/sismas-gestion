package sysmas.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sysmas.Errores.ErrorServicio;
import sysmas.Servicios.*;
import sysmas.model.OperacionModel;
import sysmas.model.Response;

@RestController
@RequestMapping("/api/operaciones")
public class OperacionesRest {

    @Autowired
    private OperacionesService opServ;

    @PostMapping
    public ResponseEntity<?> nuevaOperacion(@RequestBody String operacion) throws ErrorServicio {

        try {
            return new ResponseEntity<OperacionModel>(opServ.operacion(operacion),HttpStatus.CREATED);
        }catch (ErrorServicio e){
            return new ResponseEntity<>( new Response("Error al cargar la Operacion: " + e.getMessage() ),HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/all")
    public ResponseEntity<?> operaciones() throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarTodas(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> operacion(@RequestParam Long id) throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarPorId(id), HttpStatus.OK);
    }

    @GetMapping("/delete")
    public ResponseEntity<?> operacionDelete(@RequestParam Long id) throws ErrorServicio {
        opServ.eliminarPorId(id);
        return new ResponseEntity<>( new Response("Operacion eliminada"), HttpStatus.ACCEPTED);
    }

    @GetMapping("/tipo")
    public ResponseEntity<?> operaciones(@RequestParam String tipo) throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarPorTipo(tipo), HttpStatus.OK);
    }

    @GetMapping("/ultimas")
    public ResponseEntity<?> operacionesUltimas(@RequestParam String tipo) throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarUltimas(tipo), HttpStatus.OK);
    }

    @GetMapping("/tipo/estado")
    public ResponseEntity<?> operaciones(@RequestParam String tipo, @RequestParam String estado) throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarPorTipoyEstado(tipo,estado), HttpStatus.OK);
    }

    @GetMapping("/tipo/ecommerce")
    public ResponseEntity<?> operacionesEcoomerce() throws ErrorServicio {
        return new ResponseEntity<>(opServ.buscarPorTipoEcommerce(), HttpStatus.OK);
    }

    @PostMapping("/actualizar")
    public ResponseEntity<?> actualizarOperacion(@RequestBody String operacion) throws ErrorServicio {
        try{
            return new ResponseEntity<>(opServ.actualizarOperacion(operacion), HttpStatus.OK);
        }catch (ErrorServicio e) {
            return new ResponseEntity<>(new Response("Error al actualizar la Operacion: " + e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
