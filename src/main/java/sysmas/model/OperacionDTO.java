package sysmas.model;

import sysmas.Entidades.Usuario;
import sysmas.Enumeraciones.Estado;
import sysmas.Enumeraciones.Op;

import java.util.Date;

public class OperacionDTO {
    private Long id;
    private Date fecha;
    private Usuario vendedor;
    private Usuario cliente;
    private Double resto;
    private Double total;
    private Op operacion;
    private Estado estado;

    public Double getResto() {
        return resto;
    }

    public void setResto(Double resto) {
        this.resto = resto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getVendedor() {
        return vendedor;
    }

    public void setVendedor(Usuario vendedor) {
        this.vendedor = vendedor;
    }

    public Usuario getCliente() {
        return cliente;
    }

    public void setCliente(Usuario cliente) {
        this.cliente = cliente;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Op getOperacion() {
        return operacion;
    }

    public void setOperacion(Op operacion) {
        this.operacion = operacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
