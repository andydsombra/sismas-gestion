package sysmas.model;

public class EstadisticaDTO {

    private Double mercaderia;
    private Double ingresos;
    private Double ganancias;
    private  int faltantes;

    public Double getMercaderia() {
        return mercaderia;
    }

    public void setMercaderia(Double mercaderia) {
        this.mercaderia = mercaderia;
    }

    public Double getIngresos() {
        return ingresos;
    }

    public void setIngresos(Double ingresos) {
        this.ingresos = ingresos;
    }

    public Double getGanancias() {
        return ganancias;
    }

    public void setGanancias(Double ganancias) {
        this.ganancias = ganancias;
    }

    public int getFaltantes() {
        return faltantes;
    }

    public void setFaltantes(int faltantes) {
        this.faltantes = faltantes;
    }
}
