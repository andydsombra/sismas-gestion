package sysmas.model;

import sysmas.Entidades.ProductoApi;
import sysmas.Entidades.Producto_Op;
import sysmas.Enumeraciones.Estado;
import sysmas.Enumeraciones.Op;

import java.util.Date;
import java.util.List;

public class OperacionModel {
    private Long id;
    private Long vendedor_id;

    private String fecha;
    private Long cliente_id;
    private Double total;
    private Op operacion;
    private Double resto;
    private Estado estado;
    private List<ProductoOpModel> productos;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getResto() {
        return resto;
    }

    public void setResto(Double resto) {
        this.resto = resto;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVendedor_id() {
        return vendedor_id;
    }

    public void setVendedor_id(Long vendedor_id) {
        this.vendedor_id = vendedor_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Op getOperacion() {
        return operacion;
    }

    public void setOperacion(Op operacion) {
        this.operacion = operacion;
    }

    public List<ProductoOpModel> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductoOpModel> productos) {
        this.productos = productos;
    }
}
