package sysmas.model;

import sysmas.Enumeraciones.Divisa;

public class PagosModel {

    private Divisa divisa;
    private Double monto;
    private Long operacion_id;
    private Long usuario_id;

    public Divisa getDivisa() {
        return divisa;
    }

    public void setDivisa(Divisa divisa) {
        this.divisa = divisa;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Long getOperacion_id() {
        return operacion_id;
    }

    public void setOperacion_id(Long operacion_id) {
        this.operacion_id = operacion_id;
    }

    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }
}
