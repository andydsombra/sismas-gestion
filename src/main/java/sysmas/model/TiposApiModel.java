package sysmas.model;

import sysmas.Entidades.ApiModel;

import java.util.List;

public class TiposApiModel {

        private int id;
        private String nombre;
        private List<ApiModel> productos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ApiModel> getProductos() {
        return productos;
    }

    public void setProductos(List<ApiModel> productos) {
        this.productos = productos;
    }
}
