package sysmas.model;

import java.io.Serializable;

public class TokenInfo implements Serializable {

  private static final long serialVersionUID = 1L;

  private final String token;

  public TokenInfo(String token) {
    this.token = token;
  }

  public String getToken() {
    return this.token;
  }
}
