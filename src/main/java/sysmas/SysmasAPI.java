package sysmas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class SysmasAPI {

	public static void main(String[] args) {
		SpringApplication.run(SysmasAPI.class, args);
		prueba();
	}

	public static void prueba(){
		System.out.println(new Date().toString());
	}

}
