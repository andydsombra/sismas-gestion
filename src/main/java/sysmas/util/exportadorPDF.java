/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmas.util;

import sysmas.Entidades.ProdVenta;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

@Component("/exportar")
public class exportadorPDF extends AbstractPdfView{

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
       List<ProdVenta> prod=(List<ProdVenta>) model.get("productos");
       
        PdfPTable tablaProd= new PdfPTable(6);
       
       prod.forEach(producto ->{
           tablaProd.addCell(producto.getNombre());
           tablaProd.addCell(producto.getTipo());
           tablaProd.addCell(producto.getCant().toString());
           tablaProd.addCell(producto.getPrecio().toString());
           tablaProd.addCell(producto.getSubtotal().toString());
       });
       
       document.add(tablaProd);
    }
    
}
