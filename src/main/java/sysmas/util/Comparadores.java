package sysmas.util;

import sysmas.Entidades.ProdEstadisticas;

import java.util.Comparator;

public class Comparadores {

    public static Comparator<ProdEstadisticas> ordernarMasVendido= new Comparator<ProdEstadisticas>() {
        @Override
        public int compare(ProdEstadisticas o1, ProdEstadisticas o2) {
            return o2.getCantidad().compareTo(o1.getCantidad());
        }
    };
}
