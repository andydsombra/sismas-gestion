
package sysmas.util;

import sysmas.Entidades.ApiModel;
import sysmas.Entidades.Producto;
import java.util.ArrayList;
import java.util.List;

public class utiles {
    
    public static ArrayList setearProductos(List<Producto> pro){
        ArrayList<ApiModel> p = new ArrayList();
        
        for (Producto pr : pro) {
            ApiModel a = new ApiModel();
            a.setId(pr.getId());
           //// a.setCantActual(pr.getCantActual());
           //// a.setCantMinima(pr.getCantMinima());
            a.setNombre(pr.getNombre());
            a.setCategoria(pr.getCategoria().toString());
            a.setTipo(pr.getTipo());
            a.setBarras(pr.getBarras());
            a.setColor(pr.getColor());
            a.setCodInterno(pr.getCodInterno());
           //// a.setPrecioUnitario(pr.getPrecioUnitario());
           //// a.setPrecioMayorista(pr.getPrecioMay1());
           //// a.setPrecioMayorista2(pr.getPrecioMay2());
            a.setDescripcion(pr.getDescripcion());
           // a.setFoto(pr.getFoto());
            p.add(a);
        }

        return p;
    }
}
