package com.Sismas.controlador;

import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.Empresa;
import com.Sismas.Entidades.PedidoAPI;
import com.Sismas.Entidades.ProdVenta;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Usuario;
import com.Sismas.Entidades.Venta;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.enumeraciones.Tipo;
import com.Sismas.enumeraciones.TipoPago;
import com.Sismas.enumeraciones.TipoVenta;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.PedidoApiRepository;
import com.Sismas.servicios.APIServicio;
import com.Sismas.servicios.ClienteServicio;
import com.Sismas.servicios.EmpresaServicio;
import com.Sismas.servicios.PagoServicio;
import com.Sismas.servicios.ProdVentaServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.UsuarioServicio;
import com.Sismas.servicios.VentaServicio;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class ControladorVentas {

    ArrayList<ProdVenta> productosventa = new ArrayList<>();
    Boolean Api= false;

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private VentaServicio ventaServicio;

    @Autowired
    private ClienteServicio clienteServicio;

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private ProdVentaServicio prServ;

    @Autowired
    private PagoServicio pagoServ;

    @Autowired
    private EmpresaServicio empresaServ;

    @Autowired
    private APIServicio apiServ;

    @Autowired
    private PedidoApiRepository pedRepo;

    @GetMapping("/venta")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String ventaMain(ModelMap model) {
        Api=false;
        List<Venta> v = ventaServicio.ultimasHoy();
        List<PedidoAPI> p = apiServ.pedidosApi();
        Double ventas = ventaServicio.ventasD();
        String ven = String.format("%,.2f", ventas);

        model.put("total", ven);
        model.put("ventas", v);
        model.put("pedidos", p);
        return "ventaMain.html";
    }

    /*-----------------Ventas------------------------*/
    @GetMapping("/ventaApi")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String ventaApi(ModelMap model, Long id) throws ErrorServicio {
        PedidoAPI p = apiServ.buscarPorID(id);
        p.setEstado(Estado.VISTO);
        apiServ.guardarPedido(p);
        Cliente cliente = p.getCliente();

        try {
            productosventa = apiServ.obtenerProductos(p.getProductos(), cliente);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ControladorVentas.class.getName()).log(Level.SEVERE, null, ex);
        }

        Double total = total();

        Producto producto = new Producto();

        model.put("tipo", TipoVenta.NORMAL);
        model.put("productos", productosventa);
        model.put("producto", producto);
        model.put("cliente", cliente);
        model.put("total", total);
        if(cliente.getTipo()== Tipo.MAYORISTA){
            Api=true;
            return "venta.html";
        }else{
            Api=true;
            return "venta.html";
        }

    }

    @GetMapping("/nuevaVenta")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String nuevaVenta(ModelMap model, String id) {
        Cliente cliente = clienteServicio.buscarPorId(id);

        Producto producto = new Producto();

        Double total = null;

        productosventa.clear();
        model.put("tipo", TipoVenta.NORMAL);
        model.put("productos", productosventa);
        model.put("producto", producto);
        model.put("cliente", cliente);
        model.put("total", total);
        return "venta.html";
    }

    @GetMapping("/actualVenta")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String actualVenta(ModelMap model) {

        return "venta.html";
    }

    @PostMapping("/buscarProducto")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String findProd(ModelMap model, @RequestParam String cId, @RequestParam String pId, @RequestParam TipoVenta tipo) throws ErrorServicio {

        Cliente cliente = clienteServicio.buscarPorId(cId);
        Double precio = 0.0;
        Producto producto = new Producto();

        try {
            productoServicio.validarId(pId);
            producto = productoServicio.buscarPorCodigo(pId);
            try {

                if (tipo.toString().equals("MAYORISTA2")) {
                    precio = producto.getPrecioMay2();

                } else {
                    if (cliente.getTipo().toString().equals("MINORISTA")) {
                        precio = producto.getPrecioUnitario();
                    } else {
                        precio = producto.getPrecioMay1();
                    }
                }
                model.put("producto", producto);
            } catch (Exception e) {

            }

            Double total = total();

            if (producto.getCantLocal() == 0) {

            }

            model.put("tipo", TipoVenta.values());
            model.put("precio", precio);
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "venta.html";
        } catch (ErrorServicio e) {
            Double total = total();
            model.put("producto", producto);
            model.put("errorProducto", e.getMessage());
            model.put("tipo", TipoVenta.values());
            model.put("precio", precio);
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "venta.html";
        }

    }

    @PostMapping("/eliminar")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String delProd(RedirectAttributes atr, @RequestParam String cId, @RequestParam String pId) throws ErrorServicio {
        Cliente cliente = clienteServicio.buscarPorId(cId);
        Producto producto = new Producto();
        System.out.println("codigon en el ingreso" + pId);

        int posicion = buscarLista(pId);

        productosventa.remove(posicion);

        Double total = total();

        atr.addFlashAttribute("tipo", TipoVenta.values());
        atr.addFlashAttribute("productos", productosventa);
        atr.addFlashAttribute("producto", producto);
        atr.addFlashAttribute("cliente", cliente);
        atr.addFlashAttribute("total", total);
        return "redirect:/actualVenta";
    }

    @PostMapping("/agregarProducto")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String addProd(ModelMap model, RedirectAttributes atr, @RequestParam String cId, @RequestParam String pId, @RequestParam Integer cant, @RequestParam Double precio) throws ErrorServicio {

        Cliente cliente = clienteServicio.buscarPorId(cId);

        Producto producto = new Producto();

        Producto product = productoServicio.buscarPorCodigo(pId);

        String ex = new String();

        if (product.getCantActual() < cant || product.getCantActual() == 0) {
            ex = "No hay Stock suficiente registrado. Verificar ingresos";

        }
        ProdVenta prod = new ProdVenta();
        try {
            Integer i = (productosventa.size() + 1);
            System.out.println(i);
            prod.setId(i.toString());
            prod.setCodigo(pId);
            prod.setCant(cant);
            prod.setNombre(product.getNombre());
            prod.setColor(product.getColor());

            prod.setPrecio(precio);

            prod.setSubtotal(precio * cant);
            productosventa.add(prod);

            Double total = total();

            atr.addFlashAttribute("tipo", TipoVenta.values());
            atr.addFlashAttribute("errorCant", ex);
            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("cliente", cliente);
            atr.addFlashAttribute("total", total);

            return "redirect:/actualVenta";

        } catch (Exception e) {
            Double total = total();
            atr.addFlashAttribute("errorProducto", e.getMessage());
            atr.addFlashAttribute("errorCant", ex);
            atr.addFlashAttribute("tipo", TipoVenta.values());
            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("cliente", cliente);
            atr.addFlashAttribute("total", total);

            return "redirect:/actualVenta";

        }

    }

    @PostMapping("/vender")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String vender(ModelMap model, @RequestParam Long uId, @RequestParam String cId, @RequestParam String obs) throws ErrorServicio {
        Cliente cliente = clienteServicio.buscarPorId(cId);
        
        
        Venta venta = new Venta();

        Double total = total();

        Usuario user = usuarioServicio.buscarPorId(uId);

        try {
            venta = ventaServicio.nuevaVenta(cliente, user, total, obs);

            prod(venta);

            model.put("venta", venta);
            model.put("cliente", cliente);
            model.put("total", total);
            model.put("pago", TipoPago.values());
            productosventa.clear();

            return "enviado.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());

            model.put("tipo", TipoVenta.values());
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "venta.html";

        }

    }

    /*-------------May 2----------------*/
    @GetMapping("/VentaMay2")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String VentaMay2(ModelMap model, String id) {
        Cliente cliente = clienteServicio.buscarPorId(id);

        Producto producto = new Producto();

        Double total = null;

        productosventa.clear();
        model.put("tipo", TipoVenta.MAYORISTA2);
        model.put("productos", productosventa);
        model.put("producto", producto);
        model.put("cliente", cliente);
        model.put("total", total);
        return "ventaMay2.html";
    }

    @GetMapping("/actualVentaMay2")
    public String actualVentaMay2(ModelMap model) {

        return "ventaMay2.html";
    }

    @PostMapping("/buscarProductoMay2")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String findProdMay2(ModelMap model, @RequestParam String cId, @RequestParam String pId, @RequestParam TipoVenta tipo) throws ErrorServicio {

        Cliente cliente = clienteServicio.buscarPorId(cId);
        Double precio = 0.0;
        Producto producto = new Producto();

        try {
            productoServicio.validarId(pId);
            producto = productoServicio.buscarPorCodigo(pId);
            try {

                if (tipo.toString().equals("MAYORISTA2")) {
                    precio = producto.getPrecioMay2();

                } else {
                    if (cliente.getTipo().toString().equals("MINORISTA")) {
                        precio = producto.getPrecioUnitario();
                    } else {
                        precio = producto.getPrecioMay1();
                    }
                }
                model.put("producto", producto);
            } catch (Exception e) {

            }

            Double total = total();

            if (producto.getCantLocal() == 0) {

            }

            model.put("tipo", TipoVenta.MAYORISTA2);
            model.put("precio", precio);
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "ventaMay2.html";
        } catch (ErrorServicio e) {
            Double total = total();
            model.put("producto", producto);
            model.put("errorProducto", e.getMessage());
            model.put("tipo", TipoVenta.MAYORISTA2);
            model.put("precio", precio);
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "ventaMay2.html";
        }

    }

    @PostMapping("/eliminarMay2")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String delProdMay2(RedirectAttributes atr, @RequestParam String cId, @RequestParam String pId) throws ErrorServicio {
        Cliente cliente = clienteServicio.buscarPorId(cId);
        Producto producto = new Producto();
        System.out.println("codigon en el ingreso" + pId);

        int posicion = buscarLista(pId);

        productosventa.remove(posicion);

        Double total = total();

        atr.addFlashAttribute("tipo", TipoVenta.MAYORISTA2);
        atr.addFlashAttribute("productos", productosventa);
        atr.addFlashAttribute("producto", producto);
        atr.addFlashAttribute("cliente", cliente);
        atr.addFlashAttribute("total", total);
        return "redirect:/actualVentaMay2";
    }

    @PostMapping("/agregarProductoMay2")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String addProdMay2(ModelMap model, RedirectAttributes atr, @RequestParam String cId, @RequestParam String pId, @RequestParam Integer cant, @RequestParam Double precio) throws ErrorServicio {

        Cliente cliente = clienteServicio.buscarPorId(cId);

        Producto producto = new Producto();

        Producto product = productoServicio.buscarPorCodigo(pId);

        String ex = new String();

        if (product.getCantActual() < cant || product.getCantActual() == 0) {
            ex = "No hay Stock suficiente registrado. Verificar ingresos";

        }
        ProdVenta prod = new ProdVenta();
        try {
            Integer i = (productosventa.size() + 1);
            System.out.println(i);
            prod.setId(i.toString());
            prod.setCodigo(pId);
            prod.setCant(cant);
            prod.setNombre(product.getNombre());
            prod.setColor(product.getColor());

            prod.setPrecio(precio);

            prod.setSubtotal(precio * cant);
            productosventa.add(prod);

            Double total = total();

            atr.addFlashAttribute("tipo", TipoVenta.MAYORISTA2);
            atr.addFlashAttribute("errorCant", ex);
            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("cliente", cliente);
            atr.addFlashAttribute("total", total);

            return "redirect:/actualVentaMay2";

        } catch (Exception e) {
            Double total = total();
            atr.addFlashAttribute("errorProducto", e.getMessage());
            atr.addFlashAttribute("errorCant", ex);
            atr.addFlashAttribute("tipo", TipoVenta.MAYORISTA2);
            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("cliente", cliente);
            atr.addFlashAttribute("total", total);

            return "redirect:/actualVentaMay2";

        }

    }

    @PostMapping("/venderMay2")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String venderMay2(ModelMap model, @RequestParam Long uId, @RequestParam String cId, @RequestParam String obs) throws ErrorServicio {

        Cliente cliente = clienteServicio.buscarPorId(cId);
        Venta venta = new Venta();

        Double total = total();

        Usuario user = usuarioServicio.buscarPorId(uId);

        try {
            venta = ventaServicio.nuevaVenta(cliente, user, total, obs);

            prod(venta);

            model.put("venta", venta);
            model.put("cliente", cliente);
            model.put("total", total);
            model.put("pago", TipoPago.values());
            productosventa.clear();

            return "enviado.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());

            model.put("tipo", TipoVenta.MAYORISTA2);
            model.put("cliente", cliente);
            model.put("productos", productosventa);
            model.put("total", total);
            return "ventaMay2.html";

        }

    }

    /*---------------------------------------------------------*/
    @GetMapping("/cancelar")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String cancelarVenta() {
        return "redirect:/venta";
    }

    @GetMapping("/verVenta")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String verVenta(ModelMap model, Long id) throws ErrorServicio {

        Venta v = ventaServicio.buscarPorId(id);
        Cliente c = clienteServicio.buscarPorId(v.getCliente().getId());
        List<ProdVenta> prod = prServ.buscarPorVenta(id);

        model.put("venta", v);
        model.put("cliente", c);
        model.put("productos", prod);
        return "verVenta.html";
    }

    /*------------- Consultas -----------*/
    @GetMapping("/todasVentas")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String todasVentas(ModelMap model) {

        List<Venta> v = ventaServicio.allVentas();

        model.put("accion", "Todas");
        model.put("estado", Estado.values());
        model.put("ventas", v);
        return "ventas.html";
    }

    @PostMapping("/buscarPorestado")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPorEstado(ModelMap model, Estado b) {
        List<Venta> v = ventaServicio.buscarPorEstado(b);

        model.put("estado", Estado.values());
        model.put("accion", "Estado");
        model.put("tamano", v.size() + 1);
        model.put("ventas", v);
        return "ventas.html";
    }

    @PostMapping("/buscarPornumero")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPornumero(ModelMap model, @RequestParam Long b) throws ErrorServicio {
        Venta v = ventaServicio.buscarPorId(b);

        model.put("estado", Estado.values());
        model.put("accion", "Número de Venta");
        model.put("tamano", 1);
        model.put("ventas", v);
        return "ventas.html";
    }

    @PostMapping("/buscarPorcliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPorcliente(ModelMap model, @RequestParam String b) {
        List<Cliente> c = clienteServicio.buscarPorNombre(b);

        List<Venta> v = ventaServicio.ventasPorNombres(c);

        model.put("estado", Estado.values());
        model.put("accion", "Cliente");
        model.put("tamano", v.size() + 1);
        model.put("ventas", v);
        return "ventas.html";
    }

    @PostMapping("/eliminarVenta")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String eliminarVenta(ModelMap model, Long vId) throws ErrorServicio {

        try {
            pagoServ.elimianrPago(vId);
            List<ProdVenta> x = prServ.buscarPorVenta(vId);
            reintegro(x);
            prServ.eliminarPv(vId);
            ventaServicio.eliminarVenta(vId);
            model.put("correcta", "Venta eliminada");

            List<Venta> v = ventaServicio.allVentas();

            model.put("estado", Estado.values());
            model.put("ventas", v);
            return "ventas.html";
        } catch (ErrorServicio e) {

            List<Venta> v = ventaServicio.allVentas();

            model.put("error", "Error al eliminar la venta" + e.getMessage());
            model.put("estado", Estado.values());
            model.put("ventas", v);
            return "ventas.html";
        }
    }

    /*FUNCIONES */
    public Double total() {
        Double total = 0.0;

        for (ProdVenta aux : productosventa) {
            total = total + aux.getSubtotal();
        }
        System.out.println(total);
        return total;
    }

    public int buscarLista(String id) {
        int posicion = 0;

        System.out.println(id);

        for (int i = 0; i < productosventa.size(); i++) {
            ProdVenta p = productosventa.get(i);
            System.out.println(id);
            System.out.println(p.getId());
            if (p.getId().equals(id)) {
                posicion = i;
                break;
            }
        }

        return posicion;
    }

    public void prod(Venta v) throws ErrorServicio {

        for (ProdVenta pV : productosventa) {

            Producto po = productoServicio.buscarPorCodigo(pV.getCodigo());

            Double ganancia = pV.getPrecio() - po.getCosto();/*Calculo de la ganancia por producto*/
            Double gan = ganancia * pV.getCant();

            ProdVenta pr = prServ.cargar(po.getBarras(), po.getNombre(), po.getTipo(), po.getColor(), pV.getPrecio(), gan, pV.getCant(), pV.getSubtotal(), v);

            productoServicio.restarCant(po.getId(), pV.getCant());

        }

    }

    public void reintegro(List<ProdVenta> v) throws ErrorServicio {

        for (ProdVenta pV : v) {

            productoServicio.reintegrarStock(pV.getCodigo(), pV.getCant());

        }

    }

    @GetMapping("/imprimir")
    public String imprimirVenta(ModelMap model, @RequestParam Long id) throws ErrorServicio {

        Venta v = ventaServicio.buscarPorId(id);
        System.out.println(v.getCliente().getId());
        Cliente c = clienteServicio.buscarPorId(v.getCliente().getId());
        System.out.println(c.getNombre());
        List<ProdVenta> prod = prServ.buscarPorVenta(id);
        Empresa e = empresaServ.buscar(1);

        model.put("empresa", e);
        model.put("venta", v);
        model.put("cliente", c);
        model.put("productos", prod);

        return "impresion.html";
    }
    
    @GetMapping("/imprimirPedidoApi")
    public String imprimirPedidoApi(ModelMap model, @RequestParam Long id) throws ErrorServicio {

        Empresa e = empresaServ.buscar(1);
        PedidoAPI p = apiServ.buscarPorID(id);
        Cliente c = p.getCliente();
        Date date= new Date();
        
         DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
         String d= df.format(date);
       

        try {
            productosventa = apiServ.obtenerProductos(p.getProductos(), c);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ControladorVentas.class.getName()).log(Level.SEVERE, null, ex);
        }

        Double total = total();


        model.put("total", total);
        model.put("fecha", d);
        model.put("empresa", e);
        model.put("pedido", p);
        model.put("cliente", c);
        model.put("productos", productosventa);

        return "impresionApi.html";
    }

    @GetMapping("/eliminarPedidoApi")
    public String eliminarApi(@RequestParam Long id, ModelMap model) {

        PedidoAPI pe= apiServ.buscarPorID(id);
        
        try {
            apiServ.borrarPedido(id);
            if(pe.getCliente().getTipo() == Tipo.MINORISTA){
                clienteServicio.eliminarCliente(pe.getCliente().getId());
            }
            List<Venta> v = ventaServicio.ultimasHoy();
            List<PedidoAPI> p = apiServ.pedidosApi();
            Double ventas = ventaServicio.ventasD();
            String ven = String.format("%,.2f", ventas);
            

            model.put("total", ven);
            model.put("ventas", v);
            model.put("pedidos", p);
            return "ventaMain.html";
        } catch (Exception ex) {
            List<Venta> v = ventaServicio.ultimasHoy();
            List<PedidoAPI> p = apiServ.pedidosApi();
            Double ventas = ventaServicio.ventasD();
            String ven = String.format("%,.2f", ventas);

            model.put("total", ven);
            model.put("ventas", v);
            model.put("pedidos", p);
            return "ventaMain.html";
        }

    }

}
