 package com.Sismas.controlador;

import com.Sismas.Entidades.CierreUsuario;
import com.Sismas.Entidades.Pago;
import com.Sismas.Entidades.Usuario;
import com.Sismas.Entidades.Venta;
import com.Sismas.enumeraciones.TipoPago;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.CierreServicio;
import com.Sismas.servicios.DiarioServicio;
import com.Sismas.servicios.PagoServicio;
import com.Sismas.servicios.UsuarioServicio;
import com.Sismas.servicios.VentaServicio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class ControladorCierres {

    @Autowired
    private PagoServicio pagoServ;

    @Autowired
    private DiarioServicio diarioServ;

    @Autowired
    private VentaServicio ventaServicio;
    
    @Autowired
    private CierreServicio cierreServ;
    
    @Autowired
    private UsuarioServicio usuarioServicio;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/cierre")
    public String cierre(ModelMap model) {

        ArrayList<Double> pagos = pagos();

        ArrayList<String> tipos = tipos();

        Double gan = ventaServicio.ventas();
        String ganancias = String.format("%,.2f", gan);

        Double tot = ventaServicio.ventasD();
        String total = String.format("%,.2f", tot);

        Double ing = pagoServ.totalPagos();
        String ingresos = String.format("%,.2f", ing);
        Double pen = ventaServicio.totalRestos();
        String pendientes = String.format("%,.2f", pen);

        Double man = ventaServicio.ventasManana();
        String manana = String.format("%,.2f", man);
        Double tar = ventaServicio.ventasTarde();
        String tarde = String.format("%,.2f", tar);

        model.put("ganancias", ganancias);
        model.put("ventas", total);
        model.put("restos", pendientes);
        model.put("ingresos", ingresos);
        model.put("pagos", pagos);
        model.put("tipo", tipos);

        return "graficoCierre.html";

    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/cerrar")
    public String cerrar(ModelMap model, @RequestParam Long id) {
        
        Usuario user= usuarioServicio.buscarPorId(id);

        ArrayList<Double> pagos = pagos();

        ArrayList<String> tipos = tipos();

        Double gan = ventaServicio.ventas();
        String ganancias = String.format("%,.2f", gan);

        Double tot = ventaServicio.ventasD();
        String total = String.format("%,.2f", tot);

        Double ing = pagoServ.totalPagos();
        String ingresos = String.format("%,.2f", ing);
        Double pen = ventaServicio.totalRestos();
        String pendientes = String.format("%,.2f", pen);

        Double man = ventaServicio.ventasManana();
        String manana = String.format("%,.2f", man);
        Double tar = ventaServicio.ventasTarde();
        String tarde = String.format("%,.2f", tar);

        Double e = pagoServ.totalPagoEfectivo();
        Double de = pagoServ.totalPagoDebito();
        Double c = pagoServ.totalPagoTarjeta();
        Double me = pagoServ.totalPagoMercadoPago();
        Double t = pagoServ.totalPagoTransferencia();

        try {
            diarioServ.cierre(tot, gan, e, de, me, t, c, user);
            model.put("exito", "Cierre generado");
            model.put("ganancias", ganancias);
            model.put("ventas", total);
            model.put("restos", pendientes);
            model.put("ingresos", ingresos);
            model.put("pagos", pagos);
            model.put("tipo", tipos);

            return "graficoCierre.html";
        } catch (Exception ex) {
            model.put("error", ex.getMessage());
            model.put("ganancias", ganancias);
            model.put("ventas", total);
            model.put("restos", pendientes);
            model.put("ingresos", ingresos);
            model.put("pagos", pagos);
            model.put("tipo", tipos);

            return "graficoCierre.html";
        }

    }
    
    @GetMapping("/historialCierres")
    public String historial(ModelMap model, @RequestParam Long id){
        List<CierreUsuario> u= cierreServ.todosU(id);
        
        model.put("cierres", u);
        return "tablaCierres.html";
    }
    
    @GetMapping("/cierreUsuario")
    public String cierreUsuario(ModelMap model, @RequestParam Long id) {
        
        Usuario user= usuarioServicio.buscarPorId(id);

        ArrayList<Pago> pagos = pagosUsuario(id);

        List<Venta> ventas= ventaServicio.ventasUserD(id);
        Double gan = ventaServicio.ventasUser(id);
        String total = String.format("%,.2f", gan);

        Double ing = pagoServ.totalPagosUser(id);
        String ingresos = String.format("%,.2f", ing);

        String turno= "";
        String t= "";
        
        
        if(new Date().getHours()<14){
            Double man = ventaServicio.ventasMananaUser(id);
         turno = String.format("%,.2f", man);
         t= "Mañana";
        }else{
            Double tar = ventaServicio.ventasTardeUser(id);
         turno= String.format("%,.2f", tar);
         t= "Tarne";
        }

        
        model.put("ventas",ventas);
        model.put("user", user);
        model.put("gan", null);
        model.put("manana", turno);
        model.put("turno", t);
        model.put("ingresos", ingresos);
        model.put("total", total);
        model.put("pagos", pagos);
        return "graficoUsuariosCierre.html";
    }
    
    @PostMapping("/cerrarTurno")
    public String cerrarTurno(ModelMap model, @RequestParam Long uId, Double efe, Double cupones, String obs) {
        
        Usuario user= usuarioServicio.buscarPorId(uId);
        Double ef = pagoServ.totalPagoEfectivo();
    
        Double de = pagoServ.totalPagoDebito();
       
        Double c = pagoServ.totalPagoTarjeta();
      
        Double me = pagoServ.totalPagoMercadoPago();
     
        Double tr = pagoServ.totalPagoTransferencia();
       
        
        Double gan = ventaServicio.ventasUserT(uId);
        String total = String.format("%,.2f", gan);
        Double ing = pagoServ.totalPagosUser(uId);
        String ingresos = String.format("%,.2f", ing);
        
        Double dif=ing-(efe+cupones);
        
        String turno= "";
        String t= "";

        if(new Date().getHours()<14){
            Double man = ventaServicio.ventasMananaUser(uId);
         turno = String.format("%,.2f", man);
         t= "Mañana";
        }else{
            Double tar = ventaServicio.ventasTardeUser(uId);
         turno= String.format("%,.2f", tar);
         t= "Tarne";
        }

        try {
            cierreServ.crearCierre(gan, dif, ef, de, c, me, tr, ing, user, obs);
                            /*total,  dif, efe, deb, cred,  mP, tran, ventas, user, obs*/
        } catch (ErrorServicio e) {
            model.put("error", e.getMessage());
        }
        

 
        return "cierreOk.html";
    }
    

    /*Funciones extra
    *
    */
    
    private ArrayList<Pago> pagosUsuario(Long id) {
        ArrayList<Pago> pagos = new ArrayList();
        if(new Date().getHours()<14){
        Pago e = pagoServ.totalUserManana(TipoPago.DEBITO, id);
        pagos.add(e);
        Pago a = pagoServ.totalUserManana(TipoPago.CREDITO, id);
        pagos.add(a);
        Pago i = pagoServ.totalUserManana(TipoPago.EFECTIVO, id);
        pagos.add(i);
        Pago o = pagoServ.totalUserManana(TipoPago.MERCADOPAGO, id);
        pagos.add(o);
        Pago u = pagoServ.totalUserManana(TipoPago.TRANSFERENCIA, id);
        pagos.add(u);

        }else{
            Pago e = pagoServ.totalUserTarde(TipoPago.DEBITO, id);
        pagos.add(e);
        Pago a = pagoServ.totalUserTarde(TipoPago.CREDITO, id);
        pagos.add(a);
        Pago i = pagoServ.totalUserTarde(TipoPago.EFECTIVO, id);
        pagos.add(i);
        Pago o = pagoServ.totalUserTarde(TipoPago.MERCADOPAGO, id);
        pagos.add(o);
        Pago u = pagoServ.totalUserTarde(TipoPago.TRANSFERENCIA, id);
        pagos.add(u);
        }
        
        
        return pagos;
    }
    
    private ArrayList<Double> pagos() {
        ArrayList<Double> pagos = new ArrayList();
        Double e = pagoServ.totalPagoEfectivo();
        pagos.add(e);
        Double de = pagoServ.totalPagoDebito();
        pagos.add(de);
        Double c = pagoServ.totalPagoTarjeta();
        pagos.add(c);
        Double me = pagoServ.totalPagoMercadoPago();
        pagos.add(me);
        Double t = pagoServ.totalPagoTransferencia();
        pagos.add(t);

        return pagos;
    }

    private ArrayList<String> tipos() {
        ArrayList<String> tipos = new ArrayList();
        tipos.add(TipoPago.EFECTIVO.toString());
        tipos.add(TipoPago.DEBITO.toString());
        tipos.add(TipoPago.CREDITO.toString());
        tipos.add(TipoPago.MERCADOPAGO.toString());
        tipos.add(TipoPago.TRANSFERENCIA.toString());

        return tipos;
    }

}
