package com.Sismas.controlador;

import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.enumeraciones.Cat;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.FotoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.ProveedorServicio;
import com.Sismas.servicios.UsuarioServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class ControladorStock {

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private ProveedorServicio proveedorServicio;

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private FotoServicio fotoServicio;

    @GetMapping("/stock")
    public String stock(ModelMap model) {
        List<Producto> faltantes = productoServicio.buscarFaltante();
        model.put("productos", faltantes);
        return "stock.html";
    }

    @GetMapping("/stockTotal")
    public String stockTotal(ModelMap model) {
        List<Producto> productos = productoServicio.buscarTodos();

        String a = "Todos";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockTotal.html";
    }

    @GetMapping("/faltantesUrgentes")
    public String faltantesUrgentes(ModelMap model) {
        List<Producto> productos = productoServicio.buscarFaltanteUrgente();

        String a = "Urgentes";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockTotal.html";
    }

    @PostMapping("/buscarPorTipoTotal")
    public String buscarPorTipoTotal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorTipo(b);

        String a = "Tipo";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockTotal.html";
    }

    @PostMapping("/buscarPorNombreTotal")
    public String buscarPorNombreTotal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorNom(b);

        String a = "Nombre";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockTotal.html";
    }

    @PostMapping("/buscarPorCategoriaTotal")
    public String buscarPorCategoriaTotal(ModelMap model, @RequestParam Cat b) {
        List<Producto> productos = productoServicio.buscarPorCat(b);

        String a = "Categoría";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockTotal.html";
    }

    @PostMapping("/buscarPorTipoDeposito")
    public String buscarPorTipoDeposito(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorTipoDepo(b);

        String a = "Tipo";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockDeposito.html";
    }

    @PostMapping("/buscarPorNombreDeposito")
    public String buscarPorNombreDeposito(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorNomDepo(b);

        String a = "Nombre";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockDeposito.html";
    }

    @PostMapping("/buscarPorCategoriaDeposito")
    public String buscarPorCategoriaDeposito(ModelMap model, @RequestParam Cat b) {
        List<Producto> productos = productoServicio.buscarPorCatDepo(b);

        String a = "Categoría";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockDeposito.html";
    }

    @PostMapping("/buscarPorTipoLocal")
    public String buscarPorTipoLocal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorTipoLocal(b);

        String a = "Tipo";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockLocal.html";
    }

    @PostMapping("/buscarPorNombreLocal")
    public String buscarPorNombreLocal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorNomLocal(b);

        String a = "Nombre";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockLocal.html";
    }

    @PostMapping("/buscarPorCategoriaLocal")
    public String buscarPorCategoriaLocal(ModelMap model, @RequestParam Cat b) {
        List<Producto> productos = productoServicio.buscarPorCatLocal(b);

        String a = "Categoría";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockLocal.html";
    }

    @GetMapping("/stockLocal")
    public String stockLocal(ModelMap model) {
        List<Producto> productos = productoServicio.buscarLocal();

        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockLocal.html";
    }

    @GetMapping("/stockDeposito")
    public String stockDeposito(ModelMap model) {

        List<Producto> productos = productoServicio.buscarDeposito();

        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "stockDeposito.html";
    }

    @GetMapping("/verPrecio")
    public String verPrecios(ModelMap model, @RequestParam String id) throws ErrorServicio {
        Producto productos = productoServicio.buscarPorId(id);

        model.put("productos", productos);
        return "productos.html";
    }

    @GetMapping("/verStock")
    public String verStock(ModelMap model, @RequestParam String id) throws ErrorServicio {
        Producto productos = productoServicio.buscarPorId(id);

        model.put("productos", productos);
        return "stockTotal.html";
    }

    /*------------------Carga de ingresos------------------------------------------------------------------*/
 /*-------Por Código Interno------------*/
    
    @PostMapping("/calcularI")
    public String calculadoraI(Double calculo, ModelMap model, @RequestParam(required = false) String id) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        model.put("provedor", proveedores);
        String accion = null;

        Producto pro = new Producto();

            Double p = (calculo + (calculo / 100) * 35);
            Double m1 = (calculo + (calculo / 100) * 22);
            Double m2 = (calculo + (calculo / 100) * 15);

            accion = "Ingresar por Código Interno";

            model.put("calp", p);
            model.put("calm2", m2);
            model.put("calm1", m1);
            model.put("accion", accion);
            model.put("proveedor", proveedores);
            model.put("producto", pro);

            return "ingresoInterno.html";
        
        
    }
    
    @GetMapping("/ingresoI")
    public String ingresoI(ModelMap model) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        Producto p = new Producto();

        model.put("accion", "Ingresar por Código Interno");
        model.put("producto", p);
        model.put("proveedor", proveedores);
        return "ingresoInterno.html";
    }

    
    @PostMapping("/buscarI")
    public String buscarI(ModelMap model, @RequestParam String codInt) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        try {
            Producto p = productoServicio.buscarPorCodigoInt(codInt);

            model.put("cant", 0);
            model.put("accion", "Ingresar por Código Interno");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoInterno.html";
        } catch (ErrorServicio e) {
            Producto p = new Producto();

            model.put("cant", 0);
            model.put("accion", "Ingresar por Código Interno");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoInterno.html";
        }
    }

    @PostMapping("/ingresarI")
    public String ingresarI(ModelMap model, @RequestParam String id, Double costo, Double precioU, Double precioM1, Double precioM2, Integer cantDepo, Integer cantLocal, Integer preVenta) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        if(cantDepo==null||cantDepo.toString().isEmpty()){
            cantDepo=0;
        }
        if(cantLocal==null||cantLocal.toString().isEmpty()){
            cantLocal=0;
        }
                
        
        try {
            productoServicio.igresarInterno(id, costo, precioM1, precioM2, precioU, cantDepo, cantLocal, preVenta);

            Producto p = new Producto();
            model.put("exito", "Cargado!");
            model.put("accion", "Ingresar por Código Interno");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoInterno.html";
        } catch (ErrorServicio e) {
            Producto p = new Producto();
            model.put("accion", "Ingresar por Código Interno");
            model.put("error", e.getMessage());
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoInterno.html";
        }

    }

    /*--------------------Por Código de Barras------------*/
    @PostMapping("/calcularB")
    public String calculadoraB(Double calculo, ModelMap model, @RequestParam(required = false) String id) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        model.put("provedor", proveedores);
        String accion = null;

        Producto pro = new Producto();

            Double p = (calculo + (calculo / 100) * 35);
            Double m1 = (calculo + (calculo / 100) * 22);
            Double m2 = (calculo + (calculo / 100) * 15);

            accion = "Ingresar por Código de Barras";

            model.put("calp", p);
            model.put("calm2", m2);
            model.put("calm1", m1);
            model.put("accion", accion);
            model.put("proveedor", proveedores);
            model.put("producto", pro);

            return "ingresoBarras.html";
        
    }
    @GetMapping("/ingresoB")
    public String ingresoB(ModelMap model) throws ErrorServicio {

        List<Proveedor> proveedores = proveedorServicio.todosProv();
        Producto p = new Producto();

        model.put("cant", 0);
        model.put("accion", "Ingresar por Código de Barras");
        model.put("producto", p);
        model.put("proveedor", proveedores);
        return "ingresoBarras.html";
    }

    @PostMapping("/buscarB")
    public String buscarB(ModelMap model, @RequestParam String codBar) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        try {
            Producto p = productoServicio.buscarPorCodigo(codBar);

            model.put("accion", "Ingresar por Código de Barras");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoBarras.html";
        } catch (ErrorServicio e) {

            Producto p = new Producto();

            model.put("error", e.getMessage());
            model.put("accion", "Ingresar por Código de Barras");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoBarras.html";
        }
    }

    @PostMapping("/ingresarB")
    public String ingresarB(ModelMap model, @RequestParam String id, Double costo, Double precioU, Double precioM1, Double precioM2, Integer cantDepo, Integer cantLocal, Integer preVenta) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        try {
            productoServicio.igresarInterno(id, costo, precioM1, precioM2, precioU, cantDepo, cantLocal,preVenta);

            Producto p = new Producto();
            model.put("exito", "Cargado!");
            model.put("accion", "Ingresar por Código de Barras");
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoBarras.html";
        } catch (ErrorServicio e) {
            Producto p = new Producto();
            model.put("accion", "Ingresar por Código de Barras");
            model.put("error", e.getMessage());
            model.put("producto", p);
            model.put("proveedor", proveedores);
            return "ingresoBarras.html";
        }

    }
}
