package com.Sismas.controlador;

import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Usuario;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/foto")
public class ControladorFoto {

    @Autowired
    private ProductoServicio productoServicio;
    
    @Autowired
    private UsuarioServicio usuarioServicio;

    @GetMapping("/producto")
    public ResponseEntity<byte[]> fotoProducto(@RequestParam String id) {
        try {
            Producto producto = productoServicio.buscarPorId(id);
            if(producto.getFoto()== null){
             throw new ErrorServicio("El producto no tiene foto"); 
            
        }
            
            byte[] foto= producto.getFoto().getContenido();
            
            HttpHeaders headers= new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG);
            return new ResponseEntity<>(foto, headers, HttpStatus.OK);
        } catch (ErrorServicio e) {
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        }

    }
    
    @GetMapping("/usuario")
    public ResponseEntity<byte[]> fotoUsuario(@RequestParam Long id) {
        try {
            Usuario usuario = usuarioServicio.buscarPorId(id);
            if(usuario.getFoto()== null){
             throw new ErrorServicio("El producto no tiene foto"); 
            
        }
            
            byte[] foto= usuario.getFoto().getContenido();
            
            HttpHeaders headers= new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_JPEG);
            return new ResponseEntity<>(foto, headers, HttpStatus.OK);
        } catch (ErrorServicio e) {
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
        }

    }

}
