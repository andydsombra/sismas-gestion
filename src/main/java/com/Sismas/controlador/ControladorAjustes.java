package com.Sismas.controlador;

import com.Sismas.Entidades.Foto;
import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.enumeraciones.Cat;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.EmpresaServicio;
import com.Sismas.servicios.FotoServicio;
import com.Sismas.servicios.PedidoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.ProveedorServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class ControladorAjustes {

    @Autowired
    private EmpresaServicio empresaServicio;
    
    @Autowired
    private FotoServicio fotoServicio;

   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/ajustes")
    public String ajustes(ModelMap model) {
        
        
        return "ajustes.html";
    }

 
    @PostMapping("/registrarDatos")
    public String registrarProd(ModelMap model, @RequestParam String nombre, @RequestParam String mail, @RequestParam String direccion, @RequestParam String razon, @RequestParam MultipartFile foto, @RequestParam String num) throws ErrorServicio {

        Foto photo = null;
            try {
                photo = fotoServicio.cargarArchivo(foto);
                System.out.println("Foto guardada con exito");
            } catch (Exception e) {
                model.put("error", e.getMessage());
            }




            empresaServicio.cargarEmpresa(1, nombre, mail, direccion, razon, num, photo);

            model.put("exito", "Datos guaradados");
            return "ajustes.html";
 

    }

    
}
