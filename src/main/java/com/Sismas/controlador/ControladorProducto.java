package com.Sismas.controlador;

import com.Sismas.Entidades.Foto;
import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.enumeraciones.Cat;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.FotoServicio;
import com.Sismas.servicios.PedidoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.ProveedorServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class ControladorProducto {

    @Autowired
    private ProveedorServicio proveedorServicio;

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private PedidoServicio pedServ;

    @Autowired
    private FotoServicio fotoServicio;

    @GetMapping("/producto")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String productoMain(ModelMap model) {
        List<Producto> faltantes = productoServicio.buscarFaltante();
        List<Pedido> ultimosPedidos = pedServ.buscarOrdernado();

        model.put("productos", faltantes);
        model.put("pedidos", ultimosPedidos);
        return "productoMain.html";
    }

    @GetMapping("/verProductos")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String verProductos(ModelMap model) {

        List<Producto> productos = productoServicio.buscarTodos();

        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "productos.html";
    }

    @PostMapping("/calcular")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String calculadora(Double calculo, ModelMap model, @RequestParam(required = false) String id) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        model.put("provedor", proveedores);
        String accion = null;

        Producto pro = new Producto();
        if (id == null || id.isEmpty()) {

            Double p = (calculo + (calculo / 100) * 35);
            Double m1 = (calculo + (calculo / 100) * 22);
            Double m2 = (calculo + (calculo / 100) * 15);

            accion = "Crear";

            model.put("calp", p);
            model.put("calm2", m2);
            model.put("calm1", m1);
            model.put("categoria", Cat.values());
            model.put("accion", accion);
            model.put("proveedor", proveedores);

            return "registroProducto.html";
        } else {
            accion = "Actualizar";

            Double p = (calculo + (calculo / 100) * 35);
            Double m1 = (calculo + (calculo / 100) * 22);
            Double m2 = (calculo + (calculo / 100) * 15);

            pro = productoServicio.buscarPorCodigo(id);
            model.put("calp", p);
            model.put("calm2", m2);
            model.put("calm1", m1);
            model.put("producto", pro);
            model.put("accion", accion);
            model.put("categoria", Cat.values());
            model.put("proveedor", proveedores);

            return "registroProducto_1.html";
        }
    }

    @GetMapping("/registroProducto")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String registroProducto(ModelMap model, @RequestParam(required = false) String id) throws ErrorServicio {
        List<Proveedor> proveedores = proveedorServicio.todosProv();
        model.put("provedor", proveedores);
        String accion = null;

        Producto pro = new Producto();

        if (id == null || id.isEmpty()) {
            accion = "Crear";
            model.put("categoria", Cat.values());
            model.put("accion", accion);
            model.put("proveedor", proveedores);

            return "registroProducto.html";
        } else {
            accion = "Actualizar";
            pro = productoServicio.buscarPorId(id);
            model.put("producto", pro);
            model.put("accion", accion);
            model.put("categoria", Cat.values());
            model.put("proveedor", proveedores);

            return "registroProducto_1.html";
        }

    }

    @PostMapping("/registrarProducto")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String registrarProd(ModelMap model, @RequestParam String barras, @RequestParam String codInterno, @RequestParam String nombre, @RequestParam Cat cat, @RequestParam String tipo, @RequestParam String color, @RequestParam String descripcion, @RequestParam Integer cantI, String proveedor, @RequestParam Double costo, @RequestParam Double precioM1, @RequestParam Double precioM2, @RequestParam Double precioU, @RequestParam Double precioD, @RequestParam MultipartFile foto, @RequestParam Integer cantLocal, @RequestParam Integer cantDepo, @RequestParam Integer cantM) throws ErrorServicio {

        List<Proveedor> proveedores = proveedorServicio.todosProv();
        Foto photo = null;
        if (productoServicio.exist(barras) == false) {
            try {
                photo = fotoServicio.cargarArchivo(foto);
                System.out.println("Foto guardada con exito");
            } catch (Exception e) {
                model.put("error", e.getMessage());
            }
        }

        Proveedor provee = proveedorServicio.buscarPorId(proveedor);

        Integer cantT = cantDepo + cantLocal;

        try {
            productoServicio.cargarProducto(barras, codInterno, nombre, cat, tipo, color, descripcion, cantT, cantI, photo, provee, costo, precioM1, precioM2, precioU, precioD, cantDepo, cantLocal, cantM);
           /*String cod, String nombre, Cat cat, String tipo, String color, Integer cantActual, Integer cantIdeal, Foto foto, Proveedor proveedor, Double costo, Double precioM1, Double precioM2, Double precioUnitario, Double precioDolar, Integer cantDepo, Integer cantLocal, Integer cantM*/
            model.put("accion", "Se guardó el");
            model.put("exito", "Producto Registrado");
            return "exitoProducto.html";
        } catch (ErrorServicio e) {
            model.put("accion", "Registrar ");
            model.put("error", e.getMessage());
            model.put("id ", barras);
            model.put("nombre ", nombre);
            model.put("categoría ", cat);
            model.put("tipo ", tipo);
            model.put("color ", color);
            model.put("cantI ", cantI);
            model.put("proveedor ", proveedor);
            model.put("costo ", costo);
            model.put("cantLocal ", cantLocal);
            model.put("cantDepo ", cantDepo);
            model.put("categoria", Cat.values());

            model.put("precioD ", precioD);
            model.put("proveedor", proveedores);

            return "registroProducto.html";

        }

    }

    @PostMapping("/modificarProducto")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String modificarProd(ModelMap model, @RequestParam String id ,@RequestParam String barras, @RequestParam String codInterno, @RequestParam String nombre, @RequestParam Cat cat, @RequestParam String tipo, @RequestParam String color, @RequestParam String descripcion, @RequestParam Integer cantI, String proveedor, @RequestParam Double costo, @RequestParam Double precioM1, @RequestParam Double precioM2, @RequestParam Double precioU, @RequestParam Double precioD, @RequestParam MultipartFile foto, @RequestParam Integer cantLocal, @RequestParam Integer cantDepo, @RequestParam Integer cantM) throws ErrorServicio {

        Foto photo = null;
        Producto p = productoServicio.buscarPorId(id);
        if (foto == null || foto.isEmpty()) {
            photo = p.getFoto();
        } else {
            try {
                photo = fotoServicio.cargarArchivo(foto);
                System.out.println("Foto guardada con exito");
            } catch (Exception e) {
                model.put("error", e.getMessage());
            }
        }

        Proveedor provee = proveedorServicio.buscarPorId(proveedor);
        Integer cantT = cantDepo + cantLocal;

        try {
            productoServicio.modificarProducto(id, barras, codInterno, nombre, tipo, color, descripcion, cantT, cantI, provee, costo, precioM1, precioM2, precioU, precioD, cat, photo, cantDepo, cantLocal, cantM);
            // id, nombre, tipo, color, cantA, cantI, null, costo, precioM1, precioM2, precioU, precioD, marca, foto, cantDepo, cantLocal)

            model.put("exito", "Producto actualizado");
            model.put("accion", "Actualizar ");
            return "exitoProducto.html";
        } catch (ErrorServicio e) {
            model.put("accion", "Actualizar ");
            model.put("error", e.getMessage());
            model.put("id ", id);
            model.put("nombre ", nombre);
            model.put("categoría ", cat);
            model.put("tipo ", tipo);
            model.put("color ", color);
            model.put("cantI ", cantI);
            model.put("proveedor ", proveedor);
            model.put("costo ", costo);
            model.put("cantLocal ", cantLocal);
            model.put("cantDepo ", cantDepo);

            model.put("precioD ", precioD);

            return "registroProducto.html";
        }

    }

    @PostMapping("/buscarPorTipoProd")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPorTipoTotal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorTipo(b);

        String a = "Tipo";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "productos.html";
    }

    @PostMapping("/buscarPorNombreProd")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPorNombreTotal(ModelMap model, @RequestParam String b) {
        List<Producto> productos = productoServicio.buscarPorNom(b);

        String a = "Nombre";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "productos.html";
    }

    @PostMapping("/buscarPorCategoriaProd")@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String buscarPorCategoriaTotal(ModelMap model, @RequestParam Cat b) {
        List<Producto> productos = productoServicio.buscarPorCat(b);

        String a = "Categoría";
        model.put("accion", a);
        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "productos.html";
    }
    
    @GetMapping("/eliminarProducto")@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String eliminarProducto(ModelMap model, @RequestParam String id){
        
        try {
          productoServicio.borrarProducto(id);  
        } catch (Exception e) {
            model.put("error", e.getMessage());
        }
        
        
        List<Producto> productos = productoServicio.buscarTodos();

        model.put("categoria", Cat.values());
        model.put("productos", productos);
        return "productos.html";
    }
}
