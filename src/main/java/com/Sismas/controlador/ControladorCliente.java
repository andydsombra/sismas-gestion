package com.Sismas.controlador;

import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.Producto;
import com.Sismas.enumeraciones.Tipo;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.ClienteServicio;
import com.Sismas.servicios.ProductoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class ControladorCliente {

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private ClienteServicio clienteServicio;

    @GetMapping("/cliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String clienteMain(ModelMap model) {
        List<Producto> faltantes = productoServicio.buscarFaltante();
        model.put("productos", faltantes);
        return "clienteMain.html";
    }

    @GetMapping("/verClientes")
    public String verClientes(ModelMap model) {

        List<Cliente> clientes = clienteServicio.todosLosClientes();

        model.put("clientes", clientes);
        model.put("categoria", Tipo.values());
        return "clientes.html";
    }
    

    @PostMapping("/verClientesPorNombre")
    public String verClientesPorNombre(ModelMap model, @RequestParam String b) {

        List<Cliente> clientes = clienteServicio.buscarPorNombre(b);

        model.put("clientes", clientes);
        model.put("categoria", Tipo.values());
        return "clientes.html";
    }

    @PostMapping("/verClientesPorNumero")
    public String verClientesPorNumero(ModelMap model, @RequestParam String b) {

        Cliente clientes = clienteServicio.buscarNumero(b);

        model.put("clientes", clientes);
        model.put("categoria", Tipo.values());
        return "clientes.html";
    }

    @PostMapping("/verClientePorCat")
    public String verClientePorCat(ModelMap model, @RequestParam Tipo b) {

        List<Cliente> clientes = clienteServicio.buscarCat(b);

        model.put("clientes", clientes);
        model.put("categoria", Tipo.values());
        return "clientes.html";
    }

    @GetMapping("/registroCliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String registroCliente(ModelMap model, @RequestParam(required = false) String id) {

        String accion = null;

        Cliente cliente = null;

        if (id == null || id.isEmpty()) {
            accion = "Registrar";
            model.put("accion", accion);
            model.put("tipo", Tipo.values());

            return "registroCliente.html";
        } else {
            accion = "Actualizar";
            cliente = clienteServicio.buscarPorId(id);
            model.put("cliente", cliente);
            model.put("accion", accion);
            model.put("tipo", Tipo.values());

            return "registroCliente_1.html";
        }
    }

    @PostMapping("/registrarCliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String registrarCliente(ModelMap model, @RequestParam String nombre, @RequestParam String direccion, @RequestParam String telefono, @RequestParam String mail, @RequestParam Tipo tipo, @RequestParam String doc, @RequestParam String numClient) throws ErrorServicio {

        try {
            System.out.println("por entrar");
            clienteServicio.registrarCliente(nombre, direccion, telefono, mail, tipo, doc, numClient);
            model.put("exito", "Cliente registrado");
        } catch (ErrorServicio e) {
            model.put("error", e.getMessage());
        }
        return "registroCliente.html";
    }

    @PostMapping("/modificarCliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String modificarCliente(ModelMap model, @RequestParam String id, @RequestParam String nombre, @RequestParam String direccion, @RequestParam String telefono, @RequestParam String mail, @RequestParam Tipo tipo, @RequestParam String doc, @RequestParam String numClient) throws ErrorServicio {

        try {

            clienteServicio.modificarCliente(id, nombre, direccion, telefono, mail, tipo, doc, numClient);
            model.put("exito", "Cliente Actualizado");
            model.put("accion", "");
            return "registroCliente.html";
        } catch (ErrorServicio e) {
            model.put("error", e.getMessage());

            return "registroCliente.html";
        }

    }

    @GetMapping("/eliminarCliente")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public String eliminarCliente(ModelMap model, @RequestParam String id) {

        try {
            clienteServicio.eliminarCliente(id);
            List<Cliente> clientes = clienteServicio.todosLosClientes();

            model.put("clientes", clientes);
            model.put("categoria", Tipo.values());
            return "clientes.html";
        } catch (Exception e) {
            model.put("error", e.getMessage());

            return "clientes.html";
        }

    }

}
