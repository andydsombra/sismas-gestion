
package com.Sismas.controlador;

import com.Sismas.Entidades.Foto;
import com.Sismas.Entidades.Usuario;
import com.Sismas.enumeraciones.UserAccess;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.FotoServicio;
import com.Sismas.servicios.UsuarioServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class ControladorUsuario {
    @Autowired
    private UsuarioServicio usuarioServicio;
    
    @Autowired
    private FotoServicio fotoServicio;
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/usuario")
    public String usuario(ModelMap model) {
        
        model.put("menu", "Usuarios");
        return "usuarioMain.html";
    }
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/registroUsuario")
    public String registroUsuario(ModelMap model) {
        
        model.put("menu", "Usuarios");
        return "registroUsuario.html";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping("/registrar")
    public String registrar(ModelMap model, @RequestParam String mail, @RequestParam String nombre, @RequestParam String apellido, @RequestParam String clave, @RequestParam String clave2) throws ErrorServicio {
      
        try {
            usuarioServicio.registarUsuario(mail, nombre, apellido, clave, clave2);
            Usuario a =usuarioServicio.buscarPorMail(mail);
            model.put("exito", "Usuario Registrado");
            model.put("menu", "Usuarios");
            model.put("usuarios",a);
            return "registroUsuario.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());
            model.put("mail", mail);
            model.put("nombre", nombre);
            model.put("apellido", apellido);
            model.put("clave", clave);
            model.put("clave2", clave2);
            model.put("menu", "Usuarios");
            return "registroUsuario.html";
        }
    }
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/actualizarUsuario")
    public String actualizarUsuario(ModelMap model,@RequestParam Long id) {
        Usuario a= usuarioServicio.buscarPorId(id);
        
        model.put("access", UserAccess.values());
        model.put("usuario", a);
        return "registroUsuario_1.html";
    }
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping("/actualizarUsuario")
    public String editarloUsario(ModelMap model, MultipartFile foto, @RequestParam Long id, @RequestParam String mail, @RequestParam String nombre, @RequestParam String apellido, @RequestParam UserAccess access) throws ErrorServicio {
        
        Foto photo = null;
        Usuario a =usuarioServicio.buscarPorId(id);
        if(foto==null|| foto.isEmpty()){
            photo=a.getFoto();
        }else{
            try {
                photo = fotoServicio.cargarArchivo(foto);
                System.out.println("Foto guardada con exito");
            } catch (Exception e) {
                model.put("error", e.getMessage());
            }
        }
        
 
        
        try {
            usuarioServicio.modificarUsuario(id,mail, nombre, apellido, photo, access);
            
            model.put("exito", "Usuario Actualizado");
            return "/registroUsuario";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());
            model.put("mail", mail);
            model.put("nombre", nombre);
            model.put("apellido", apellido);

            return "registroUsuario.html";
        }
    }
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping("/editarUsario")
    public String editarUsario(ModelMap model) {
        
        
        model.put("menu","menu");

        return "registroUsuario_2.html";
    }
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping("/fotoUsuario")
    public String fotoUsuario(@RequestParam Long id, ModelMap model){
        Usuario a= usuarioServicio.buscarPorId(id);
        
        model.put("usuario", a);
        model.put("accion","foto");
        
        return "registroUsuario_3_1.html";
    }
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @PostMapping("/actualizarFoto")
    public String actualizarFoto(ModelMap model, MultipartFile foto, @RequestParam Long id) throws ErrorServicio {
        
        Foto photo = null;
        Usuario a =usuarioServicio.buscarPorId(id);
        if(foto==null|| foto.isEmpty()){
            photo=a.getFoto();
        }else{
            try {
                photo = fotoServicio.cargarArchivo(foto);
                System.out.println("Foto guardada con exito");
            } catch (Exception e) {
                model.put("error", e.getMessage());
            }
        }
        
 
        
        try {
            usuarioServicio.modificarFoto(id, photo);
            
            model.put("exito", "Foto Actualizada");
            return "ok.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());
          
            return "registroUsuario_3_1.html";
        }
    }
    
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping("/passUsuario")
    public String passUsuario(@RequestParam Long id, ModelMap model){
        Usuario a= usuarioServicio.buscarPorId(id);
        
        model.put("usuario", a);
        model.put("accion","pass");
        
        return "registroUsuario_3.html";
    }
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @PostMapping("/actualizarPass")
    public String actualizarPass(ModelMap model, @RequestParam String pass, @RequestParam String clave, @RequestParam String clave2, @RequestParam Long id) throws ErrorServicio {
        

        Usuario a =usuarioServicio.buscarPorId(id);

        if(pass.equals(a.getClave())){
            try {
            usuarioServicio.modificarClave(clave, clave2,id );
            
            model.put("exito", "Contraseña Actualizada");
            return "ok.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());
          
            return "registroUsuario_3_1.html";
        }
            
        }     else{
            model.put("error","Contraseña incorrecta");
          
            return "registroUsuario_3_1.html";
        }

        
    }
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping("/verUsuario")
    public String buscarPorTipoTotal(ModelMap model,@RequestParam Long id) {
        Usuario a= usuarioServicio.buscarPorId(id);
      
        model.put("usuarios", a);
        return "usuarios_1.html";
    }
    
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/verUsuarios")
    public String buscarTodos(ModelMap model ) {
        List<Usuario> usuarios = usuarioServicio.todos();
       
        model.put("usuarios", usuarios);
        return "usuarios_1.html";
    }
    
}
