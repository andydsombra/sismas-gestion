package com.Sismas.controlador;

import com.Sismas.Entidades.Categoria;
import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.PedidoAPI;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.ResponseApi;
import com.Sismas.enumeraciones.Cat;
import com.Sismas.enumeraciones.Tipo;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.APIServicio;
import com.Sismas.servicios.ClienteServicio;
import com.Sismas.servicios.UsuarioServicio;
import com.Sismas.servicios.WppService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import interfaces.ClienteInterface;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/1")
public class ControladorAPI {

    @Autowired
    WppService wpp;

    @Autowired
    private UsuarioServicio usuarioServ;
    
    @Autowired
    ClienteServicio clienteServ;

    @Autowired
    private APIServicio apiServicio;

    @GetMapping("/productos")
    public ArrayList<Producto> productoAPI(ModelMap model) {

        return apiServicio.buscarTodosAPI();
    }

    @GetMapping("/categoria")
    public ArrayList categoriasAPI(ModelMap model) {

        ArrayList<Categoria> a = new ArrayList();

        Categoria d = new Categoria();

        d.setId(1);
        d.setNombre((Cat.ELECTRO).toString());
        d.setTipos(apiServicio.buscarTipoAPI(Cat.ELECTRO));
        a.add(d);

        Categoria c = new Categoria();

        c.setId(0);
        c.setNombre((Cat.AUTO).toString());
        c.setTipos(apiServicio.buscarTipoAPI(Cat.AUTO));
        a.add(c);

        return a;
    }

    @PostMapping("/pedido/nuevo")
    public String guardarPedido(@RequestBody String pedido) {

        PedidoAPI p = new PedidoAPI();

        try {
            p = apiServicio.leerPedido(pedido);

            apiServicio.guardarPedido(p);
            String mensaje = "Nuevo pedido de " + p.getCliente().getNombre() + ". Para verlo ve a http://localhost:8080/ventaApi?id=" + p.getId();
            if (p.getCliente().getTipo() == Tipo.MAYORISTA) {
                wpp.mandarWppMay(mensaje);
            } else {
                wpp.mandarWpp();
            }

            return "Recibido";

        } catch (ErrorServicio e) {
            return "Error al guardar el pedido";
        }
    }

    @PostMapping("/verificar")
    public ClienteInterface verificarCliente(@RequestBody String mail ){
        
        System.out.println(mail);
        String email= mail.replace("{\"mail\":\"", "");
        String correo= email.replace("\"}", "");
        System.out.println(correo);
        
        try {
            return apiServicio.clientePorMail(correo);
        } catch (Exception e) {
            return apiServicio.clientePorMail(correo);
        }
    }

    
    @GetMapping("/ejecutar")
    public Cliente ejecutar() {
        String g = "02634572569";

        Cliente c =clienteServ.buscarNumero(g);
        
        System.out.println(c.getNombre());

        return null;
    }

    @GetMapping("/loginAPI")
    public ResponseApi loginAPI(@RequestParam(required = false) String user, @RequestParam(required = false) String password, ModelMap model) {
        ResponseApi x = new ResponseApi();
        Boolean usuario = usuarioServ.verificarUser(user, password);

        if (usuario = true) {
            x.setMsg("validado");
            x.setStatus(true);
            x.setValue("ok");
        } else {
            x.setMsg("noValidado");
            x.setStatus(false);
            x.setValue("noOk");
        }

        return x;
    }

    @GetMapping("/clientes")
    public ArrayList allClientes() {

        try {
            return apiServicio.todosClientes();
   
        } catch (Exception e) {
           return null;
        }
    }
    
    @PostMapping("/venta/nueva")
    public String ventaApi(@RequestBody String venta) throws ErrorServicio{
        
        apiServicio.leerVenta(venta);
        

        return null;
    }
}
