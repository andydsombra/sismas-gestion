package com.Sismas.controlador;

import com.Sismas.Entidades.CierreUsuario;
import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.Diario;
import com.Sismas.Entidades.Usuario;
import com.Sismas.Entidades.Venta;
import com.Sismas.enumeraciones.TipoPago;
import com.Sismas.servicios.CierreServicio;
import com.Sismas.servicios.ClienteServicio;
import com.Sismas.servicios.DiarioServicio;
import com.Sismas.servicios.PagoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.UsuarioServicio;
import com.Sismas.servicios.VentaServicio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
@Controller
@RequestMapping("/")
public class ControladorEstadisticas {

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private PagoServicio pagoServ;

    @Autowired
    private DiarioServicio diarioServ;

    @Autowired
    private ClienteServicio clienteServ;

    @Autowired
    private VentaServicio ventaServicio;
    
    @Autowired
    private UsuarioServicio usuarioServicio;
    
    @Autowired
    private CierreServicio cUserServ;

    @GetMapping("/estadisticas")
    public String estadisticasMain(ModelMap model) {

        Double mercaderia = productoServicio.totalMercaderia();
        String val1 = String.format("%,.2f", mercaderia);

        Double ventas = ventaServicio.ventas();
        String ven = String.format("%,.2f", ventas);
        
        List<Usuario> user= usuarioServicio.todos();

        model.put("user", user);
        model.put("ventas", ven);
        model.put("mercaderia", val1);
        return "estadisiticasMain.html";
    }
    
    @GetMapping("/estadisticaUsuario")
    public String estadisticasUsuario(ModelMap model, @RequestParam Long id) {
        
        Usuario user= usuarioServicio.buscarPorId(id);

        ArrayList<Double> pagos = pagosUsuario(id);

        ArrayList<String> tipos = tipos();

        List<Venta> ventas= ventaServicio.ventasUserD(id);
        Double gan = ventaServicio.ventasUser(id);
        String total = String.format("%,.2f", gan);

        Double ing = pagoServ.totalPagos();
        String ingresos = String.format("%,.2f", ing);

        Double man = ventaServicio.ventasMananaUser(id);
        String manana = String.format("%,.2f", man);

        Double tar = ventaServicio.ventasTardeUser(id);
        String tarde = String.format("%,.2f", tar);

        List<Diario> d = new ArrayList();
        
        

        model.put("ventas",ventas);
        model.put("user", user);
        model.put("gan", null);
        model.put("tarde", tarde);
        model.put("manana", manana);
        model.put("ingresos", ingresos);
        model.put("total", total);
        model.put("pagos", pagos);
        model.put("tipo", tipos);
        return "graficoUsuarios.html";
    }
    
    @GetMapping("/estadisticaUsuarioTabla")
    public String estadisticasUsuarioTabla(ModelMap model) {
       
        List<CierreUsuario> user= usuarioServicio.todos();
       
        
        
        model.put("user", user);

        return "tablaUsuarios.html";
    }
    
    

    @GetMapping("/ventasDiarias")
    public String ventasDiarias(ModelMap model) {

        ArrayList<Double> pagos = pagos();

        ArrayList<String> tipos = tipos();

        Double gan = ventaServicio.ventas();
        String ganancias = String.format("%,.2f", gan);

        Double ing = pagoServ.totalPagos();
        String ingresos = String.format("%,.2f", ing);

        Double pen = ventaServicio.totalRestos();
        String pendientes = String.format("%,.2f", pen);

        Double man = ventaServicio.ventasManana();
        String manana = String.format("%,.2f", man);

        Double tar = ventaServicio.ventasTarde();
        String tarde = String.format("%,.2f", tar);

        List<Diario> d = new ArrayList();

        model.put("gan", null);
        model.put("tarde", tarde);
        model.put("manana", manana);
        model.put("restos", pendientes);
        model.put("ingresos", ingresos);
        model.put("ganancias", ganancias);
        model.put("pagos", pagos);
        model.put("tipo", tipos);

        return "graficoVentasDiarias.html";
    }

    @PostMapping("/buscarFecha")
    public String bFecha(ModelMap model, int d, int m, int a) {

        /* GRAFICO DE TIPOS DE PAGOS DIARIOS*/
        ArrayList<Double> pagos = pagos();

        ArrayList<String> tipos = tipos();

        Double gan = ventaServicio.ventas();
        String ganancias = String.format("%,.2f", gan);
        Double ing = pagoServ.totalPagos();
        String ingresos = String.format("%,.2f", ing);
        Double pen = ventaServicio.totalRestos();
        String pendientes = String.format("%,.2f", pen);

        Double man = ventaServicio.ventasManana();
        String manana = String.format("%,.2f", man);
        Double tar = ventaServicio.ventasTarde();
        String tarde = String.format("%,.2f", tar);

        Double date = diarioServ.ventasPorDia(d, m, a);
        String fecha = String.format("%,.2f", date);

        String dia = d + "-" + m + "-" + a;

        model.put("dia", dia);
        model.put("fecha", fecha);
        model.put("tarde", tarde);
        model.put("manana", manana);
        model.put("restos", pendientes);
        model.put("ingresos", ingresos);
        model.put("ganancias", ganancias);
        model.put("pagos", pagos);
        model.put("tipo", tipos);

        return "graficoVentasDiarias.html";
    }

    @PostMapping("/buscarPorMes")
    public String bEntreFecha(ModelMap model, int m, int a) {

        /* GRAFICO DE TIPOS DE PAGOS DIARIOS*/
        ArrayList<Double> pagos = pagos();

        ArrayList<String> tipos = tipos();

        Double ganancias = ventaServicio.ventas();
        Double ingresos = pagoServ.totalPagos();
        Double pendientes = ventaServicio.totalRestos();

        Double manana = ventaServicio.ventasManana();
        Double tarde = ventaServicio.ventasTarde();

        List<Double> g = diarioServ.porMesG(a, m);
        List<Date> d = diarioServ.porMesF(a, m);

        String x = null;

        model.put("gan", g);
        model.put("d", d);
        model.put("dia", x);
        model.put("tarde", tarde);
        model.put("manana", manana);
        model.put("restos", pendientes);
        model.put("ingresos", ingresos);
        model.put("ganancias", ganancias);
        model.put("pagos", pagos);
        model.put("tipo", tipos);

        return "graficoVentasDiarias_1.html";
    }

    @GetMapping("/clientesDeudores")
    public String clientesDeudores(ModelMap model) {
        ArrayList<Cliente> c = clienteServ.clientesDeudores();

        model.put("clientes", c);

        return "clientesD.html";
    }

    private ArrayList<Double> pagos() {
        ArrayList<Double> pagos = new ArrayList();
        Double e = pagoServ.totalPagoEfectivo();
        pagos.add(e);
        Double de = pagoServ.totalPagoDebito();
        pagos.add(de);
        Double c = pagoServ.totalPagoTarjeta();
        pagos.add(c);
        Double me = pagoServ.totalPagoMercadoPago();
        pagos.add(me);
        Double t = pagoServ.totalPagoTransferencia();
        pagos.add(t);

        return pagos;
    }
    
    private ArrayList<Double> pagosUsuario(Long id) {
        ArrayList<Double> pagos = new ArrayList();
        Double e = pagoServ.totalPagoEfectivoUser(id);
        pagos.add(e);
        Double de = pagoServ.totalPagoDebitoUser(id);
        pagos.add(de);
        Double c = pagoServ.totalPagoTarjetaUser(id);
        pagos.add(c);
        Double me = pagoServ.totalPagoMercadoPagoUser(id);
        pagos.add(me);
        Double t = pagoServ.totalPagoTransferenciaUser(id);
        pagos.add(t);

        return pagos;
    }

    private ArrayList<String> tipos() {
        ArrayList<String> tipos = new ArrayList();
        tipos.add(TipoPago.EFECTIVO.toString());
        tipos.add(TipoPago.DEBITO.toString());
        tipos.add(TipoPago.CREDITO.toString());
        tipos.add(TipoPago.MERCADOPAGO.toString());
        tipos.add(TipoPago.TRANSFERENCIA.toString());

        return tipos;
    }

}
