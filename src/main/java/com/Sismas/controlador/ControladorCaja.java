package com.Sismas.controlador;

import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.Pago;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Usuario;
import com.Sismas.Entidades.Venta;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.enumeraciones.TipoPago;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.ClienteServicio;
import com.Sismas.servicios.PagoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.UsuarioServicio;
import com.Sismas.servicios.VentaServicio;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class ControladorCaja {

    ArrayList<Pago> pagosLista = new ArrayList();

    @Autowired
    private VentaServicio ventaServ;

    @Autowired
    private ProductoServicio prodServ;

    @Autowired
    private ClienteServicio clienteServ;

    @Autowired
    private PagoServicio pagoServ;

    @Autowired
    private UsuarioServicio usuarioServicio;

    @GetMapping("/caja")
    public String cajaMain(ModelMap model) {
        List<Venta> v = ventaServ.ultimas();
        List<Producto> p = prodServ.buscarFaltante();
        Double tot = ventaServ.ventasD();
        String total = String.format("%,.2f", tot);

        model.put("hoy", total);
        model.put("ventas", v);
        model.put("productos", p);
        return "cajaMain.html";
    }

    @GetMapping("/cobrarVenta")
    public String cobrar(Long id, ModelMap model) throws ErrorServicio {
        Venta v = ventaServ.buscarPorId(id);
        Cliente c = clienteServ.buscarPorId(v.getCliente().getId());

        pagosLista.clear();
        Double resto = v.getResto();
        model.put("pagos", pagosLista);
        model.put("pago", TipoPago.values());
        model.put("venta", v);
        model.put("cliente", c);
        model.put("resto", resto);
        return "pago.html";
    }

    @GetMapping("/actualCobro")
    public String actualCobro(ModelMap model) {

        return "pago.html";
    }

    @PostMapping("/agregarPago")
    public String addPago(ModelMap model, RedirectAttributes atr, @RequestParam Long id, @RequestParam Double monto, @RequestParam String detalles, @RequestParam TipoPago pago) throws ErrorServicio {
        Venta v = ventaServ.buscarPorId(id);
        Cliente c = clienteServ.buscarPorId(v.getCliente().getId());

        Pago p = new Pago();

        try {
            p.setDetalles(detalles);
            p.setMonto(monto);
            p.setPago(pago);

        } catch (Exception e) {

        }

        pagosLista.add(p);

        Double resto = resto(v.getResto());

        atr.addFlashAttribute("venta", v);
        atr.addFlashAttribute("pagos", pagosLista);
        atr.addFlashAttribute("resto", resto);
        atr.addFlashAttribute("pago", TipoPago.values());
        atr.addFlashAttribute("cliente", c);

        return "redirect:/actualCobro";
    }

    @PostMapping("/eliminarPago")
    public String eliminarPago(RedirectAttributes atr, @RequestParam Long id, @RequestParam Double monto, TipoPago pago) throws ErrorServicio {
        Venta v = ventaServ.buscarPorId(id);
        Cliente c = clienteServ.buscarPorId(v.getCliente().getId());

        int posicion = verPosicion(monto, pago);

        pagosLista.remove(posicion);

        Double resto = resto(v.getTotal());

        atr.addFlashAttribute("venta", v);
        atr.addFlashAttribute("pagos", pagosLista);
        atr.addFlashAttribute("resto", resto);
        atr.addFlashAttribute("pago", TipoPago.values());
        atr.addFlashAttribute("cliente", c);

        return "redirect:/actualCobro";
    }

    @PostMapping("/confirmarPago")
    public String confirmarPago(ModelMap model, @RequestParam Long id, @RequestParam Long uId) throws ErrorServicio {
        Venta v = ventaServ.buscarPorId(id);
        Usuario user = usuarioServicio.buscarPorId(uId);

        for (Pago pago : pagosLista) {
            Pago p = pagoServ.nuevoPago(pago.getDetalles(), pago.getMonto(), v, pago.getPago(), user);
        }
        Double resto = resto(v.getTotal());

        if (resto == 0.0) {
            ventaServ.cambiarEstado(id, Estado.PAGADO, resto);
            model.put("venta", v);
            return "pagoExitoso.html";
        } else {
            ventaServ.cambiarEstado(id, Estado.PARCIAL, resto);
            model.put("venta", v);
            model.put("resto", resto);
            return "pagoParcial.html";
        }

    }

    public Double resto(Double total) {
        Double pagos = 0.0;
        for (Pago pago : pagosLista) {
            pagos = pagos + pago.getMonto();
        }
        Double resto = total - pagos;

        return resto;
    }

    public int verPosicion(Double monto, TipoPago pago) {
        int posicion = 0;
        for (int i = 0; i < pagosLista.size(); i++) {
            Pago p = pagosLista.get(i);
            if (p.getMonto().equals(monto) && p.getPago().equals(pago)) {
                posicion = i;
            }

        }
        return posicion;
    }
}
