
package com.Sismas.controlador;

import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class ControladorReg {
@Autowired
    private UsuarioServicio usuarioServicio;

    
    @GetMapping("/registro")
    public String registroUsuario(ModelMap model) {
        
        model.put("menu", "Usuarios");
        return "registro.html";
    }

    @PostMapping("/registrarlo")
    public String registrar(ModelMap model, @RequestParam String mail, @RequestParam String nombre, @RequestParam String apellido, @RequestParam String clave, @RequestParam String clave2) throws ErrorServicio {
        System.out.println("mail" + mail);
        System.out.println("nombre" + nombre);
        System.out.println("apellido" + apellido);
        System.out.println("clave1" + clave);
        System.out.println("clave2" + clave2);
        try {
            usuarioServicio.registarUsuario(mail, nombre, apellido, clave, clave2);
            model.put("exito", "Usuario Registrado");
            
            return "registro.html";
        } catch (ErrorServicio e) {

            model.put("error", e.getMessage());
            model.put("mail", mail);
            model.put("nombre", nombre);
            model.put("apellido", apellido);
            model.put("clave", clave);
            model.put("clave2", clave2);
            model.put("menu", "Usuarios");
            return "registro.html";
        }
    }
}
