package com.Sismas.controlador;

import com.Sismas.Entidades.Empresa;
import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.ProdPedido;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.Entidades.Usuario;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.EmpresaServicio;
import com.Sismas.servicios.PedidoServicio;
import com.Sismas.servicios.ProdPedidoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.ProveedorServicio;
import com.Sismas.servicios.UsuarioServicio;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class ControladorPedidos {

    ArrayList<ProdPedido> productosventa = new ArrayList<>();

    @Autowired
    private ProductoServicio productoServicio;

    @Autowired
    private EmpresaServicio empresaServ;

    @Autowired
    private PedidoServicio pedidoServicio;

    @Autowired
    private ProveedorServicio proveedorServicio;

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private ProdPedidoServicio ppServ;

    @GetMapping("/pedidos")
    public String pedidoMain(ModelMap model) {
        List<Producto> faltantes = productoServicio.buscarFaltante();
        List<Pedido> ultimosPedidos = pedidoServicio.buscarOrdernado();

        model.put("productos", faltantes);
        model.put("pedidos", ultimosPedidos);
        return "pedidoMain.html";
    }

    @GetMapping("/nuevoPedido")
    public String nuevaPedido(ModelMap model, String id) {
        Proveedor proveedor = proveedorServicio.buscarPorId(id);

        Producto producto = new Producto();

        productosventa.clear();
        model.put("productos", productosventa);
        model.put("producto", producto);
        model.put("proveedor", proveedor);

        return "pedido.html";
    }

    @GetMapping("/actualPedido")
    public String actualPedido(ModelMap model) {

        return "pedido.html";
    }

    @PostMapping("/findProducto")
    public String findProd(ModelMap model, @RequestParam String id, @RequestParam String pId) throws ErrorServicio {

        Proveedor proveedor = proveedorServicio.buscarPorId(id);

        Producto producto = new Producto();
        try {
            producto = productoServicio.buscarPorCodigo(pId);
            model.put("productos", productosventa);
            model.put("producto", producto);
            model.put("proveedor", proveedor);
            return "pedido.html";

        } catch (ErrorServicio e) {
            model.put("error", e.getMessage());
            model.put("productos", productosventa);
            model.put("producto", producto);
            model.put("proveedor", proveedor);
            return "pedido.html";
        }

    }

    @PostMapping("/eliminarProd")
    public String delProd(RedirectAttributes atr, @RequestParam String id, @RequestParam String pId) throws ErrorServicio {
        Proveedor proveedor = proveedorServicio.buscarPorId(id);
        System.out.println("------------------------------");
        System.out.println("------------------------------");
        System.out.println(proveedor.getNombre());
        System.out.println("------------------------------");
        System.out.println("------------------------------");
        Producto producto = new Producto();

        int posicion = buscarLista(pId);
        System.out.println("posiciòn " + posicion);

        productosventa.remove(posicion);

        atr.addFlashAttribute("productos", productosventa);
        atr.addFlashAttribute("producto", producto);
        atr.addFlashAttribute("proveedor", proveedor);

        return "redirect:/actualPedido";
    }

    @PostMapping("/addProducto")
    public String addProd(RedirectAttributes atr, @RequestParam String id, @RequestParam String pId, @RequestParam Integer cant) throws ErrorServicio {

        Proveedor proveedor = proveedorServicio.buscarPorId(id);
        System.out.println("------------------------------");
        System.out.println("------------------------------");
        System.out.println(proveedor.getNombre());
        System.out.println("------------------------------");
        System.out.println("------------------------------");

        Producto producto = new Producto();

        Producto product = productoServicio.buscarPorCodigo(pId);

        ProdPedido prod = new ProdPedido();
        try {
            prod.setCodigo(pId);
            prod.setCant(cant);
            prod.setInterno(product.getCodInterno());
            prod.setNombre(product.getNombre());
            prod.setColor(product.getColor());

            productosventa.add(prod);
            System.out.println("Producto ok");

            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("proveedor", proveedor);
            return "redirect:/actualPedido";
        } catch (Exception e) {
            atr.addFlashAttribute("error", e.getMessage());
            atr.addFlashAttribute("productos", productosventa);
            atr.addFlashAttribute("producto", producto);
            atr.addFlashAttribute("proveedor", proveedor);
            return "redirect:/actualPedido";

        }

    }

    @PostMapping("/pedir")
    public String pedir(ModelMap model, @RequestParam Long uId, @RequestParam String id, @RequestParam String obs) throws ErrorServicio {

        Proveedor proveedor = proveedorServicio.buscarPorId(id);
        System.out.println("------------------------------");
        System.out.println("------------------------------");
        System.out.println(proveedor.getNombre());
        System.out.println("------------------------------");
        System.out.println("------------------------------");

        Usuario user = usuarioServicio.buscarPorId(uId);
        Pedido p = new Pedido();

        try {
            p = pedidoServicio.nuevoPedido(obs, proveedor, user);

            prod(p);

            model.put("pedido", p);
            model.put("proveedor", proveedor);

            return "pedidoOk.html";
        } catch (ErrorServicio er) {

            model.put("error", er.getMessage());

            model.put("pedido", p);
            model.put("proveedor", proveedor);
            model.put("productos", productosventa);

            return "pedido.html";

        }

    }

    @GetMapping("/cancelarPedido")
    public String cancelarPedido() {
        return "redirect:/pedido";
    }

    /*-------------------Consultas---------------*/
    @GetMapping("/verPedido")
    public String verPedido(ModelMap model, Long id) throws ErrorServicio {

        Pedido p = pedidoServicio.buscarPorId(id);
        Proveedor pro = proveedorServicio.buscarPorId(p.getProveedor().getId());
        List<ProdPedido> prod = ppServ.buscarPorPedido(id);

        model.put("pedido", p);
        model.put("proveedor", pro);
        model.put("producto", prod);
        model.put("estado", Estado.values());
        return "verPedido.html";
    }

    @PostMapping("/actualizarEstado")
    public String actualizarEstado(ModelMap model, Long id, Estado estado) throws ErrorServicio {
        Pedido p = pedidoServicio.buscarPorId(id);
        Proveedor pro = proveedorServicio.buscarPorId(p.getProveedor().getId());
        List<ProdPedido> prod = ppServ.buscarPorPedido(id);

        pedidoServicio.editarEstado(estado, id);

        model.put("pedido", p);
        model.put("proveedor", pro);
        model.put("producto", prod);
        model.put("estado", Estado.values());
        return "verPedido.html";
    }

    @GetMapping("/verPedidos")
    public String verPedidos(ModelMap model) {
        List<Pedido> p = pedidoServicio.allPedidos();

        model.put("estado", Estado.values());
        model.put("accion", "Todos");
        model.put("tamano", p.size() + 1);
        model.put("pedidos", p);
        return "pedidos.html";
    }

    @PostMapping("/buscarPorEstado")
    public String buscarPorEstado(ModelMap model, String b) {
        List<Pedido> p = pedidoServicio.buscarPorEstado(b);

        model.put("estado", Estado.values());
        model.put("accion", "Estado");
        model.put("tamano", p.size() + 1);
        model.put("pedidos", p);
        return "pedidos.html";
    }

    @PostMapping("/buscarPorId")
    public String buscarPorId(ModelMap model, @RequestParam Long b) throws ErrorServicio {
        Pedido p = pedidoServicio.buscarPorId(b);

        model.put("estado", Estado.values());
        model.put("accion", "Número de Pedido");
        model.put("tamano", 1);
        model.put("pedidos", p);
        return "pedidos.html";
    }

    @PostMapping("/buscarPorProveedor")
    public String buscarPorProveedor(ModelMap model, @RequestParam String b) {
        Proveedor pe = proveedorServicio.encontrarPorNombre(b);

        List<Pedido> p = pedidoServicio.buscarPorProveedor(pe.getId());

        model.put("estado", Estado.values());
        model.put("accion", "Proveedor");
        model.put("tamano", p.size() + 1);
        model.put("pedidos", p);
        return "pedidos.html";
    }

    public int buscarLista(String id) {
        int posicion = 0;

        for (int i = 0; i < productosventa.size(); i++) {
            ProdPedido p = productosventa.get(i);
            if (p.getCodigo().equals(id)) {
                posicion = i;
                break;
            }
        }

        return posicion;
    }

    public void prod(Pedido p) throws ErrorServicio {

        for (ProdPedido pV : productosventa) {

            Producto po = productoServicio.buscarPorCodigo(pV.getCodigo());

            ProdPedido prop = new ProdPedido();
            prop = ppServ.cargar(po.getBarras(), po.getCodInterno(), po.getNombre(), po.getTipo(), po.getColor(), pV.getCant(), p);
            //(Long codigo, String nombre, String tiop, String color, int cant, Pedido pId)

        }

    }

    @GetMapping("/imprimirPedido")
    public String imprimirVenta(ModelMap model, @RequestParam Long id) throws ErrorServicio {

        Pedido p = pedidoServicio.buscarPorId(id);
        Proveedor pro = proveedorServicio.buscarPorId(p.getProveedor().getId());
        List<ProdPedido> prod = ppServ.buscarPorPedido(id);
        Empresa e = empresaServ.buscar(1);

        model.put("empresa", e);
        model.put("pedido", p);
        model.put("proveedor", pro);
        model.put("productos", prod);

        return "impresionPedido.html";
    }

    @GetMapping("/eliminarPedido")
    public String eliminarPedido(ModelMap model, @RequestParam Long id) throws ErrorServicio {
        try {
            Pedido pe = pedidoServicio.buscarPorId(id);
            
            System.out.println("Por eliminar productos");
            ppServ.elimarPp(id);
            
            System.out.println("Por eliminar productos");
            pedidoServicio.eliminarPedido(id);

            List<Pedido> p = pedidoServicio.allPedidos();

            model.put("estado", Estado.values());
            model.put("accion", "Todos");
            model.put("tamano", p.size() + 1);
            model.put("pedidos", p);
            return "pedidos.html";
        } catch (ErrorServicio e) {

            
            List<Pedido> p = pedidoServicio.allPedidos();

            model.put("estado", Estado.values());
            model.put("accion", "Todos");
            model.put("tamano", p.size() + 1);
            model.put("pedidos", p);
            return "pedidos.html";
        }

    }
}
