
package com.Sismas.controlador;


import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.servicios.PedidoServicio;
import com.Sismas.servicios.ProductoServicio;
import com.Sismas.servicios.ProveedorServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class ControladorProveedor {
    
    @Autowired
    private ProductoServicio productoServicio;    
    
    @Autowired  
    private ProveedorServicio proveedorServicio;
    
    @Autowired
    private PedidoServicio pedidoServicio;
    
    
    @GetMapping("/proveedor")
    public String proveedorMain(ModelMap model) {
        List<Producto> faltantes = productoServicio.buscarFaltante();
        model.put("productos", faltantes);
        return "proveedorMain.html";
    }
    
    @GetMapping("/verProveedores")
    public String verProveedor(ModelMap model) {

        List<Proveedor> proveedor = proveedorServicio.todosProv();

        model.put("proveedor", proveedor);
        return "proveedores.html";
    }
    
    @GetMapping("/registroProveedor")
    public String registroProveedor(ModelMap model, @RequestParam(required = false) String id) {

        String accion = null;

        Proveedor proveedor = null;

        if (id == null || id.isEmpty()) {
            accion = "Registrar";
            model.put("accion", accion);

            return "registroProveedor.html";
        } else {
            accion = "Actualizar";
            proveedor= proveedorServicio.buscarPorId(id);
            model.put("proveedor", proveedor);
            model.put("accion", accion);

            return "registroProveedor_1.html";
        }
    }
    @PostMapping("/registarProveedor")
    public String registrarProveedor(ModelMap model,@RequestParam String nombre,@RequestParam String dir,@RequestParam Long telefono,@RequestParam String mail) throws ErrorServicio{
        
            try {
               proveedorServicio.cargarProveedor(nombre, telefono, mail, dir);
                model.put("exito","Proveedor registrado");
                return "exitoProveedor.html";
            } catch (ErrorServicio e) {
                model.put("error", e.getMessage());
                return "registroProveedor.html";
            }
          
    }
    
    @PostMapping("/modificarProveedor")
    public String modificarProveedor(ModelMap model, @RequestParam String id,@RequestParam String nombre,@RequestParam String dir,@RequestParam Long telefono,@RequestParam String mail) throws ErrorServicio{
            
        try {
                
               proveedorServicio.modificarProveedor(id, nombre, telefono, mail, dir);
               Proveedor p= proveedorServicio.buscarPorId(id);
                model.put("exito","Proveedor Actualizado");
                model.put("proveedor", p);
                return "proveedores.html";
            } catch (ErrorServicio e) {
                model.put("error", e.getMessage());
                
                return "registroProveedor.html";
            }
            
    }

    @GetMapping("/verProveedor")
    public String verProveedor(ModelMap model, @RequestParam String id){
        Proveedor p= proveedorServicio.buscarPorId(id);
        model.put("proveedor",p);
        
        return "proveedores.html";
    }
    
     @GetMapping("/verProv")
    public String verProv(ModelMap model){
        List<Proveedor> proveedor = proveedorServicio.todosProv();
        model.put("proveedor",proveedor);
        
        return "proveedor.html";
    }
    
    @GetMapping("/verProveedor1")
    public String verProveedor_1(ModelMap model){
  
        List<Proveedor> proveedor = proveedorServicio.todosProv();
        model.put("proveedor",proveedor);
       
        return "proveedores_1.html";
    }
    
    @GetMapping("/verProveedor2")
    public String verProveedor_2(ModelMap model){
        List<Proveedor> proveedor = proveedorServicio.todosProv();
        model.put("proveedor",proveedor);
        
        return "proveedores_2.html";
    }
    
    @GetMapping("/verProdProv")
    public String verProdProv(ModelMap model, @RequestParam String id){
        Proveedor p= proveedorServicio.buscarPorId(id);
        List<Producto> pro= productoServicio.buscarPorProveedor(id);
        model.put("proveedor",p);
        model.put("productos", pro);
        return "provProd.html";
    }
    
    @GetMapping("/verProdPed")
    public String verProdPed(ModelMap model, @RequestParam String id){
        Proveedor p= proveedorServicio.buscarPorId(id);
        List<Pedido> pedido= pedidoServicio.buscarPorProv(id);
        model.put("proveedor",p);
        model.put("pedido", pedido);
        return "provPed.html";
    }
}
