package com.Sismas.controlador;

import com.Sismas.servicios.FotoServicio;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;



@Controller
@RequestMapping("/")
public class Controlador {

    
    @Autowired
    private FotoServicio fotoServicio;
    
    
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @GetMapping("/index")
    public String inicio(ModelMap model, HttpSession session) {
        
        model.put("menu", "Dashboard");
        return "inicio.html";
        
    }
    
    @GetMapping("/foto")
    public String foto() {
        return "pruebaFoto.html";
    }
    
    @PostMapping("/cargarFoto")
    public String cargarFoto(ModelMap model,@RequestParam MultipartFile foto){
        try {
            fotoServicio.cargarArchivo(foto);
            model.put("exito","Foto guardada");
        } catch (Exception e) {
            model.put("error", e.getMessage());
        }
 
        return "pruebaFoto.html";
    }
    
        


    
    
    

}
