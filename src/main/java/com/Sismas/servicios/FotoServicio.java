
package com.Sismas.servicios;

import com.Sismas.Entidades.Foto;
import com.Sismas.repository.FotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FotoServicio {
    @Autowired
    private FotoRepository fotoRepositorio;
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Foto cargarArchivo(MultipartFile archivo){
        Foto foto = new Foto();
        
        if (archivo != null && !archivo.isEmpty()) {
            try {
                
                foto.setMime(archivo.getContentType());
                System.out.println("cargó el mime");
                
                foto.setNombre(archivo.getName());
                System.out.println("cargó el nombre");
                
                foto.setContenido(archivo.getBytes());
                System.out.println("cargó el contenido"); 

                return fotoRepositorio.save(foto);
   
            } catch (Exception e) { 
                System.out.println(e.getMessage());
                return null;
            }
        }else{
            System.out.println("Archivo llegó nulo");
            return null;
        }
        
        
    }
}

