package com.Sismas.servicios;

import com.Sismas.Entidades.Empresa;
import com.Sismas.Entidades.Foto;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmpresaServicio {

    @Autowired
    private EmpresaRepository empresaRepositorio;


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Empresa cargarEmpresa(Integer id,  String nombre, String mail, String direcion, String razon, String tel, Foto foto) throws ErrorServicio {

       Empresa e= new Empresa();

       e.setDireccion(direcion);
       e.setId(id);
       e.setLogo(foto);
       e.setMail(mail);
       e.setNombre(nombre);
       e.setRazonSocial(razon);
       e.setTelefono(tel);

        return empresaRepositorio.save(e);
    }

    @Transactional(readOnly = true)
    public Empresa buscar(Integer id){
        return empresaRepositorio.buscar(id);
    }
}
