package com.Sismas.servicios;

import com.Sismas.Entidades.Diario;
import com.Sismas.Entidades.Usuario;
import com.Sismas.repository.DiarioRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DiarioServicio {

    @Autowired
    private DiarioRepository diarioRepo;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Diario cierre(Double venta, Double gan, Double efe, Double deb, Double mer, Double tran, Double cred, Usuario user) {
        Diario d = new Diario();

        d.setFecha(new Date());
        d.setVentas(venta);
        d.setGanancia(gan);
        d.setEfectivo(efe);
        d.setCredito(cred);
        d.setDebito(deb);
        d.setTransferencia(tran);
        d.setMercadoPago(mer);
        d.setVendedor(user);

        return diarioRepo.save(d);
    }

    public void validar(Double venta, Double gan, Double efe, Double deb, Double mer, Double tran, Double cred) {

        if (venta == null || venta.toString().isEmpty()) {
            venta = 0.0;
        }
        if (gan == null || gan.toString().isEmpty()) {
            gan = 0.0;
        }
        if (efe == null || efe.toString().isEmpty()) {
            efe = 0.0;
        }
        if (deb == null || deb.toString().isEmpty()) {
            deb = 0.0;
        }
        if (mer == null || mer.toString().isEmpty()) {
            mer = 0.0;
        }
        if (tran == null || tran.toString().isEmpty()) {
            tran = 0.0;
        }
        if (cred == null || cred.toString().isEmpty()) {
            cred = 0.0;
        }

    }

    @Transactional(readOnly = true)
    public List todos() {
        List<Double> d = new ArrayList();

        List<Diario> dias = diarioRepo.findAll();
        for (Diario dia : dias) {
            d.add(dia.getGanancia());
            System.out.println(dia.getGanancia());
        }
        return d;
    }

    @Transactional(readOnly = true)
    public List dias() {
        List<String> d = new ArrayList();

        List<Diario> dias = diarioRepo.findAll();
        for (Diario dia : dias) {
            d.add(dia.getFecha().toString());
            System.out.println(dia.getFecha().toString());
        }
        return d;
    }

    @Transactional(readOnly = true)
    public List<Double> porMesG(int a, int m) {
        List<Diario> dias = diarioRepo.findAll();
        List<Double> gan = new ArrayList();
        

        for (int i = 0; i < dias.size(); i++) {
            Diario vr = dias.get(i);
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if ( m == mes && a == ano) {
                gan.add(vr.getGanancia());
            }

        }
        
        return gan;
    }
    
    @Transactional(readOnly = true)
    public List<Date> porMesF(int a, int m) {
        List<Diario> dias = diarioRepo.findAll();
        List<Date> gan = new ArrayList();
        

        for (int i = 0; i < dias.size(); i++) {
            Diario vr = dias.get(i);
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if ( m == mes && a == ano) {
                gan.add(vr.getFecha());
            }

        }
        
        return gan;
    }
    
    
    @Transactional(readOnly = true)
    public Double ventasPorDia(int d, int m, int a) {
        List<Diario> v = diarioRepo.findAll();
        Double total = 0.0;

        for (int i = 0; i < v.size(); i++) {
            Diario vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (d == dia && m == mes && a == ano) {
                total = total + vr.getGanancia();
            }

        }
        return total;
    }
}
