package com.Sismas.servicios;

import com.Sismas.Entidades.Proveedor;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.ProveedorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProveedorServicio {

    @Autowired
    private ProveedorRepository proveedorRepositorio;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Proveedor cargarProveedor(String nombre, Long telefono, String mail, String dir) throws ErrorServicio {
        Proveedor proveedor = new Proveedor();
        
        validar(nombre, telefono, mail, dir);
        
        proveedor.setNombre(nombre);
        proveedor.setMail(mail);
        proveedor.setTelefono(telefono);
        proveedor.setDireccion(dir);
        return proveedorRepositorio.save(proveedor);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Proveedor modificarProveedor(String id,String nombre, Long telefono, String mail, String dir) throws ErrorServicio {
        Proveedor proveedor=buscarPorId(id);
        validar(nombre, telefono, mail, dir);

        proveedor.setNombre(nombre);
        proveedor.setMail(mail);
        proveedor.setTelefono(telefono);
        proveedor.setDireccion(dir);
        return proveedorRepositorio.save(proveedor);
    }
    
    public void validar(String nombre, Long telefono, String mail, String dir) throws ErrorServicio {
        if (nombre == null || nombre.isEmpty()) {
            throw new ErrorServicio("El nombre del proveedor no puede estar vacío");
        }

        if (telefono == null || telefono.toString().isEmpty()) {
            throw new ErrorServicio("Debe ingresar un número de teléfono");
        }

        if (mail == null || mail.isEmpty()) {
            throw new ErrorServicio("El mail del cliente no puede ser nulo");
        }

        if (dir == null || dir.isEmpty()) {
            throw new ErrorServicio("Debe ingresar la dirección");
        }
    }

    @Transactional(readOnly = true)
    public Proveedor encontrarPorNombre(String nombre) {
        Proveedor a = null;
        List<Proveedor> proveedores = proveedorRepositorio.findAll();
        for (Proveedor provee : proveedores) {
            if (provee.getNombre().equalsIgnoreCase(nombre)) {
                a = provee;
            }
        }
        return a;
    }
    

    @Transactional(readOnly = true)
    public List<Proveedor> todosProv() {
        return proveedorRepositorio.findAll();
    }

    @Transactional(readOnly = true)
    public Proveedor buscarPorId(String id) {
        return proveedorRepositorio.getById(id);

    }
}
