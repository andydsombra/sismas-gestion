
package com.Sismas.servicios;

import com.Sismas.Entidades.CierreUsuario;
import com.Sismas.Entidades.Usuario;
import com.Sismas.enumeraciones.Turno;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.CierreRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CierreServicio {
    
    @Autowired
    private CierreRepository cierreRepo;
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public CierreUsuario crearCierre(Double total, Double dif, Double efe, Double deb, Double cred, Double mP, Double tran, Double ventas, Usuario user, String obs) throws ErrorServicio{
        
        CierreUsuario cierre= new CierreUsuario();

        cierre.setTotal(total);
        cierre.setVentas(ventas);
        cierre.setTurno(validarTurno());
        cierre.setCredito(cred);
        cierre.setDebito(deb);
        cierre.setEfectivo(efe);
        cierre.setMercadoPago(mP);
        cierre.setTransferencia(tran);
        cierre.setUsuario(user);
        cierre.setFecha(new Date());
        cierre.setObservaciones(obs);
        cierre.setDiferencia(dif);

        
        return cierreRepo.save(cierre);               
    }
    
    @Transactional(readOnly = true)
    public List<CierreUsuario> todosU(Long id){
        return cierreRepo.buscarPorId(id);
    }
    
    /*******Funciones extra
     * 
     * @return
     * @throws ErrorServicio 
     */
    
    public Turno validarTurno() throws ErrorServicio{
        Turno t= asignarTurno();
        Date d= new Date();
        List<CierreUsuario> c = cierreRepo.findAll();
        Date hoy = new Date();

        for (int i = 0; i < c.size(); i++) {
            CierreUsuario u= c.get(i);
            Date ve = u.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDate() == ve.getDate()) {

                if(u.getTurno().equals(t)){
                    throw new ErrorServicio("El turno ya fue cerrado");
                }
            }
        }
        
        
        
        return t;
    }
    
    public Turno asignarTurno(){
        Turno t= Turno.MAÑANA;
        Date d= new Date();
        if( d.getHours()> 14){
            t= Turno.TARDE;
        }
        
        return t;
    }
    
    
}
