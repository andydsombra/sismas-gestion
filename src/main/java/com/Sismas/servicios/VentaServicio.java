package com.Sismas.servicios;

import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.Usuario;
import com.Sismas.Entidades.Venta;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.VentaRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VentaServicio {

    @Autowired
    private VentaRepository ventaRepositorio;

    @Autowired
    private ProdVentaServicio pVServ;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Venta nuevaVenta(Cliente cliente, Usuario user, Double total, String obs) throws ErrorServicio {

        Venta venta = new Venta();
        validar(cliente, total);

        venta.setCliente(cliente);
        venta.setEstado(Estado.PAGAR);
        venta.setVendedor(user);
        venta.setFecha(new Date());
        venta.setObservaciones(obs);
        venta.setTotal(total);
        venta.setResto(total);

        return ventaRepositorio.save(venta);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarVenta(Long id) {
        ventaRepositorio.deleteVenta(id);
    }

    public void validar(Cliente cliente, Double total) throws ErrorServicio {

        if (cliente == null || cliente.toString().isEmpty()) {
            throw new ErrorServicio("Debe indicar un cliente");
        }
        if (total == 0.0 || total == null || total.toString().isEmpty()) {
            throw new ErrorServicio("El total no puede ser 0");
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void cambiarEstado(Long id, Estado estado, Double resto) {
        Venta v = ventaRepositorio.buscarPorId(id);
        v.setResto(resto);
        v.setEstado(estado);
        ventaRepositorio.save(v);

    }

    @Transactional(readOnly = true)
    public List<Venta> buscarPorEstado(Estado e) {
        return ventaRepositorio.buscarPorEstado(e);
    }

    @Transactional(readOnly = true)
    public List<Venta> ultimas() {
        List p = ventaRepositorio.buscarUltimas(Estado.PAGAR);
        List pr = ventaRepositorio.buscarUltimas(Estado.PARCIAL);

        p.addAll(pr);
        
        return p;
    }

    @Transactional(readOnly = true)
    public List<Venta> ultimasHoy() {
        List<Venta> v = ventaRepositorio.findAll();
        List<Venta> h = new ArrayList();
        Date hoy = new Date();

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            Date ve = vr.getFecha();
            if (hoy.getYear() == ve.getYear() && hoy.getMonth() == ve.getMonth() && hoy.getDay() == ve.getDay()) {

                h.add(vr);

            }
        }
        return h;
    }

    @Transactional(readOnly = true)
    public List<Venta> allVentas() {
        return ventaRepositorio.buscarTodas();
    }

    @Transactional(readOnly = true)
    public Double totalRestos() {
        Double total = 0.0;
        List<Venta> v = ventaRepositorio.findAll();
        for (int i = 0; i < v.size(); i++) {
            Venta ve = v.get(i);
            int e = (int) Math.round(ve.getResto());
            if (e != 0) {
                total = total + ve.getResto();
            }
        }
        return total;
    }

    /*CALCULO DE GANANCIA POR DIA*/
    @Transactional(readOnly = true)
    public Double ventas() {
        List<Venta> v = ventaRepositorio.findAll();
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {

                total = total + pVServ.gananciaPorVenta(vr.getId());

            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double ventasUser(Long id) {
        List<Venta> v = ventaRepositorio.buscarPorUser(id);
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {

                total = total + vr.getTotal();

            }
        }
        return total;
    }
    
    @Transactional(readOnly = true)
    public Double ventasUserT(Long id) {
        List<Venta> v = ventaRepositorio.buscarPorUser(id);
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {

                if(hoy.getHours()<16){
                    if(vr.getFecha().getHours()<16)
                    total = total + vr.getTotal();
                }else{
                    if(vr.getFecha().getHours()>16)
                    total = total + vr.getTotal();
                }
                

            }
        }
        return total;
    }
    
    @Transactional(readOnly = true)
    public List<Venta> ventasUserD(Long id) {
        List<Venta> v = ventaRepositorio.buscarPorUser(id);
        ArrayList<Venta> ve= new ArrayList();
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {
                ve.add(vr);
               
            }
        }
        
        return ve;
    }

    /*Total Ventas del día*/
    @Transactional(readOnly = true)
    public Double ventasD() {
        List<Venta> v = ventaRepositorio.findAll();
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {

                total = total + vr.getTotal();

            }
        }

        System.out.println("Total ventas " + total);
        return total;
    }

    @Transactional(readOnly = true)
    public Double ventasManana() {
        List<Venta> v = ventaRepositorio.findAll();
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {
                if (vr.getFecha().getHours() <= 16) {
                    total = total + vr.getTotal();
                }

            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double ventasMananaUser(Long id) {
        List<Venta> v = ventaRepositorio.buscarPorUser(id);
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {
                if (vr.getFecha().getHours() <= 16) {
                    total = total + vr.getTotal();
                }

            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double ventasTarde() {
        List<Venta> v = ventaRepositorio.findAll();
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {
                if (vr.getFecha().getHours() >= 17) {
                    total = total + vr.getTotal();
                }

            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Double ventasTardeUser(Long id) {
        List<Venta> v = ventaRepositorio.buscarPorUser(id);
        Double total = 0.0;
        Date hoy = new Date();
        int diah = hoy.getDate();
        int mesh = hoy.getMonth() + 1;
        int anoh = hoy.getYear() + 1900;

        for (int i = 0; i < v.size(); i++) {
            Venta vr = v.get(i);
            int dia = vr.getFecha().getDate();
            int mes = vr.getFecha().getMonth() + 1;
            int ano = vr.getFecha().getYear() + 1900;

            if (anoh == ano && mesh == mes && diah == dia) {
                if (vr.getFecha().getHours() >= 17) {
                    total = total + vr.getTotal();
                }

            }
        }
        return total;
    }

    @Transactional(readOnly = true)
    public Venta buscarPorId(Long id) throws ErrorServicio {
        if (id == null || id.toString().isEmpty()) {
            throw new ErrorServicio("Ingrese un código");
        }

        return ventaRepositorio.buscarPorId(id);
    }

    @Transactional(readOnly = true)
    public List buscarPorCliente(String id) {
        return ventaRepositorio.buscarPorCliente(id);
    }

    @Transactional(readOnly = true)
    public ArrayList<Double> ventasPorFechasG(String d1, String m1, String a1, String d2, String m2, String a2) {
        List<Venta> v = ventaRepositorio.findAll();
        ArrayList<Double> gan = new ArrayList();
        Double total = 0.0;

        String s = a1 + "-" + m1 + "-" + d1;
        String e = a2 + "-" + m2 + "-" + d2;
        List<String> f = getBetweenDays(s, e);

        for (String fecha : f) {

            for (int i = 0; i < v.size(); i++) {
                Venta vr = v.get(i);
                Date ve = vr.getFecha();

                System.out.println("DATA " + ve);
                int dia = ve.getDate();
                int mes = ve.getMonth() + 1;
                int ano = ve.getYear() + 1900;
                String d = "";

                if ((ve.getDate()) < 10) {
                    d = ano + "-" + mes + "-0" + dia;
                }
                if ((ve.getMonth() + 1) < 10) {
                    d = ano + "-0" + mes + "-" + dia;
                }
                if ((ve.getMonth() + 1) < 10 && (ve.getDate()) < 10) {
                    d = ano + "-0" + mes + "-0" + dia;
                }

                if (d.equals(fecha)) {
                    total = total + pVServ.gananciaPorVenta(vr.getId());
                    System.out.println("ganancia" + pVServ.gananciaPorVenta(vr.getId()));
                    System.out.println(d + "--:--" + total);
                }

            }
            gan.add(total);
        }

        return gan;
    }

    @Transactional(readOnly = true)
    public List<String> ventasPorFechasF(String d1, String m1, String a1, String d2, String m2, String a2) {

        String s = a1 + "-" + m1 + "-" + d1;
        String e = a2 + "-" + m2 + "-" + d2;
        List<String> dias = getBetweenDays(s, e);

        return dias;
    }

    public static List<String> getBetweenDays(String s, String e) {

        LocalDate start = LocalDate.parse(s);
        LocalDate end = LocalDate.parse(e);
        List<String> totalDates = new ArrayList<>();
        while (!start.isAfter(end)) {
            totalDates.add(start.toString());
            start = start.plusDays(1);
        }

        return totalDates;
    }
    
    public ArrayList ventasPorNombres(List<Cliente> c){
        ArrayList<Venta> v = new ArrayList();
           
        for (Cliente cli : c) {
            List<Venta> a = buscarPorCliente(cli.getId());
            for (Venta ven : a) {
                v.add(ven);
            }
        }
        
        return v;
    }

}
