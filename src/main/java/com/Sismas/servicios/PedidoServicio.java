package com.Sismas.servicios;

import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.Proveedor;
import com.Sismas.Entidades.Usuario;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.PedidoRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PedidoServicio {

    @Autowired
    private PedidoRepository pedidoRepositorio;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Pedido nuevoPedido( String obs, Proveedor pro, Usuario user) {

        Pedido p = new Pedido();

    
        p.setEstado(Estado.PEDIDO);
      
        p.setFecha(new Date());
        p.setObservaciones(obs);
        p.setProveedor(pro);
        p.setVendedor(user);

        return pedidoRepositorio.save(p);
    }
    
    public void editarEstado(Estado estado, Long id){
        Pedido p= pedidoRepositorio.buscarPorId(id);
        p.setEstado(estado);
        pedidoRepositorio.save(p);
    }

    @Transactional(readOnly = true)
    public List<Pedido> buscarOrdernado(){
        return pedidoRepositorio.buscarOrdernado(Estado.PEDIDO);
    }
    
    @Transactional(readOnly = true)
    public List<Pedido> buscarPorEstado(String a){
        
        System.out.println(a);
        List<Pedido> p = null;
           
        switch (a){
            case "PAGADO": p= pedidoRepositorio.buscarOrdernado(Estado.PAGADO);
            break;
            case "PEDIDO": p= pedidoRepositorio.buscarOrdernado(Estado.PEDIDO);
            break;
            case "PAGAR": p= pedidoRepositorio.buscarOrdernado(Estado.PAGAR);
            break;
            case "PENDIENTE": p= pedidoRepositorio.buscarOrdernado(Estado.PENDIENTE);
            break;
            case "PARCIAL": p= pedidoRepositorio.buscarOrdernado(Estado.PARCIAL);
        } 
        return p;
    }
    
     @Transactional(readOnly = true)
     public List buscarPorProveedor(String id){
         return pedidoRepositorio.buscarPorProv(id);
     }
    
    @Transactional(readOnly = true)
    public List<Pedido> allPedidos(){
        return pedidoRepositorio.findAll();
    } 
    
    @Transactional(readOnly = true)
    public Pedido buscarPorId(Long id) throws ErrorServicio{
        if(id==null|| id.toString().isEmpty()){
            throw new ErrorServicio("Ingrese un código");
        }
        
        return pedidoRepositorio.buscarPorId(id) ;
    }
   
    @Transactional(readOnly = true)
    public List buscarPorProv(String id){
        return pedidoRepositorio.buscarPorProv(id);
    }
    
    
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarPedido(Long id){
        pedidoRepositorio.deletePedido(id);
    }
}
