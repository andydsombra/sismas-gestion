package com.Sismas.servicios;

import com.Sismas.Entidades.ApiModel;
import com.Sismas.Entidades.Cliente;
import com.Sismas.Entidades.ClienteModel;
import com.Sismas.Entidades.PedidoAPI;
import com.Sismas.Entidades.PedidoRecibido;
import com.Sismas.Entidades.ProdVenta;
import com.Sismas.Entidades.Producto;
import com.Sismas.Entidades.ProductoApi;
import com.Sismas.Entidades.TiposApi;
import com.Sismas.Entidades.Venta;
import com.Sismas.Entidades.VentaApi;
import com.Sismas.enumeraciones.Cat;
import com.Sismas.enumeraciones.Estado;
import com.Sismas.enumeraciones.Tipo;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.PedidoApiRepository;
import com.Sismas.repository.ProductoRepository;
import com.Sismas.repository.VentaRepository;
import com.Sismas.servicios.utiles.Utilities;
import com.Sismas.util.utiles;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import interfaces.ClienteInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class APIServicio {

    @Autowired
    private PedidoApiRepository pedidoRepo;

    @Autowired
    private ProductoServicio prodServicio;

    @Autowired
    private UsuarioServicio userServ;

    @Autowired
    private ClienteServicio clienteServ;

    @Autowired
    private ProductoRepository productoRepositorio;

    @Autowired
    private VentaRepository ventaRepository;

    @Transactional(readOnly = true)
    public ArrayList buscarTodosAPI() {

        List<Producto> pro = productoRepositorio.findAll();
        ArrayList<ApiModel> p = utiles.setearProductos(pro);

        return p;
    }

    @Transactional(readOnly = true)
    public PedidoAPI buscarPorID(Long id) {

        return pedidoRepo.buscarPorID(id);
    }

    @Transactional(readOnly = true)
    public ArrayList buscarCategoriaAPI(Cat categoria) {
        List<Producto> pro = productoRepositorio.buscarPorCat(categoria);

        ArrayList<ApiModel> p = utiles.setearProductos(pro);

        return p;
    }

    @Transactional(readOnly = true)
    public ArrayList buscarTipoAPI(Cat categoria) {
        List<Producto> pro = productoRepositorio.buscarPorCat(categoria);
        ArrayList<TiposApi> p = new ArrayList();
        ArrayList<String> tiposString = new ArrayList();
        String tipo = "";

        if (tiposString.size() == 0) {
            TiposApi t = new TiposApi();
            t.setId(0);
            t.setNombre("Todos");
            t.setProductos(utiles.setearProductos(pro));
            p.add(t);
        }

        for (Producto pe : pro) {
            tipo = pe.getTipo();
            int cont = 0;

            for (int i = 0; i < tiposString.size(); i++) {

                String l = tiposString.get(i);
                if (tipo.equals(l)) {
                    cont = 1;
                    break;
                } else {
                    cont = 0;
                }

            }

            if (cont == 0) {
                tiposString.add(tipo);
            }
        }

        for (int i = 0; i < tiposString.size(); i++) {
            TiposApi t = new TiposApi();
            t.setId(i + 1);
            t.setNombre(tiposString.get(i));
            t.setProductos(utiles.setearProductos(productoRepositorio.buscarPorTipo(tiposString.get(i))));
            p.add(t);
        }

        return p;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public PedidoAPI guardarPedido(PedidoAPI pedido) {

        return pedidoRepo.save(pedido);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void borrarPedido(Long id) {

        pedidoRepo.deleteById(id);
    }

    @Transactional(readOnly = true)
    public ArrayList productosPedidos(String productos) throws ErrorServicio {
        ArrayList<ProductoApi> pro = new ArrayList();
        ObjectMapper objectMapper = new ObjectMapper();
        ProductoApi p = new ProductoApi();

        int posicion, contador = 0;

        posicion = productos.indexOf(",");
        while (posicion != -1) {
            contador++;
            posicion = productos.indexOf(".", posicion + 1);
        }

        String[] partes = productos.split(",");

        for (int i = 0; i < (contador); i++) {
            String parte = partes[i];
            parte = parte.replaceAll("[.]", ",");

            try {
                p = objectMapper.readValue(parte, ProductoApi.class);
                Producto or = prodServicio.buscarPorId(p.getId());
                p.setBarras(or.getBarras());
                p.setCategoria(or.getCategoria().toString());
                p.setCodInterno(or.getCodInterno());
                p.setTipo(or.getTipo());
                p.setNombre(or.getNombre());
                p.setColor(or.getColor());
                p.setPrecioUnitario(or.getPrecioUnitario());
                p.setPrecioMayorista(or.getPrecioMay1());
                pro.add(p);

            } catch (JsonProcessingException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return pro;

    }

    public String corregirJson(String json) {

        String b = json.replaceAll("[()]", "\"");

        return b;
    }

    public List pedidosApi() {
        return (List) pedidoRepo.findAll();
    }

    public PedidoAPI leerPedido(String pedido) throws ErrorServicio {
        PedidoAPI ped = new PedidoAPI();
        PedidoRecibido recibido = new PedidoRecibido();
        ObjectMapper objectMapper = new ObjectMapper();
        String products = "[";

        try {
            recibido = objectMapper.readValue(pedido, new TypeReference<PedidoRecibido>() {
            });
            ClienteModel client = recibido.getCliente();
            Cliente c = obtenerCliente(client.getTelefono(), client.getNombre(), client.getMail());

            for (int i = 0; i < recibido.getProductos().size(); i++) {
                ApiModel pro = recibido.getProductos().get(i);
                products = products + "{ \"id\": \"" + pro.getId() + "\",";
                products = products + " \"barras\": \"" + pro.getBarras() + "\",";
                products = products + " \"codInterno\": \"" + pro.getCodInterno() + "\",";
                products = products + " \"tipo\": \"" + pro.getCodInterno() + "\",";
                products = products + " \"categoria\": \"" + pro.getCategoria() + "\",";
                products = products + " \"color\": \"" + pro.getColor() + "\",";
                products = products + " \"cantActual\": \"" + pro.getCantActual() + "\",";
                products = products + " \"cantMinima\": \"" + pro.getCantMinima() + "\",";
                products = products + " \"cantidad\": \"" + pro.getCantidad() + "\",";
                products = products + " \"precioUnitario\": \"" + pro.getPrecioUnitario() + "\",";
                products = products + " \"foto\": \"" + pro.getFoto() + "\",";
                String nombre = Utilities.validarNombre(pro.getNombre());
                products = products + " \"nombre\": \"" + nombre + "\"}";

                if ((i + 1) != recibido.getProductos().size()) {
                    products = products + ",";
                }
            }

            products = products + "]";

            ped.setCliente(c);
            ped.setProductos(products);
            ped.setEstado(Estado.NUEVO);
            ped.setFecha(recibido.getFecha());
            ped.setTotal(recibido.getTotal());

        } catch (JsonProcessingException ex) {
            Logger.getLogger(APIServicio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ped;
    }

    public void leerVenta(String ventaApi) throws ErrorServicio {
        VentaApi ven = new VentaApi();
        Venta venta = new Venta();
        ObjectMapper objectMapper = new ObjectMapper();
        String products = "[";

        try {
            ven = objectMapper.readValue(ventaApi, new TypeReference<VentaApi>() {
            });
            Cliente client = ven.getCliente();
            Cliente c = clienteServ.buscarPorId(client.getId());

            for (int i = 0; i < ven.getListProd().size(); i++) {
                ApiModel pro = ven.getListProd().get(i);
                products = products + "{ \"id\": \"" + pro.getId() + "\",";
                products = products + " \"barras\": \"" + pro.getBarras() + "\",";
                products = products + " \"codInterno\": \"" + pro.getCodInterno() + "\",";
                products = products + " \"tipo\": \"" + pro.getCodInterno() + "\",";
                products = products + " \"categoria\": \"" + pro.getCategoria() + "\",";
                products = products + " \"color\": \"" + pro.getColor() + "\",";
                products = products + " \"cantActual\": \"" + pro.getCantActual() + "\",";
                products = products + " \"cantMinima\": \"" + pro.getCantMinima() + "\",";
                products = products + " \"cantidad\": \"" + pro.getCantidad() + "\",";
                products = products + " \"precioUnitario\": \"" + pro.getPrecioUnitario() + "\",";
                products = products + " \"precioMayorista\": \"" + pro.getPrecioMayorista() + "\",";
                products = products + " \"precioMayorista2\": \"" + pro.getPrecioMayorista2() + "\",";
                products = products + " \"precioVenta\": \"" + pro.getPrecioVenta() + "\",";
                products = products + " \"subtotal\": \"" + pro.getSubtotal() + "\",";
                products = products + " \"foto\": \"" + pro.getFoto() + "\",";
                String nombre = Utilities.validarNombre(pro.getNombre());
                products = products + " \"nombre\": \"" + nombre + "\"}";

                if ((i + 1) != ven.getListProd().size()) {
                    products = products + ",";
                }
            }

            products = products + "]";

            venta.setCliente(c);
            venta.setListProd(products);
            venta.setEstado(Estado.PAGAR);
            venta.setFecha(new Date());
            venta.setTotal(ven.getTotal());
            venta.setResto(ven.getTotal());
            long id = 213;
            venta.setVendedor(userServ.buscarPorId(id));
            venta.setObservaciones("-");

            ventaRepository.save(venta);

            descontarProductos(venta.getListProd());

        } catch (JsonProcessingException ex) {
            Logger.getLogger(APIServicio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Cliente obtenerCliente(String tel, String nom, String mail) throws JsonProcessingException, ErrorServicio {
        Cliente c = new Cliente();

        Cliente cx = clienteServ.buscarTel(tel);
        if (cx == null || cx.toString().isEmpty()) {
            c = clienteServ.registrarCliente(nom, "-", tel, mail, Tipo.MINORISTA, "-", "-");
        } else {
            if (cx.getMail().equals(".")) {
                cx.setMail(mail);
                clienteServ.modificarMailCliente(cx.getId(), mail);
            }
            c = cx;
        }

        return c;
    }

    public ArrayList<ProdVenta> obtenerProductos(String prod, Cliente c) throws JsonProcessingException, ErrorServicio {
        ArrayList<ProdVenta> productosventa = new ArrayList();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        List<ApiModel> prods = objectMapper.readValue(prod, new TypeReference<List<ApiModel>>() {
        });

        for (ApiModel prod1 : prods) {

            Producto pto = prodServicio.buscarPorId(prod1.getId());

            ProdVenta prodVent = new ProdVenta();
            Integer i = (productosventa.size() + 1);
            prodVent.setId(i.toString());
            prodVent.setCodigo(prod1.getBarras());
            prodVent.setNombre(prod1.getNombre());
            prodVent.setCant(prod1.getCantidad());

            prodVent.setColor(prod1.getColor());
            if (c.getTipo().equals(Tipo.MAYORISTA)) {
                prodVent.setPrecio(pto.getPrecioMay1());
            } else {
                prodVent.setPrecio(pto.getPrecioUnitario());
            }

            prodVent.setSubtotal(prodVent.getPrecio() * prod1.getCantidad());

            productosventa.add(prodVent);

        }

        return productosventa;
    }

    public void descontarProductos(String prod) throws JsonProcessingException, ErrorServicio {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        List<ApiModel> prods = objectMapper.readValue(prod, new TypeReference<List<ApiModel>>() {
        });

        for (ApiModel prod1 : prods) {

            prodServicio.restarCant(prod1.getId(), prod1.getCantidad());

        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void descripcion() {

        List<PedidoAPI> p = (List) pedidoRepo.findAll();
        for (PedidoAPI pro : p) {
            pro.setEstado(Estado.NUEVO);
            pedidoRepo.save(pro);
        }
    }

    public ArrayList todosClientes() {
        List<Cliente> clientes = clienteServ.todosLosClientes();
        ArrayList<ClienteInterface> x = new ArrayList<>();

        for (Cliente c : clientes) {
            ClienteInterface a = new ClienteInterface();
            a.setNombre(c.getNombre());
            a.setDireccion(c.getDireccion());
            a.setId(c.getId());
            a.setMail(c.getMail());
            a.setDocumento(c.getDocumento());
            a.setNumClient(c.getNumClient());
            a.setTelefono(c.getTelefono());
            a.setTipo(c.getTipo());
            x.add(a);
        }

        return x;
    }

    public ClienteInterface clientePorMail(String mail) {
        List<Cliente> clientes = clienteServ.todosLosClientes();
        ClienteInterface a = new ClienteInterface();

        for (int i = 0; i < clientes.size(); i++) {
            Cliente c = clientes.get(i);

            if (c.getMail().equalsIgnoreCase(mail)) {
            a.setNombre(c.getNombre());
            a.setDireccion(c.getDireccion());
            a.setId(c.getId());
            a.setMail(c.getMail());
            a.setDocumento(c.getDocumento());
            a.setNumClient(c.getNumClient());
            a.setTelefono(c.getTelefono());
            a.setTipo(c.getTipo());
            }
        }

        return a;
    }

}
