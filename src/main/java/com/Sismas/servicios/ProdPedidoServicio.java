package com.Sismas.servicios;

import com.Sismas.Entidades.Pedido;
import com.Sismas.Entidades.ProdPedido;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.ProdPedidRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProdPedidoServicio {

    @Autowired
    private ProdPedidRepository pPRepositorio;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public ProdPedido cargar(String codigo, String interno, String nombre, String tiop, String color, int cant, Pedido pId) {

        ProdPedido p = new ProdPedido();

        p.setCodigo(codigo);
        p.setInterno(interno);
        p.setNombre(nombre);
        p.setCant(cant);
        p.setTipo(tiop);
        p.setColor(color);
        p.setPedido(pId);

        return pPRepositorio.save(p);
    }

    @Transactional(readOnly = true)
    public List<ProdPedido> buscarPorPedido(Long p) {
        return pPRepositorio.buscarPorPedido(p);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void elimarPp(Long p) throws ErrorServicio {
        List<ProdPedido> pp = pPRepositorio.buscarPorPedido(p);
        try {
            for (ProdPedido prodPedido : pp) {
                System.out.println(prodPedido.getNombre());
                pPRepositorio.deleteById(prodPedido.getId());
            }
        } catch (Error e) {
            throw new ErrorServicio(e.getMessage());
        }

    }
}
