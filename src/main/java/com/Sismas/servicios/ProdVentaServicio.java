package com.Sismas.servicios;

import com.Sismas.Entidades.ProdVenta;
import com.Sismas.Entidades.Venta;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.ProdVentaRepository;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProdVentaServicio {

    @Autowired
    private ProdVentaRepository prRepositorio;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public ProdVenta cargar(String codigo, String nombre, String tipo, String color, Double precio, Double gan, int cant, Double subTotal, Venta vId) {

        ProdVenta p = new ProdVenta();

        p.setCodigo(codigo);
        p.setNombre(nombre);
        p.setTipo(tipo);
        p.setColor(color);
        p.setPrecio(precio);
        p.setGanancia(gan);
        p.setCant(cant);
        p.setSubtotal(subTotal);
        p.setVenta(vId);

        return prRepositorio.save(p);
    }

    @Transactional(readOnly = true)
    public List<ProdVenta> buscarPorVenta(Long v) {
        return prRepositorio.buscarPorVenta(v);
    }

    @Transactional(readOnly = true)
    public Double gananciaPorVenta(Long v) {
        List<ProdVenta> pv = prRepositorio.buscarPorVenta(v);
        Double ganancia = 0.0;
        for (int i = 0; i < pv.size(); i++) {
            ProdVenta x = pv.get(i);

            ganancia = ganancia + x.getGanancia();
        }

        return ganancia;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void eliminarPv(Long vId) throws ErrorServicio {
        List<ProdVenta> a = prRepositorio.buscarPorVenta(vId);
        try {
            for (ProdVenta pv : a) {
                System.out.println(pv.getNombre());
                prRepositorio.deleteById(pv.getId());
            }
        } catch (Error e) {
            throw new ErrorServicio(e.getMessage());
        }

    }
    
        @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void corregirGanancias(Long id){
        List<ProdVenta> a = prRepositorio.buscarPorVenta(id);
        for (ProdVenta x : a) {
            if(x.getCant()>1){
                Double g=x.getCant()*x.getGanancia();
                
                x.setGanancia(g);
                prRepositorio.save(x);
            }
        }
    }
    
}
