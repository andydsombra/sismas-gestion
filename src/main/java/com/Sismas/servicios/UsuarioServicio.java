package com.Sismas.servicios;

import com.Sismas.Entidades.Foto;
import com.Sismas.Entidades.Usuario;
import com.Sismas.enumeraciones.UserAccess;
import com.Sismas.errores.ErrorServicio;
import com.Sismas.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class UsuarioServicio implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepositorio;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario registarUsuario(String mail, String nombre, String apellido, String clave1, String clave2) throws ErrorServicio {

        Usuario usuario = new Usuario();

        Validar(mail, nombre, apellido);
        String clave = validarClave(clave1, clave2);

        usuario.setMail(mail);
        usuario.setNombre(nombre);
        usuario.setApellido(apellido);
        String encriptada = new BCryptPasswordEncoder().encode(clave);
        usuario.setClave(encriptada);
        usuario.setIngreso(new Date());
        usuario.setTipo(UserAccess.ADMIN);

        return usuarioRepositorio.save(usuario);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario modificarUsuario(Long id, String mail, String nombre, String apellido, Foto foto, UserAccess access) throws ErrorServicio {

        Usuario usuario = usuarioRepositorio.buscarPorId(id);

        Validar("x", nombre, apellido);

        usuario.setMail(mail);
        usuario.setNombre(nombre);
        usuario.setApellido(apellido);
        usuario.setTipo(access);
        usuario.setFoto(foto);

        return usuarioRepositorio.save(usuario);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Usuario modificarFoto(Long id, Foto foto) throws ErrorServicio {

        Usuario usuario = usuarioRepositorio.buscarPorId(id);

        usuario.setFoto(foto);

        return usuarioRepositorio.save(usuario);

    }

    public void Validar(String mail, String nombre, String apellido) throws ErrorServicio {

        if (nombre == null || nombre.isEmpty()) {
            throw new ErrorServicio("El nombre del usuario no puede ser nulo");
        }

        if (apellido == null || apellido.isEmpty()) {
            throw new ErrorServicio("El apellido del usuario no puede ser nulo");
        }

        if (mail == null || mail.isEmpty()) {
            throw new ErrorServicio("Debe ingresar su mail");
        }

        Usuario e = buscarPorMail(mail);

        if (e != null) {
            throw new ErrorServicio("El mail ya se encuentra registrado");
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void modificarClave(String clave1, String clave2, Long id) throws ErrorServicio {
        String clave = validarClave(clave1, clave2);

        Optional<Usuario> respuesta = usuarioRepositorio.findById(id);
        if (respuesta.isPresent()) {
            Usuario usuario = respuesta.get();

            String encriptada = new BCryptPasswordEncoder().encode(clave);
            usuario.setClave(encriptada);
            usuarioRepositorio.save(usuario);
        }
    }

    public String validarClave(String clave1, String clave2) throws ErrorServicio {

        if (clave1 == null || clave1.length() <= 5) {
            throw new ErrorServicio("La clave debe tener al menos 6 carateres");
        }

        if (clave1.equals(clave2)) {
            String clave = clave1;
            return clave;
        } else {
            throw new ErrorServicio("Las claves deben coincidir");
        }
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorId(Long id) {
        Usuario a = null;
        Optional<Usuario> res = usuarioRepositorio.findById(id);
        if (res.isPresent()) {
            a = res.get();
        }
        return a;
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorMail(String mail) {
        Usuario a = usuarioRepositorio.buscarPorMail(mail);

        return a;
    }

    @Transactional(readOnly = true)
    public List todos() {
        List<Usuario> todos = usuarioRepositorio.findAll();
        return todos;
    }

    public Boolean existe(Long id) {
        Boolean existe = false;
        try {
            Usuario resultado = usuarioRepositorio.buscarPorId(id);
            existe = true;
        } catch (Exception e) {
            existe = false;
        }

        return existe;
    }

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepositorio.buscarPorMail(mail);
        if (usuario != null) {
            List<GrantedAuthority> permisos = new ArrayList<>();

            GrantedAuthority p1 = new SimpleGrantedAuthority("ROLE_" + usuario.getTipo());
            permisos.add(p1);

            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(true);

            session.setAttribute("usuariosession", usuario);

            User user = new User(usuario.getMail(), usuario.getClave(), permisos);
            return user;

        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Boolean verificarUser(String mail, String password) {
        Boolean b = false;
        try {
            Usuario u = usuarioRepositorio.buscarPorMail(mail);

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            b = encoder.matches(password, u.getClave());
        } catch (Exception e) {
            b= false;
        }



        return b;
    }
}
