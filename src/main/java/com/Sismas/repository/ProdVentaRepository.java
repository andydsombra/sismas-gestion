
package com.Sismas.repository;

import com.Sismas.Entidades.ProdVenta;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdVentaRepository extends JpaRepository<ProdVenta, String>{
    
    @Query("SELECT a FROM ProdVenta a WHERE a.venta.id= :venta")
    public List<ProdVenta> buscarPorVenta(@Param("venta")Long venta);
    
   
}
