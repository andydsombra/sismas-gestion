

package com.Sismas.repository;

import com.Sismas.Entidades.ProdPedido;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdPedidRepository extends JpaRepository<ProdPedido, String>{
    
     @Query("SELECT a FROM ProdPedido a Where a.pedido.id= :pedido")
    public List<ProdPedido> buscarPorPedido(@Param("pedido")Long pedido);
}
