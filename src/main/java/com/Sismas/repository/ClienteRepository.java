
package com.Sismas.repository;

import com.Sismas.Entidades.Cliente;
import com.Sismas.enumeraciones.Tipo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, String>{
    
    @Query("SELECT a FROM Cliente a WHERE a.nombre LIKE %:nom%")
    public List<Cliente> buscarPorNombre(@Param ("nom")String nom);
    
    @Query("SELECT a FROM Cliente a WHERE a.nombre LIKE %:nom%")
    public Cliente buscarNombre(@Param ("nom")String nom);
    
    @Query("SELECT a FROM Cliente a WHERE a.telefono LIKE :telefono")
    public Cliente buscarTel(@Param ("telefono")String telefono);
    
    @Query("SELECT a FROM Cliente a WHERE a.numClient LIKE %:num%")
    public Cliente buscarNumero(@Param ("num")String num);
    
    @Query("SELECT a FROM Cliente a WHERE a.tipo LIKE :tipo")
    public List<Cliente> buscarTipoMay(@Param ("tipo")Tipo tipo);
    
    @Query("SELECT a FROM Cliente a WHERE a.tipo LIKE :tipo")
    public List<Cliente> buscarTipoMin(@Param ("tipo")Tipo tipo);
    
    @Query("SELECT a FROM Cliente a WHERE a.activo LIKE :p")
    public List<Cliente> buscarActivos(@Param ("p")String p);
}
