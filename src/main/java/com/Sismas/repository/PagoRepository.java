
package com.Sismas.repository;


import com.Sismas.Entidades.Pago;
import com.Sismas.enumeraciones.TipoPago;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PagoRepository extends JpaRepository<Pago, String>{
    
    @Query("SELECT a FROM Pago a Where a.pago LIKE :tipo")
    public List<Pago> buscarPago(@Param("tipo")TipoPago tipo);
    
    @Query("SELECT a FROM Pago a Where a.venta.id LIKE :v")
    public List<Pago> buscarPorVenta(@Param("v")Long v);
    
    @Query("SELECT a FROM Pago a Where a.pago LIKE :tipo AND a.usuario.id LIKE :id")
    public List<Pago> buscarPagoId(@Param("tipo")TipoPago tipo,@Param("id")Long id );
    
}
