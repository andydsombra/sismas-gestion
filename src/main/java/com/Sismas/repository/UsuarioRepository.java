
package com.Sismas.repository;

import com.Sismas.Entidades.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    @Query("SELECT a FROM Usuario a WHERE a.id = :id")
    public Usuario buscarPorId(@Param("id")Long id);
    
     
    @Query("SELECT a FROM Usuario a WHERE a.mail = :mail")
    public Usuario buscarPorMail(@Param("mail")String mail);
}
