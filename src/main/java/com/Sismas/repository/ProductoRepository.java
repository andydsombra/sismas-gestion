
package com.Sismas.repository;

import com.Sismas.Entidades.Producto;
import com.Sismas.enumeraciones.Cat;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long>{
    
    @Query("SELECT a FROM Producto a WHERE a.tipo LIKE %:tip%")
    public List<Producto> buscarPorTipo(@Param("tip")String tip);
    
    @Query("SELECT a FROM Producto a WHERE a.barras LIKE :barra")
    public Producto buscarPorBarra(@Param("barra")String barra);
    
    @Query("SELECT a FROM Producto a WHERE a.nombre LIKE %:nom%")
    public List<Producto> buscarPorNom(@Param("nom")String nom);
    
    @Query("SELECT a FROM Producto a WHERE a.categoria LIKE :cat")
    public List<Producto> buscarPorCat(@Param("cat")Cat cat);
    
    @Query("SELECT a FROM Producto a WHERE a.tipo LIKE %:tip% AND a.cantLocal > 0")
    public List<Producto> buscarPorTipoLocal(@Param("tip")String tip);
    
    @Query("SELECT a FROM Producto a WHERE a.nombre LIKE %:nom% AND a.cantLocal > 0")
    public List<Producto> buscarPorNomLocal(@Param("nom")String nom);
    
    @Query("SELECT a FROM Producto a WHERE a.categoria LIKE :cat AND a.cantLocal > 0")
    public List<Producto> buscarPorCatLocal(@Param("cat")Cat cat);
    
    @Query("SELECT a FROM Producto a WHERE a.tipo LIKE %:tip% AND a.cantDeposito > 0")
    public List<Producto> buscarPorTipoDepo(@Param("tip")String tip);
    
    @Query("SELECT a FROM Producto a WHERE a.nombre LIKE %:nom% AND a.cantDeposito > 0")
    public List<Producto> buscarPorNomDepo(@Param("nom")String nom);
    
    @Query("SELECT a FROM Producto a WHERE a.categoria LIKE :cat AND a.cantDeposito > 0")
    public List<Producto> buscarPorCatDepo(@Param("cat")Cat cat);
    
    @Query("SELECT a FROM Producto a WHERE a.id = :id")
    public Producto buscarPorCodigo(@Param("id")String id);
    
    @Query("SELECT a FROM Producto a WHERE a.codInterno = :id")
    public Producto buscarPorCodigoInterno(@Param("id")String id);
    
    @Query("SELECT a FROM Producto a WHERE a.nombre = :nombre ORDER BY a.nombre DESC")
    public List<Producto> buscarPorNombre(@Param("nombre")String nombre);
    
    @Query("SELECT a FROM Producto a WHERE a.cantLocal >= 1")
    public List<Producto> buscarLocal(@Param("cant")Integer cant);
    
    @Query("SELECT a FROM Producto a WHERE a.cantDeposito >= 1")
    public List<Producto> buscarDeposito(@Param("cant")Integer cant);
    
    @Query("SELECT a FROM Producto a WHERE a.cantActual < a.cantIdeal")
    public List<Producto> buscarFaltante(@Param("cant")Integer cant);
    
    @Query("SELECT a FROM Producto a WHERE a.cantActual < a.cantMinima")
    public List<Producto> buscarFaltanteUrgente(@Param("cant")Integer cant);
    
    @Query("SELECT a FROM Producto a WHERE a.proveedor.id= :prov")
    public List<Producto> buscarPorProveedor(@Param("prov") String prov);
}
