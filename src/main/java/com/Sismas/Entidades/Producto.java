
package sysmas.Entidades;

import sysmas.Enumeraciones.Cat;
import com.sun.istack.NotNull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;


@Entity
public class Producto {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;// idenitificador del producto
    
    private String barras;
    
    private String codInterno;

    @Column(nullable = false)
    private String nombre;// nombre comercial

    @Column(nullable = false)
    private String tipo;//Categoría general del producto
    

    @Enumerated(EnumType.STRING)
    private Cat categoria;


    private String color;// color 
    
    private String descripcion;

    @Column(nullable = false)
    private Integer cantActual;
    
    @Column(nullable = false)
    private Integer cantMinima;
    
    @Column(nullable = false)
    private Integer cantLocal;
    
    @Column(nullable = false)
    private Integer cantDeposito;

    @Column(nullable = false)
    private Integer cantIdeal;
    
    
    private Integer PreVenta;
    
    @NotNull
    private Double costo;
    
    @Column(nullable = false)
    private Double precioUnitario;
    
    @Column(nullable = false)
    private Double precioUnitarios;

    @Column(nullable = false)
    private Double precioMay1;
    
    @Column(nullable = false)
    private Double precioMay1s;

    @Column(nullable = false)
    private Double precioMay2;
    
    @Column(nullable = false)
    private Double precioMay2s;

    @Column(nullable = false)
    private Double precioDolar;

    @ManyToOne
    private Proveedor proveedor;
    
    @OneToOne
    private Foto foto;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getBarras() {
        return barras;
    }

    public Integer getPreVenta() {
        return PreVenta;
    }

    public void setPreVenta(Integer PreVenta) {
        this.PreVenta = PreVenta;
    }

    public void setBarras(String barras) {
        this.barras = barras;
    }

    public String getCodInterno() {
        return codInterno;
    }

    public void setCodInterno(String codInterno) {
        this.codInterno = codInterno;
    }
    
    public Integer getCantMinima() {
        return cantMinima;
    }

    public void setCantMinima(Integer cantMinima) {
        this.cantMinima = cantMinima;
    }
    
    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public Integer getCantLocal() {
        return cantLocal;
    }

    public void setCantLocal(Integer cantLocal) {
        this.cantLocal = cantLocal;
    }

    public Integer getCantDeposito() {
        return cantDeposito;
    }

    public void setCantDeposito(Integer cantDeposito) {
        this.cantDeposito = cantDeposito;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Cat getCategoria() {
        return categoria;
    }

    public void setCategoria(Cat categoria) {
        this.categoria = categoria;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCantActual() {
        return cantActual;
    }

    public void setCantActual(Integer cantActual) {
        this.cantActual = cantActual;
    }

    public Integer getCantIdeal() {
        return cantIdeal;
    }

    public void setCantIdeal(Integer cantIdeal) {
        this.cantIdeal = cantIdeal;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Double getPrecioMay1() {
        return precioMay1;
    }

    public void setPrecioMay1(Double precioMay1) {
        this.precioMay1 = precioMay1;
    }

    public Double getPrecioMay2() {
        return precioMay2;
    }

    public void setPrecioMay2(Double precioMay2) {
        this.precioMay2 = precioMay2;
    }

    public Double getPrecioDolar() {
        return precioDolar;
    }

    public void setPrecioDolar(Double precioDolar) {
        this.precioDolar = precioDolar;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Double getPrecioUnitarios() {
        return precioUnitarios;
    }

    public void setPrecioUnitarios(Double precioUnitarios) {
        this.precioUnitarios = precioUnitarios;
    }

    public Double getPrecioMay1s() {
        return precioMay1s;
    }

    public void setPrecioMay1s(Double precioMay1s) {
        this.precioMay1s = precioMay1s;
    }

    public Double getPrecioMay2s() {
        return precioMay2s;
    }

    public void setPrecioMay2s(Double precioMay2s) {
        this.precioMay2s = precioMay2s;
    }
    
}
    

    