
package sysmas.Entidades;

import java.util.List;

public class Categoria {

    private Integer id;
    private String nombre;
    private List<TiposApi> tipos;

    public List<TiposApi> getTipos() {
        return tipos;
    }

    public void setTipos(List<TiposApi> tipos) {
        this.tipos = tipos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
