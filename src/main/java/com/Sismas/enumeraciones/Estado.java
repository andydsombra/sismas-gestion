
package com.Sismas.enumeraciones;


public enum Estado {
    PAGADO, PENDIENTE, PEDIDO, PARCIAL, PAGAR, NUEVO, VISTO, CERRADO;
}
