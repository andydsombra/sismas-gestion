FROM openjdk:11
ADD target/SysmasAPI-2.0.jar SysmasAPI-2.0.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "SysmasAPI-2.0.jar"]